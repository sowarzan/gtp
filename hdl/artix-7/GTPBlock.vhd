library ieee;
use ieee.STD_LOGIC_1164.all;
use ieee.NUMERIC_STD.all;
library work;
use work.GTP_TILE_PKG.all;
entity GTPBlock is
  generic (
           G_RATE : GTP_RATE_TYPE := GBPS_3_0
           );
  port (
        gtp_WrData : in std_logic_vector(15 downto 0 );
        Clk : in std_logic;
        TxStr1 : in std_logic;
        TxStr2 : in std_logic;
        TxStr3 : in std_logic;
        TxStr4 : in std_logic;
        Debug : out std_logic_vector(31 downto 0 );
        Rst : in std_logic;
        gtp_Addr : in std_logic_vector(5 downto 1 );
        gtp_WrDone : out std_logic;
        SFPTxFault_N : in std_logic_vector(3 downto 0 );
        RxStr1 : out std_logic;
        RxStr2 : out std_logic;
        RxStr3 : out std_logic;
        RxStr4 : out std_logic;
        RxData1 : out std_logic_vector(15 downto 0 );
        RxData2 : out std_logic_vector(15 downto 0 );
        GTP4_TX_P : out std_logic;
        RxData3 : out std_logic_vector(15 downto 0 );
        RxData4 : out std_logic_vector(15 downto 0 );
        GTP4_RX_P : in std_logic;
        SFPTxEna : out std_logic_vector(3 downto 0 );
        GTP4_TX_N : out std_logic;
        GTP4_RX_N : in std_logic;
        GTP3_TX_P : out std_logic;
        GTP3_RX_P : in std_logic;
        gtp_RdData : out std_logic_vector(15 downto 0 );
        GTP3_TX_N : out std_logic;
        GTP3_RX_N : in std_logic;
        RefClk : in std_logic;
        gtp_RdMem : in std_logic;
        GTP2_TX_P : out std_logic;
        GTP2_RX_P : in std_logic;
        GTP2_TX_N : out std_logic;
        GTP2_RX_N : in std_logic;
        SFPLOS_N : in std_logic_vector(3 downto 0 );
        GTP1_TX_P : out std_logic;
        GTP1_RX_P : in std_logic;
        RxMarker1 : out std_logic;
        RxMarker2 : out std_logic;
        RxMarker3 : out std_logic;
        RxMarker4 : out std_logic;
        GTP1_TX_N : out std_logic;
        GTP1_RX_N : in std_logic;
        gtp_RdDone : out std_logic;
        SFPPrsnt : in std_logic_vector(3 downto 0 );
        ClocksOk : in std_logic;
        Faults : out std_logic;
        TxData1 : in std_logic_vector(15 downto 0 );
        TxData2 : in std_logic_vector(15 downto 0 );
        TxData3 : in std_logic_vector(15 downto 0 );
        TxData4 : in std_logic_vector(15 downto 0 );
        UserClk : in std_logic;
        TxMarker1 : in std_logic;
        TxMarker2 : in std_logic;
        TxMarker3 : in std_logic;
        TxMarker4 : in std_logic;
        gtp_WrMem : in std_logic;
        ClearFaults : in std_logic;
        PLLLock : out std_logic
        );


end GTPBlock;


architecture GTPBlock of GTPBlock is

  signal Loc_TxMkr2 : std_logic;
  signal Loc_TxMkr3 : std_logic;
  signal Loc_TxMkr4 : std_logic;
  signal PRBSTxEna1 : std_logic_vector(2 downto 0 );
  signal PRBSTxEna2 : std_logic_vector(2 downto 0 );
  signal PRBSTxEna3 : std_logic_vector(2 downto 0 );
  signal GTPRxRst1 : std_logic;
  signal PRBSTxEna4 : std_logic_vector(2 downto 0 );
  signal GTPRxRst2 : std_logic;
  signal GTPRxRst3 : std_logic;
  signal GTPRxRst4 : std_logic;
  signal Loc_PRBSCntReset1 : std_logic;
  signal Loc_PRBSCntReset2 : std_logic;
  signal Loc_PRBSCntReset3 : std_logic;
  signal Loc_PRBSCntReset4 : std_logic;
  signal GTPRxRstDone1 : std_logic;
  signal GTPRxRstDone2 : std_logic;
  signal GTPRxRstDone3 : std_logic;
  signal GTPRxRstDone4 : std_logic;
  signal GTPTxRstDone1 : std_logic;
  signal GTPTxRstDone2 : std_logic;
  signal GTPPRBSErrCnt1 : std_logic_vector(15 downto 0 );
  signal GTPTxRstDone3 : std_logic;
  signal GTPTxRstDone4 : std_logic;
  signal GTPPRBSErrCnt2 : std_logic_vector(15 downto 0 );
  signal GTPPRBSErrCnt3 : std_logic_vector(15 downto 0 );
  signal GTPPRBSErrCnt4 : std_logic_vector(15 downto 0 );
  signal GTPAlign1 : std_logic;
  signal GTPAlign2 : std_logic;
  signal GTPAlign3 : std_logic;
  signal GTPAlign4 : std_logic;
  signal SFPLOS : std_logic_vector(3 downto 0 );
  signal Loc_TxStr1 : std_logic;
  signal Loc_TxStr2 : std_logic;
  signal gtp3_faults_dispError : std_logic;
  signal Loc_TxStr3 : std_logic;
  signal Loc_TxStr4 : std_logic;
  signal Loc_RxBufRst1 : std_logic;
  signal Loc_RxBufRst2 : std_logic;
  signal Loc_RxBufRst3 : std_logic;
  signal Loc_RxBufRst4 : std_logic;
  signal gtp4_faults_notInTable : std_logic;
  signal GTPRxPMARst1 : std_logic;
  signal GTPRxPMARst2 : std_logic;
  signal GTPRxPMARst3 : std_logic;
  signal GTPRxPMARst4 : std_logic;
  signal Loc_RxClkCorCnt1 : std_logic_vector(1 downto 0 );
  signal Loc_RxClkCorCnt2 : std_logic_vector(1 downto 0 );
  signal Loc_RxClkCorCnt3 : std_logic_vector(1 downto 0 );
  signal Loc_RxClkCorCnt4 : std_logic_vector(1 downto 0 );
  signal GTPPRBSEna1 : std_logic;
  signal GTPPRBSEna2 : std_logic;
  signal GTPPRBSEna3 : std_logic;
  signal GTPPRBSEna4 : std_logic;
  signal GTPFaultsNotInTable1 : std_logic;
  signal GTPFaultsNotInTable2 : std_logic;
  signal GTPFaultsNotInTable3 : std_logic;
  signal GTPFaultsNotInTable4 : std_logic;
  signal PRBSRxEna1 : std_logic_vector(2 downto 0 );
  signal PRBSRxEna2 : std_logic_vector(2 downto 0 );
  signal PRBSRxEna3 : std_logic_vector(2 downto 0 );
  signal PRBSRxEna4 : std_logic_vector(2 downto 0 );
  signal Loc_TxRst1 : std_logic;
  signal GTPFaultsDispError1 : std_logic;
  signal Loc_TxRst2 : std_logic;
  signal GTPFaultsDispError2 : std_logic;
  signal Loc_TxRst3 : std_logic;
  signal GTPFaultsDispError3 : std_logic;
  signal GTPFaultsDispError4 : std_logic;
  signal Loc_TxRst4 : std_logic;
  signal gtp1_faults_notInTable : std_logic;
  signal Loc_PRBSErrCnt1 : std_logic_vector(15 downto 0 );
  signal Loc_PRBSErrCnt2 : std_logic_vector(15 downto 0 );
  signal Loc_PRBSErrCnt3 : std_logic_vector(15 downto 0 );
  signal Loc_PRBSErrCnt4 : std_logic_vector(15 downto 0 );
  signal TxTestData : std_logic_vector(15 downto 0 );
  signal GTPFaultsSRFF1 : std_logic_vector(15 downto 0 );
  signal GTPFaultsSRFF2 : std_logic_vector(15 downto 0 );
  signal GTPFaultsSRFF3 : std_logic_vector(15 downto 0 );
  signal Loc_Align1 : std_logic;
  signal GTPFaultsSRFF4 : std_logic_vector(15 downto 0 );
  signal Loc_Align2 : std_logic;
  signal Rst_UserClk : std_logic;
  signal Loc_Align3 : std_logic;
  signal Loc_Align4 : std_logic;
  signal GTPPRBSCntReset1 : std_logic;
  signal Loc_FaultsDispError1 : std_logic;
  signal GTPPRBSCntReset2 : std_logic;
  signal GTPPRBSForce1 : std_logic;
  signal GTPPRBSCntReset3 : std_logic;
  signal Loc_FaultsDispError2 : std_logic;
  signal GTPPRBSForce2 : std_logic;
  signal GTPPRBSCntReset4 : std_logic;
  signal GTPPRBSForce3 : std_logic;
  signal Loc_FaultsDispError3 : std_logic;
  signal GTPPRBSForce4 : std_logic;
  signal Loc_FaultsDispError4 : std_logic;
  signal gtp1_faults_dispError : std_logic;
  signal PLLRst : std_logic;
  signal Loc_RxRst1 : std_logic;
  signal Loc_RxRst2 : std_logic;
  signal Loc_RxRst3 : std_logic;
  signal Loc_RxRst4 : std_logic;
  signal Loc_FaultsNotInTable1 : std_logic;
  signal Loc_FaultsNotInTable2 : std_logic;
  signal Loc_FaultsNotInTable3 : std_logic;
  signal Loc_FaultsNotInTable4 : std_logic;
  signal Loc_TxPCSRst1 : std_logic;
  signal Loc_TxPCSRst2 : std_logic;
  signal Loc_TxPCSRst3 : std_logic;
  signal Loc_TxPCSRst4 : std_logic;
  signal Loc_RxBufStatus1 : std_logic_vector(2 downto 0 );
  signal Loc_RxBufStatus2 : std_logic_vector(2 downto 0 );
  signal GTPTxPCSRst1 : std_logic;
  signal SFPLOS_UserClk : std_logic_vector(3 downto 0 );
  signal Loc_RxBufStatus3 : std_logic_vector(2 downto 0 );
  signal GTPTxPCSRst2 : std_logic;
  signal Loc_RxBufStatus4 : std_logic_vector(2 downto 0 );
  signal GTPTxPCSRst3 : std_logic;
  signal GTPTxPCSRst4 : std_logic;
  signal ClocksOk_UserClk : std_logic;
  signal TxTestStr : std_logic;
  signal Loc_PRBSForce1 : std_logic;
  signal Loc_PRBSForce2 : std_logic;
  signal Loc_PRBSForce3 : std_logic;
  signal Loc_PRBSForce4 : std_logic;
  signal gtp2_faults_notInTable : std_logic;
  signal GTPIsAligned1 : std_logic;
  signal GTPIsAligned2 : std_logic;
  signal GTPIsAligned3 : std_logic;
  signal GTPIsAligned4 : std_logic;
  signal Loc_TxPMARst1 : std_logic;
  signal Loc_TxPMARst2 : std_logic;
  signal Loc_TxPMARst3 : std_logic;
  signal Loc_TxPMARst4 : std_logic;
  signal SFPOK : std_logic_vector(3 downto 0 );
  signal TxTestMkr : std_logic;
  signal GTPRxBufStatus1 : std_logic_vector(2 downto 0 );
  signal GTPRxBufStatus2 : std_logic_vector(2 downto 0 );
  signal GTPRxBufStatus3 : std_logic_vector(2 downto 0 );
  signal GTPRxBufStatus4 : std_logic_vector(2 downto 0 );
  signal Loc_RxPCSRst1 : std_logic;
  signal Loc_RxPCSRst2 : std_logic;
  signal Loc_RxPCSRst3 : std_logic;
  signal Loc_RxPCSRst4 : std_logic;
  signal SFPTxFault : std_logic_vector(3 downto 0 );
  signal Loc_TxData1 : std_logic_vector(15 downto 0 );
  signal Loc_TxData2 : std_logic_vector(15 downto 0 );
  signal Loc_TxData3 : std_logic_vector(15 downto 0 );
  signal Loc_TxData4 : std_logic_vector(15 downto 0 );
  signal gtp4_faults_dispError : std_logic;
  signal GTPRxPCSRst1 : std_logic;
  signal GTPRxPCSRst2 : std_logic;
  signal GTPRxPCSRst3 : std_logic;
  signal GTPRxPCSRst4 : std_logic;
  signal Loc_TxBufStatus1 : std_logic_vector(1 downto 0 );
  signal Loc_TxBufStatus2 : std_logic_vector(1 downto 0 );
  signal Loc_TxBufStatus3 : std_logic_vector(1 downto 0 );
  signal GTPTestEna1 : std_logic;
  signal Loc_TxBufStatus4 : std_logic_vector(1 downto 0 );
  signal GTPRxBufRst1 : std_logic;
  signal GTPTestEna2 : std_logic;
  signal GTPRxBufRst2 : std_logic;
  signal GTPTestEna3 : std_logic;
  signal GTPRxBufRst3 : std_logic;
  signal GTPTestEna4 : std_logic;
  signal GTPRxBufRst4 : std_logic;
  signal GTPTxRst1 : std_logic;
  signal GTPTxRst2 : std_logic;
  signal GTPTxRst3 : std_logic;
  signal GTPTxRst4 : std_logic;
  signal Loc_RxPMARst1 : std_logic;
  signal Loc_RxPMARst2 : std_logic;
  signal Loc_RxPMARst3 : std_logic;
  signal Loc_RxPMARst4 : std_logic;
  signal SFPPrsnt_UserClk : std_logic_vector(3 downto 0 );
  signal GTPRxClkCorCnt1 : std_logic_vector(1 downto 0 );
  signal GTPRxClkCorCnt2 : std_logic_vector(1 downto 0 );
  signal GTPRxClkCorCnt3 : std_logic_vector(1 downto 0 );
  signal GTPRxClkCorCnt4 : std_logic_vector(1 downto 0 );
  signal Loc_IsAligned1 : std_logic;
  signal Loc_IsAligned2 : std_logic;
  signal gtp3_faults_notInTable : std_logic;
  signal Loc_IsAligned3 : std_logic;
  signal Loc_IsAligned4 : std_logic;
  signal GTPFault1 : std_logic;
  signal GTPFault2 : std_logic;
  signal GTPFault3 : std_logic;
  signal GTPFault4 : std_logic;
  signal GTPTxBufStatus1 : std_logic_vector(1 downto 0 );
  signal GTPTxBufStatus2 : std_logic_vector(1 downto 0 );
  signal GTPTxBufStatus3 : std_logic_vector(1 downto 0 );
  signal GTPTxBufStatus4 : std_logic_vector(1 downto 0 );
  signal GTPTxPMARst1 : std_logic;
  signal GTPTxPMARst2 : std_logic;
  signal GTPTxPMARst3 : std_logic;
  signal GTPTxPMARst4 : std_logic;
  signal gtp2_faults_dispError : std_logic;
  signal Loc_RxRstDone1 : std_logic;
  signal Loc_RxRstDone2 : std_logic;
  signal Loc_RxRstDone3 : std_logic;
  signal Loc_RxRstDone4 : std_logic;
  signal Loc_TxRstDone1 : std_logic;
  signal Loc_TxRstDone2 : std_logic;
  signal Loc_TxRstDone3 : std_logic;
  signal Loc_TxRstDone4 : std_logic;
  signal Loc_TxMkr1 : std_logic;
  component FanIn32
      generic (
               X : std_logic := '0'
               );
      port (
            O : out std_logic_vector(31 downto 0 );
            I0 : in std_logic := X;
            I1 : in std_logic := X;
            I2 : in std_logic := X;
            I3 : in std_logic := X;
            I4 : in std_logic := X;
            I5 : in std_logic := X;
            I6 : in std_logic := X;
            I7 : in std_logic := X;
            I8 : in std_logic := X;
            I9 : in std_logic := X;
            I10 : in std_logic := X;
            I11 : in std_logic := X;
            I12 : in std_logic := X;
            I13 : in std_logic := X;
            I14 : in std_logic := X;
            I15 : in std_logic := X;
            I16 : in std_logic := X;
            I17 : in std_logic := X;
            I18 : in std_logic := X;
            I19 : in std_logic := X;
            I20 : in std_logic := X;
            I21 : in std_logic := X;
            I22 : in std_logic := X;
            I23 : in std_logic := X;
            I24 : in std_logic := X;
            I25 : in std_logic := X;
            I26 : in std_logic := X;
            I27 : in std_logic := X;
            I28 : in std_logic := X;
            I29 : in std_logic := X;
            I30 : in std_logic := X;
            I31 : in std_logic := X
            );
  end component;
  -- component RegCtrl_gtp
  --     port (
  --           Clk : in std_logic;
  --           Rst : in std_logic;
  --           VMEAddr : in std_logic_vector(5 downto 1 );
  --           VMERdData : out std_logic_vector(15 downto 0 );
  --           VMEWrData : in std_logic_vector(15 downto 0 );
  --           VMERdMem : in std_logic;
  --           VMEWrMem : in std_logic;
  --           VMERdDone : out std_logic;
  --           VMEWrDone : out std_logic;
  --           gtp1_control_sfpTxEna : out std_logic;
  --           gtp1_control_enaPRBS : out std_logic;
  --           gtp1_control_clrPRBSErrCnt : out std_logic;
  --           gtp1_control_forcePRBSErr : out std_logic;
  --           gtp1_control_testEna : out std_logic;
  --           gtp1_status_sfpPrsnt : in std_logic;
  --           gtp1_status_sfpLOS : in std_logic;
  --           gtp1_status_sfpTxFault : in std_logic;
  --           gtp1_status_rxRstDone : in std_logic;
  --           gtp1_status_txRstDone : in std_logic;
  --           gtp1_status_aligned : in std_logic;
  --           gtp1_status_rxClkCorCnt : in std_logic_vector(6 downto 5 );
  --           gtp1_status_rxBufStatus : in std_logic_vector(4 downto 2 );
  --           gtp1_status_txBufStatus : in std_logic_vector(1 downto 0 );
  --           gtp1_faults_dispError : in std_logic;
  --           gtp1_faults_notInTable : in std_logic;
  --           gtp1_faults_SRFF : out std_logic_vector(15 downto 0 );
  --           gtp1_faults_ClrSRFF : in std_logic;
  --           gtp1_reset_alignRx : out std_logic;
  --           gtp1_reset_rxRst : out std_logic;
  --           gtp1_reset_rxBufRst : out std_logic;
  --           gtp1_reset_rxPCSRst : out std_logic;
  --           gtp1_reset_rxPMARst : out std_logic;
  --           gtp1_reset_txRst : out std_logic;
  --           gtp1_reset_txPCSRst : out std_logic;
  --           gtp1_reset_txPMARst : out std_logic;
  --           gtp1_prbsErrCnt : in std_logic_vector(15 downto 0 );
  --           gtp2_control_sfpTxEna : out std_logic;
  --           gtp2_control_enaPRBS : out std_logic;
  --           gtp2_control_clrPRBSErrCnt : out std_logic;
  --           gtp2_control_forcePRBSErr : out std_logic;
  --           gtp2_control_testEna : out std_logic;
  --           gtp2_status_sfpPrsnt : in std_logic;
  --           gtp2_status_sfpLOS : in std_logic;
  --           gtp2_status_sfpTxFault : in std_logic;
  --           gtp2_status_rxRstDone : in std_logic;
  --           gtp2_status_txRstDone : in std_logic;
  --           gtp2_status_aligned : in std_logic;
  --           gtp2_status_rxClkCorCnt : in std_logic_vector(6 downto 5 );
  --           gtp2_status_rxBufStatus : in std_logic_vector(4 downto 2 );
  --           gtp2_status_txBufStatus : in std_logic_vector(1 downto 0 );
  --           gtp2_faults_dispError : in std_logic;
  --           gtp2_faults_notInTable : in std_logic;
  --           gtp2_faults_SRFF : out std_logic_vector(15 downto 0 );
  --           gtp2_faults_ClrSRFF : in std_logic;
  --           gtp2_reset_alignRx : out std_logic;
  --           gtp2_reset_rxRst : out std_logic;
  --           gtp2_reset_rxBufRst : out std_logic;
  --           gtp2_reset_rxPCSRst : out std_logic;
  --           gtp2_reset_rxPMARst : out std_logic;
  --           gtp2_reset_txRst : out std_logic;
  --           gtp2_reset_txPCSRst : out std_logic;
  --           gtp2_reset_txPMARst : out std_logic;
  --           gtp2_prbsErrCnt : in std_logic_vector(15 downto 0 );
  --           gtp3_control_sfpTxEna : out std_logic;
  --           gtp3_control_enaPRBS : out std_logic;
  --           gtp3_control_clrPRBSErrCnt : out std_logic;
  --           gtp3_control_forcePRBSErr : out std_logic;
  --           gtp3_control_testEna : out std_logic;
  --           gtp3_status_sfpPrsnt : in std_logic;
  --           gtp3_status_sfpLOS : in std_logic;
  --           gtp3_status_sfpTxFault : in std_logic;
  --           gtp3_status_rxRstDone : in std_logic;
  --           gtp3_status_txRstDone : in std_logic;
  --           gtp3_status_aligned : in std_logic;
  --           gtp3_status_rxClkCorCnt : in std_logic_vector(6 downto 5 );
  --           gtp3_status_rxBufStatus : in std_logic_vector(4 downto 2 );
  --           gtp3_status_txBufStatus : in std_logic_vector(1 downto 0 );
  --           gtp3_faults_dispError : in std_logic;
  --           gtp3_faults_notInTable : in std_logic;
  --           gtp3_faults_SRFF : out std_logic_vector(15 downto 0 );
  --           gtp3_faults_ClrSRFF : in std_logic;
  --           gtp3_reset_alignRx : out std_logic;
  --           gtp3_reset_rxRst : out std_logic;
  --           gtp3_reset_rxBufRst : out std_logic;
  --           gtp3_reset_rxPCSRst : out std_logic;
  --           gtp3_reset_rxPMARst : out std_logic;
  --           gtp3_reset_txRst : out std_logic;
  --           gtp3_reset_txPCSRst : out std_logic;
  --           gtp3_reset_txPMARst : out std_logic;
  --           gtp3_prbsErrCnt : in std_logic_vector(15 downto 0 );
  --           gtp4_control_sfpTxEna : out std_logic;
  --           gtp4_control_enaPRBS : out std_logic;
  --           gtp4_control_clrPRBSErrCnt : out std_logic;
  --           gtp4_control_forcePRBSErr : out std_logic;
  --           gtp4_control_testEna : out std_logic;
  --           gtp4_status_sfpPrsnt : in std_logic;
  --           gtp4_status_sfpLOS : in std_logic;
  --           gtp4_status_sfpTxFault : in std_logic;
  --           gtp4_status_rxRstDone : in std_logic;
  --           gtp4_status_txRstDone : in std_logic;
  --           gtp4_status_aligned : in std_logic;
  --           gtp4_status_rxClkCorCnt : in std_logic_vector(6 downto 5 );
  --           gtp4_status_rxBufStatus : in std_logic_vector(4 downto 2 );
  --           gtp4_status_txBufStatus : in std_logic_vector(1 downto 0 );
  --           gtp4_faults_dispError : in std_logic;
  --           gtp4_faults_notInTable : in std_logic;
  --           gtp4_faults_SRFF : out std_logic_vector(15 downto 0 );
  --           gtp4_faults_ClrSRFF : in std_logic;
  --           gtp4_reset_alignRx : out std_logic;
  --           gtp4_reset_rxRst : out std_logic;
  --           gtp4_reset_rxBufRst : out std_logic;
  --           gtp4_reset_rxPCSRst : out std_logic;
  --           gtp4_reset_rxPMARst : out std_logic;
  --           gtp4_reset_txRst : out std_logic;
  --           gtp4_reset_txPCSRst : out std_logic;
  --           gtp4_reset_txPMARst : out std_logic;
  --           gtp4_prbsErrCnt : in std_logic_vector(15 downto 0 )
  --           );
  -- end component;
  component GTP_A7
      generic (
               G_RATE : GTP_RATE_TYPE := GBPS_3_0
               );
      port (
            PRBSTxEna0 : in std_logic_vector(2 downto 0 );
            PRBSTxEna1 : in std_logic_vector(2 downto 0 );
            PRBSTxEna2 : in std_logic_vector(2 downto 0 );
            PRBSTxEna3 : in std_logic_vector(2 downto 0 );
            PRBSErrCnt0 : out std_logic_vector(15 downto 0 );
            PRBSErrCnt1 : out std_logic_vector(15 downto 0 );
            PRBSErrCnt2 : out std_logic_vector(15 downto 0 );
            PRBSErrCnt3 : out std_logic_vector(15 downto 0 );
            RxPMARst0 : in std_logic;
            RxPMARst1 : in std_logic;
            RxPMARst2 : in std_logic;
            RxPMARst3 : in std_logic;
            Rx0_N : in std_logic;
            Rx0_P : in std_logic;
            PLLRefClkLost : out std_logic;
            PRBSCntReset0 : in std_logic;
            PRBSCntReset1 : in std_logic;
            PRBSCntReset2 : in std_logic;
            PRBSCntReset3 : in std_logic;
            Valid0 : in std_logic;
            Valid1 : in std_logic;
            Valid2 : in std_logic;
            Valid3 : in std_logic;
            TxBufStatus0 : out std_logic_vector(1 downto 0 );
            TxBufStatus1 : out std_logic_vector(1 downto 0 );
            TxBufStatus2 : out std_logic_vector(1 downto 0 );
            TxBufStatus3 : out std_logic_vector(1 downto 0 );
            RxPCSRst0 : in std_logic;
            RxPCSRst1 : in std_logic;
            RxPCSRst2 : in std_logic;
            RxPCSRst3 : in std_logic;
            TxRst0 : in std_logic;
            TxRst1 : in std_logic;
            TxRst2 : in std_logic;
            TxRst3 : in std_logic;
            PRBSTxForce0 : in std_logic;
            PRBSTxForce1 : in std_logic;
            Tx0_N : out std_logic;
            PRBSTxForce2 : in std_logic;
            PRBSTxForce3 : in std_logic;
            Tx0_P : out std_logic;
            TxStr0 : in std_logic;
            TxStr1 : in std_logic;
            TxStr2 : in std_logic;
            TxStr3 : in std_logic;
            AlignRx0 : in std_logic;
            AlignRx1 : in std_logic;
            TxUserReady0 : in std_logic;
            AlignRx2 : in std_logic;
            TxUserReady1 : in std_logic;
            AlignRx3 : in std_logic;
            TxUserReady2 : in std_logic;
            TxUserReady3 : in std_logic;
            PRBSRxEna0 : in std_logic_vector(2 downto 0 );
            PRBSRxEna1 : in std_logic_vector(2 downto 0 );
            PRBSRxEna2 : in std_logic_vector(2 downto 0 );
            PRBSRxEna3 : in std_logic_vector(2 downto 0 );
            Rx2_N : in std_logic;
            IsAligned0 : out std_logic;
            IsAligned1 : out std_logic;
            IsAligned2 : out std_logic;
            Rx2_P : in std_logic;
            IsAligned3 : out std_logic;
            RxClkCorCnt0 : out std_logic_vector(1 downto 0 );
            RxClkCorCnt1 : out std_logic_vector(1 downto 0 );
            RxClkCorCnt2 : out std_logic_vector(1 downto 0 );
            RxClkCorCnt3 : out std_logic_vector(1 downto 0 );
            RxRstDone0 : out std_logic;
            Tx2_N : out std_logic;
            RxRstDone1 : out std_logic;
            RxRstDone2 : out std_logic;
            Tx2_P : out std_logic;
            RxRstDone3 : out std_logic;
            TxRstDone0 : out std_logic;
            TxRstDone1 : out std_logic;
            TxRstDone2 : out std_logic;
            TxRstDone3 : out std_logic;
            RxRst0 : in std_logic;
            RxRst1 : in std_logic;
            RxRst2 : in std_logic;
            RxRst3 : in std_logic;
            RxStr0 : out std_logic;
            RxStr1 : out std_logic;
            RxStr2 : out std_logic;
            RxStr3 : out std_logic;
            RxData0 : out std_logic_vector(15 downto 0 );
            RxData1 : out std_logic_vector(15 downto 0 );
            RxData2 : out std_logic_vector(15 downto 0 );
            RxData3 : out std_logic_vector(15 downto 0 );
            RxUserReady0 : in std_logic;
            RxUserReady1 : in std_logic;
            RxUserReady2 : in std_logic;
            Faults_DispError0 : out std_logic;
            RxUserReady3 : in std_logic;
            Faults_DispError1 : out std_logic;
            Faults_DispError2 : out std_logic;
            Faults_DispError3 : out std_logic;
            PLLRst : in std_logic;
            RxCommaMisalign0 : out std_logic;
            RxCommaMisalign1 : out std_logic;
            RxCommaMisalign2 : out std_logic;
            RxCommaMisalign3 : out std_logic;
            RxDoubleComma0 : out std_logic;
            RxDoubleComma1 : out std_logic;
            RxDoubleComma2 : out std_logic;
            RxDoubleComma3 : out std_logic;
            Faults_NotInTable0 : out std_logic;
            Faults_NotInTable1 : out std_logic;
            Faults_NotInTable2 : out std_logic;
            Faults_NotInTable3 : out std_logic;
            RxDataTest0 : out std_logic_vector(15 downto 0 );
            RxDataTest1 : out std_logic_vector(15 downto 0 );
            PLLLock : out std_logic;
            RxDataTest2 : out std_logic_vector(15 downto 0 );
            Rx1_N : in std_logic;
            RxDataTest3 : out std_logic_vector(15 downto 0 );
            Rx1_P : in std_logic;
            GTPClk : in std_logic;
            RxComma0 : out std_logic;
            RxComma1 : out std_logic;
            RxComma2 : out std_logic;
            RxComma3 : out std_logic;
            Tx1_N : out std_logic;
            Tx1_P : out std_logic;
            RxIsKTest0 : out std_logic_vector(1 downto 0 );
            RxIsKTest1 : out std_logic_vector(1 downto 0 );
            TxMkr0 : in std_logic;
            RxIsKTest2 : out std_logic_vector(1 downto 0 );
            TxMkr1 : in std_logic;
            RxIsKTest3 : out std_logic_vector(1 downto 0 );
            TxMkr2 : in std_logic;
            TxMkr3 : in std_logic;
            Rx3_N : in std_logic;
            Rx3_P : in std_logic;
            TxPMARst0 : in std_logic;
            TxPMARst1 : in std_logic;
            TxPMARst2 : in std_logic;
            TxPMARst3 : in std_logic;
            RxBufStatus0 : out std_logic_vector(2 downto 0 );
            RxBufStatus1 : out std_logic_vector(2 downto 0 );
            RxBufStatus2 : out std_logic_vector(2 downto 0 );
            RxBufStatus3 : out std_logic_vector(2 downto 0 );
            TxData0 : in std_logic_vector(15 downto 0 );
            TxData1 : in std_logic_vector(15 downto 0 );
            TxData2 : in std_logic_vector(15 downto 0 );
            TxData3 : in std_logic_vector(15 downto 0 );
            UserClk : in std_logic;
            TxPCSRst0 : in std_logic;
            TxPCSRst1 : in std_logic;
            TxPCSRst2 : in std_logic;
            TxPCSRst3 : in std_logic;
            Tx3_N : out std_logic;
            Tx3_P : out std_logic;
            RxBufRst0 : in std_logic;
            RxBufRst1 : in std_logic;
            RxBufRst2 : in std_logic;
            RxBufRst3 : in std_logic
            );
  end component;
  component PosEdge
      port (
            Clk : in std_logic;
            SigIn : in std_logic;
            PEdg : out std_logic
            );
  end component;
  component DffxN
      generic (
               N : INTEGER := 2
               );
      port (
            Clk : in std_logic;
            E : in std_logic := '1';
            E1 : in std_logic := '1';
            Rst : in std_logic;
            D : in std_logic;
            Q : out std_logic
            );
  end component;
  component GTP_InFF
      port (
            Clk : in std_logic;
            UserClk : in std_logic;
            PRBSEna_I : in std_logic;
            PRBSCntReset_I : in std_logic;
            PRBSForce_I : in std_logic;
            RxRst_I : in std_logic;
            TxRst_I : in std_logic;
            RxBufRst_I : in std_logic;
            Align_I : in std_logic;
            RxPMARst_I : in std_logic;
            RxPCSRst_I : in std_logic;
            TxPMARst_I : in std_logic;
            TxPCSRst_I : in std_logic;
            PRBSRxEna_O : out std_logic_vector(2 downto 0 );
            PRBSTxEna_O : out std_logic_vector(2 downto 0 );
            PRBSCntReset_O : out std_logic;
            PRBSForce_O : out std_logic;
            RxRst_O : out std_logic;
            TxRst_O : out std_logic;
            RxBufRst_O : out std_logic;
            Align_O : out std_logic;
            RxPMARst_O : out std_logic;
            RxPCSRst_O : out std_logic;
            TxPMARst_O : out std_logic;
            TxPCSRst_O : out std_logic
            );
  end component;
  component GTP_OutFF
      port (
            Clk : in std_logic;
            UserClk : in std_logic;
            PRBSErrCnt_I : in std_logic_vector(15 downto 0 );
            RxClkCorCnt_I : in std_logic_vector(1 downto 0 );
            RxBufStatus_I : in std_logic_vector(2 downto 0 );
            TxBufStatus_I : in std_logic_vector(1 downto 0 );
            IsAligned_I : in std_logic;
            FaultsDispError_I : in std_logic;
            FaultsNotInTable_I : in std_logic;
            RxRstDone_I : in std_logic;
            TxRstDone_I : in std_logic;
            PRBSErrCnt_O : out std_logic_vector(15 downto 0 );
            RxClkCorCnt_O : out std_logic_vector(1 downto 0 );
            RxBufStatus_O : out std_logic_vector(2 downto 0 );
            TxBufStatus_O : out std_logic_vector(1 downto 0 );
            IsAligned_O : out std_logic;
            FaultsDispError_O : out std_logic;
            FaultsNotInTable_O : out std_logic;
            RxRstDone_O : out std_logic;
            TxRstDone_O : out std_logic
            );
  end component;

  type visual_C28_intern_type is array (INTEGER range 2 - 1 downto 0 ) of
                                 std_logic_vector(4 - 1 downto 0 );
  type visual_C30_intern_type is array (INTEGER range 2 - 1 downto 0 ) of
                                 std_logic_vector(4 - 1 downto 0 );

  -- Start Configuration Specification
  -- ++ for all : FanIn32 use entity work.FanIn32(FanIn32);
  -- ++ for all : RegCtrl_gtp use entity work.RegCtrl_gtp(Gena);
  -- ++ for all : GTP_A7 use entity work.GTP_A7(GTP_A7);
  -- ++ for all : PosEdge use entity work.PosEdge(V1);
  -- ++ for all : DffxN use entity work.DffxN(DffxN);
  -- ++ for all : GTP_InFF use entity work.GTP_InFF(V1);
  -- ++ for all : GTP_OutFF use entity work.GTP_OutFF(V1);
  -- End Configuration Specification

begin

  C17: FanIn32
    port map (
              O => Debug(31 downto 0),
              I0 => open,
              I1 => GTPIsAligned1,
              I2 => GTPFaultsNotInTable1,
              I3 => open,
              I4 => GTPFaultsDispError1,
              I5 => open,
              I6 => open,
              I7 => open,
              I8 => open,
              I9 => GTPIsAligned2,
              I10 => GTPFaultsNotInTable2,
              I11 => open,
              I12 => GTPFaultsDispError2,
              I13 => open,
              I14 => open,
              I15 => open,
              I16 => open,
              I17 => GTPIsAligned3,
              I18 => GTPFaultsNotInTable3,
              I19 => open,
              I20 => GTPFaultsDispError3,
              I21 => open,
              I22 => open,
              I23 => open,
              I24 => open,
              I25 => GTPIsAligned4,
              I26 => GTPFaultsNotInTable4,
              I27 => open,
              I28 => GTPFaultsDispError4,
              I29 => open,
              I30 => open,
              I31 => open
              );
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
SFPTxEna(0)      <= '1';
GTPPRBSEna1      <= '0';
GTPPRBSCntReset1 <= '0';
GTPPRBSForce1    <= '0';
GTPTestEna1      <= '0';
--SFPPrsnt(0)      <= '1';
--SFPLOS(0)        <= '0';
SFPTxFault(0)    <= '0';
GTPAlign1        <= '0';
GTPRxRst1        <= Rst;
GTPRxBufRst1     <= '0';
GTPRxPCSRst1     <= '0';
GTPRxPMARst1     <= '0';
GTPTxRst1        <= Rst;
GTPTxPCSRst1     <= '0';
GTPTxPMARst1     <= '0';

SFPTxEna(1)      <= '1';
GTPPRBSEna2      <= '0';
GTPPRBSCntReset2 <= '0';
GTPPRBSForce2    <= '0';
GTPTestEna2      <= '0';
--SFPPrsnt(1)      <= '1';
--SFPLOS(1)        <= '0';
SFPTxFault(1)    <= '0';
GTPAlign2        <= '0';
GTPRxRst2        <= Rst;
GTPRxBufRst2     <= '0';
GTPRxPCSRst2     <= '0';
GTPRxPMARst2     <= '0';
GTPTxRst2        <= Rst;
GTPTxPCSRst2     <= '0';
GTPTxPMARst2     <= '0';

SFPTxEna(2)      <= '1';
GTPPRBSEna3      <= '0';
GTPPRBSCntReset3 <= '0';
GTPPRBSForce3    <= '0';
GTPTestEna3      <= '0';
--SFPPrsnt(2)      <= '1';
--SFPLOS(2)        <= '0';
SFPTxFault(2)    <= '0';
GTPAlign3        <= '0';
GTPRxRst3        <= Rst;
GTPRxBufRst3     <= '0';
GTPRxPCSRst3     <= '0';
GTPRxPMARst3     <= '0';
GTPTxRst3        <= Rst;
GTPTxPCSRst3     <= '0';
GTPTxPMARst3     <= '0';

SFPTxEna(3)      <= '1';
GTPPRBSEna4      <= '0';
GTPPRBSCntReset4 <= '0';
GTPPRBSForce4    <= '0';
GTPTestEna4      <= '0';
--SFPPrsnt(3)      <= '1';
--SFPLOS(3)        <= '0';
SFPTxFault(3)    <= '0';
GTPAlign4        <= '0';
GTPRxRst4        <= Rst;
GTPRxBufRst4     <= '0';
GTPRxPCSRst4     <= '0';
GTPRxPMARst4     <= '0';
GTPTxRst4        <= Rst;
GTPTxPCSRst4     <= '0';
GTPTxPMARst4     <= '0';
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  -- B_GTPRegs: RegCtrl_gtp
  --   port map (
  --             Clk => Clk,
  --             Rst => Rst,
  --             VMEAddr => gtp_Addr(5 downto 1),
  --             VMERdData => gtp_RdData(15 downto 0),
  --             VMEWrData => gtp_WrData(15 downto 0),
  --             VMERdMem => gtp_RdMem,
  --             VMEWrMem => gtp_WrMem,
  --             VMERdDone => gtp_RdDone,
  --             VMEWrDone => gtp_WrDone,
  --             gtp1_control_sfpTxEna => SFPTxEna(0),  -- 1
  --             gtp1_control_enaPRBS => GTPPRBSEna1,    -- 0
  --             gtp1_control_clrPRBSErrCnt => GTPPRBSCntReset1, -- 0
  --             gtp1_control_forcePRBSErr => GTPPRBSForce1, -- 0
  --             gtp1_control_testEna => GTPTestEna1, -- 0
  --             gtp1_status_sfpPrsnt => SFPPrsnt(0), -- 1
  --             gtp1_status_sfpLOS => SFPLOS(0), -- 0
  --             gtp1_status_sfpTxFault => SFPTxFault(0), -- 0
  --             gtp1_status_rxRstDone => GTPRxRstDone1,
  --             gtp1_status_txRstDone => GTPTxRstDone1,
  --             gtp1_status_aligned => GTPIsAligned1,
  --             gtp1_status_rxClkCorCnt => GTPRxClkCorCnt1(1 downto 0),
  --             gtp1_status_rxBufStatus => GTPRxBufStatus1(2 downto 0),
  --             gtp1_status_txBufStatus => GTPTxBufStatus1(1 downto 0),
  --             gtp1_faults_dispError => gtp1_faults_dispError,
  --             gtp1_faults_notInTable => gtp1_faults_notInTable,
  --             gtp1_faults_SRFF => GTPFaultsSRFF1(15 downto 0),
  --             gtp1_faults_ClrSRFF => ClearFaults,
  --             gtp1_reset_alignRx => GTPAlign1, -- 0
  --             gtp1_reset_rxRst => GTPRxRst1, -- reset
  --             gtp1_reset_rxBufRst => GTPRxBufRst1, --0
  --             gtp1_reset_rxPCSRst => GTPRxPCSRst1, --0
  --             gtp1_reset_rxPMARst => GTPRxPMARst1, --0
  --             gtp1_reset_txRst => GTPTxRst1, -- reset
  --             gtp1_reset_txPCSRst => GTPTxPCSRst1, --0
  --             gtp1_reset_txPMARst => GTPTxPMARst1, --0
  --             gtp1_prbsErrCnt => GTPPRBSErrCnt1(15 downto 0),
  --             gtp2_control_sfpTxEna => SFPTxEna(1),
  --             gtp2_control_enaPRBS => GTPPRBSEna2,
  --             gtp2_control_clrPRBSErrCnt => GTPPRBSCntReset2,
  --             gtp2_control_forcePRBSErr => GTPPRBSForce2,
  --             gtp2_control_testEna => GTPTestEna2,
  --             gtp2_status_sfpPrsnt => SFPPrsnt(1),
  --             gtp2_status_sfpLOS => SFPLOS(1),
  --             gtp2_status_sfpTxFault => SFPTxFault(1),
  --             gtp2_status_rxRstDone => GTPRxRstDone2,
  --             gtp2_status_txRstDone => GTPTxRstDone2,
  --             gtp2_status_aligned => GTPIsAligned2,
  --             gtp2_status_rxClkCorCnt => GTPRxClkCorCnt2(1 downto 0),
  --             gtp2_status_rxBufStatus => GTPRxBufStatus2(2 downto 0),
  --             gtp2_status_txBufStatus => GTPTxBufStatus2(1 downto 0),
  --             gtp2_faults_dispError => gtp2_faults_dispError,
  --             gtp2_faults_notInTable => gtp2_faults_notInTable,
  --             gtp2_faults_SRFF => GTPFaultsSRFF2(15 downto 0),
  --             gtp2_faults_ClrSRFF => ClearFaults,
  --             gtp2_reset_alignRx => GTPAlign2,
  --             gtp2_reset_rxRst => GTPRxRst2,
  --             gtp2_reset_rxBufRst => GTPRxBufRst2,
  --             gtp2_reset_rxPCSRst => GTPRxPCSRst2,
  --             gtp2_reset_rxPMARst => GTPRxPMARst2,
  --             gtp2_reset_txRst => GTPTxRst2,
  --             gtp2_reset_txPCSRst => GTPTxPCSRst2,
  --             gtp2_reset_txPMARst => GTPTxPMARst2,
  --             gtp2_prbsErrCnt => GTPPRBSErrCnt2(15 downto 0),
  --             gtp3_control_sfpTxEna => SFPTxEna(2),
  --             gtp3_control_enaPRBS => GTPPRBSEna3,
  --             gtp3_control_clrPRBSErrCnt => GTPPRBSCntReset3,
  --             gtp3_control_forcePRBSErr => GTPPRBSForce3,
  --             gtp3_control_testEna => GTPTestEna3,
  --             gtp3_status_sfpPrsnt => SFPPrsnt(2),
  --             gtp3_status_sfpLOS => SFPLOS(2),
  --             gtp3_status_sfpTxFault => SFPTxFault(2),
  --             gtp3_status_rxRstDone => GTPRxRstDone3,
  --             gtp3_status_txRstDone => GTPTxRstDone3,
  --             gtp3_status_aligned => GTPIsAligned3,
  --             gtp3_status_rxClkCorCnt => GTPRxClkCorCnt3(1 downto 0),
  --             gtp3_status_rxBufStatus => GTPRxBufStatus3(2 downto 0),
  --             gtp3_status_txBufStatus => GTPTxBufStatus3(1 downto 0),
  --             gtp3_faults_dispError => gtp3_faults_dispError,
  --             gtp3_faults_notInTable => gtp3_faults_notInTable,
  --             gtp3_faults_SRFF => GTPFaultsSRFF3(15 downto 0),
  --             gtp3_faults_ClrSRFF => ClearFaults,
  --             gtp3_reset_alignRx => GTPAlign3,
  --             gtp3_reset_rxRst => GTPRxRst3,
  --             gtp3_reset_rxBufRst => GTPRxBufRst3,
  --             gtp3_reset_rxPCSRst => GTPRxPCSRst3,
  --             gtp3_reset_rxPMARst => GTPRxPMARst3,
  --             gtp3_reset_txRst => GTPTxRst3,
  --             gtp3_reset_txPCSRst => GTPTxPCSRst3,
  --             gtp3_reset_txPMARst => GTPTxPMARst3,
  --             gtp3_prbsErrCnt => GTPPRBSErrCnt3(15 downto 0),
  --             gtp4_control_sfpTxEna => SFPTxEna(3),
  --             gtp4_control_enaPRBS => GTPPRBSEna4,
  --             gtp4_control_clrPRBSErrCnt => GTPPRBSCntReset4,
  --             gtp4_control_forcePRBSErr => GTPPRBSForce4,
  --             gtp4_control_testEna => GTPTestEna4,
  --             gtp4_status_sfpPrsnt => SFPPrsnt(3),
  --             gtp4_status_sfpLOS => SFPLOS(3),
  --             gtp4_status_sfpTxFault => SFPTxFault(3),
  --             gtp4_status_rxRstDone => GTPRxRstDone4,
  --             gtp4_status_txRstDone => GTPTxRstDone4,
  --             gtp4_status_aligned => GTPIsAligned4,
  --             gtp4_status_rxClkCorCnt => GTPRxClkCorCnt4(1 downto 0),
  --             gtp4_status_rxBufStatus => GTPRxBufStatus4(2 downto 0),
  --             gtp4_status_txBufStatus => GTPTxBufStatus4(1 downto 0),
  --             gtp4_faults_dispError => gtp4_faults_dispError,
  --             gtp4_faults_notInTable => gtp4_faults_notInTable,
  --             gtp4_faults_SRFF => GTPFaultsSRFF4(15 downto 0),
  --             gtp4_faults_ClrSRFF => ClearFaults,
  --             gtp4_reset_alignRx => GTPAlign4,
  --             gtp4_reset_rxRst => GTPRxRst4,
  --             gtp4_reset_rxBufRst => GTPRxBufRst4,
  --             gtp4_reset_rxPCSRst => GTPRxPCSRst4,
  --             gtp4_reset_rxPMARst => GTPRxPMARst4,
  --             gtp4_reset_txRst => GTPTxRst4,
  --             gtp4_reset_txPCSRst => GTPTxPCSRst4,
  --             gtp4_reset_txPMARst => GTPTxPMARst4,
  --             gtp4_prbsErrCnt => GTPPRBSErrCnt4(15 downto 0)
  --             );

  B_GTP_TILE: GTP_A7
    generic map (G_RATE
                 )
    port map (
              PRBSTxEna0 => PRBSTxEna1(2 downto 0),
              PRBSTxEna1 => PRBSTxEna2(2 downto 0),
              PRBSTxEna2 => PRBSTxEna3(2 downto 0),
              PRBSTxEna3 => PRBSTxEna4(2 downto 0),
              PRBSErrCnt0 => Loc_PRBSErrCnt1(15 downto 0),
              PRBSErrCnt1 => Loc_PRBSErrCnt2(15 downto 0),
              PRBSErrCnt2 => Loc_PRBSErrCnt3(15 downto 0),
              PRBSErrCnt3 => Loc_PRBSErrCnt4(15 downto 0),
              RxPMARst0 => Loc_RxPMARst1,
              RxPMARst1 => Loc_RxPMARst2,
              RxPMARst2 => Loc_RxPMARst3,
              RxPMARst3 => Loc_RxPMARst4,
              Rx0_N => GTP1_RX_N,
              Rx0_P => GTP1_RX_P,
              PLLRefClkLost => open,
              PRBSCntReset0 => Loc_PRBSCntReset1,
              PRBSCntReset1 => Loc_PRBSCntReset2,
              PRBSCntReset2 => Loc_PRBSCntReset3,
              PRBSCntReset3 => Loc_PRBSCntReset4,
              Valid0 => SFPOK(0),
              Valid1 => SFPOK(1),
              Valid2 => SFPOK(2),
              Valid3 => SFPOK(3),
              TxBufStatus0 => Loc_TxBufStatus1(1 downto 0),
              TxBufStatus1 => Loc_TxBufStatus2(1 downto 0),
              TxBufStatus2 => Loc_TxBufStatus3(1 downto 0),
              TxBufStatus3 => Loc_TxBufStatus4(1 downto 0),
              RxPCSRst0 => Loc_RxPCSRst1,
              RxPCSRst1 => Loc_RxPCSRst2,
              RxPCSRst2 => Loc_RxPCSRst3,
              RxPCSRst3 => Loc_RxPCSRst4,
              TxRst0 => Loc_TxRst1,
              TxRst1 => Loc_TxRst2,
              TxRst2 => Loc_TxRst3,
              TxRst3 => Loc_TxRst4,
              PRBSTxForce0 => Loc_PRBSForce1,
              PRBSTxForce1 => Loc_PRBSForce2,
              Tx0_N => GTP1_TX_N,
              PRBSTxForce2 => Loc_PRBSForce3,
              PRBSTxForce3 => Loc_PRBSForce4,
              Tx0_P => GTP1_TX_P,
              TxStr0 => Loc_TxStr1,
              TxStr1 => Loc_TxStr2,
              TxStr2 => Loc_TxStr3,
              TxStr3 => Loc_TxStr4,
              AlignRx0 => Loc_Align1,
              AlignRx1 => Loc_Align2,
              TxUserReady0 => ClocksOk_UserClk,
              AlignRx2 => Loc_Align3,
              TxUserReady1 => ClocksOk_UserClk,
              AlignRx3 => Loc_Align4,
              TxUserReady2 => ClocksOk_UserClk,
              TxUserReady3 => ClocksOk_UserClk,
              PRBSRxEna0 => PRBSRxEna1(2 downto 0),
              PRBSRxEna1 => PRBSRxEna2(2 downto 0),
              PRBSRxEna2 => PRBSRxEna3(2 downto 0),
              PRBSRxEna3 => PRBSRxEna4(2 downto 0),
              Rx2_N => GTP3_RX_N,
              IsAligned0 => Loc_IsAligned1,
              IsAligned1 => Loc_IsAligned2,
              IsAligned2 => Loc_IsAligned3,
              Rx2_P => GTP3_RX_P,
              IsAligned3 => Loc_IsAligned4,
              RxClkCorCnt0 => Loc_RxClkCorCnt1(1 downto 0),
              RxClkCorCnt1 => Loc_RxClkCorCnt2(1 downto 0),
              RxClkCorCnt2 => Loc_RxClkCorCnt3(1 downto 0),
              RxClkCorCnt3 => Loc_RxClkCorCnt4(1 downto 0),
              RxRstDone0 => Loc_RxRstDone1,
              Tx2_N => GTP3_TX_N,
              RxRstDone1 => Loc_RxRstDone2,
              RxRstDone2 => Loc_RxRstDone3,
              Tx2_P => GTP3_TX_P,
              RxRstDone3 => Loc_RxRstDone4,
              TxRstDone0 => Loc_TxRstDone1,
              TxRstDone1 => Loc_TxRstDone2,
              TxRstDone2 => Loc_TxRstDone3,
              TxRstDone3 => Loc_TxRstDone4,
              RxRst0 => Loc_RxRst1,
              RxRst1 => Loc_RxRst2,
              RxRst2 => Loc_RxRst3,
              RxRst3 => Loc_RxRst4,
              RxStr0 => RxStr1,
              RxStr1 => RxStr2,
              RxStr2 => RxStr3,
              RxStr3 => RxStr4,
              RxData0 => RxData1(15 downto 0),
              RxData1 => RxData2(15 downto 0),
              RxData2 => RxData3(15 downto 0),
              RxData3 => RxData4(15 downto 0),
              RxUserReady0 => ClocksOk_UserClk,
              RxUserReady1 => ClocksOk_UserClk,
              RxUserReady2 => ClocksOk_UserClk,
              Faults_DispError0 => Loc_FaultsDispError1,
              RxUserReady3 => ClocksOk_UserClk,
              Faults_DispError1 => Loc_FaultsDispError2,
              Faults_DispError2 => Loc_FaultsDispError3,
              Faults_DispError3 => Loc_FaultsDispError4,
              PLLRst => PLLRst,
              RxCommaMisalign0 => open,
              RxCommaMisalign1 => open,
              RxCommaMisalign2 => open,
              RxCommaMisalign3 => open,
              RxDoubleComma0 => RxMarker1,
              RxDoubleComma1 => RxMarker2,
              RxDoubleComma2 => RxMarker3,
              RxDoubleComma3 => RxMarker4,
              Faults_NotInTable0 => Loc_FaultsNotInTable1,
              Faults_NotInTable1 => Loc_FaultsNotInTable2,
              Faults_NotInTable2 => Loc_FaultsNotInTable3,
              Faults_NotInTable3 => Loc_FaultsNotInTable4,
              RxDataTest0 => open,
              RxDataTest1 => open,
              PLLLock => PLLLock,
              RxDataTest2 => open,
              Rx1_N => GTP2_RX_N,
              RxDataTest3 => open,
              Rx1_P => GTP2_RX_P,
              GTPClk => RefClk,
              RxComma0 => open,
              RxComma1 => open,
              RxComma2 => open,
              RxComma3 => open,
              Tx1_N => GTP2_TX_N,
              Tx1_P => GTP2_TX_P,
              RxIsKTest0 => open,
              RxIsKTest1 => open,
              TxMkr0 => Loc_TxMkr1,
              RxIsKTest2 => open,
              TxMkr1 => Loc_TxMkr2,
              RxIsKTest3 => open,
              TxMkr2 => Loc_TxMkr3,
              TxMkr3 => Loc_TxMkr4,
              Rx3_N => GTP4_RX_N,
              Rx3_P => GTP4_RX_P,
              TxPMARst0 => Loc_TxPMARst1,
              TxPMARst1 => Loc_TxPMARst2,
              TxPMARst2 => Loc_TxPMARst3,
              TxPMARst3 => Loc_TxPMARst4,
              RxBufStatus0 => Loc_RxBufStatus1(2 downto 0),
              RxBufStatus1 => Loc_RxBufStatus2(2 downto 0),
              RxBufStatus2 => Loc_RxBufStatus3(2 downto 0),
              RxBufStatus3 => Loc_RxBufStatus4(2 downto 0),
              TxData0 => Loc_TxData1(15 downto 0),
              TxData1 => Loc_TxData2(15 downto 0),
              TxData2 => Loc_TxData3(15 downto 0),
              TxData3 => Loc_TxData4(15 downto 0),
              UserClk => UserClk,
              TxPCSRst0 => Loc_TxPCSRst1,
              TxPCSRst1 => Loc_TxPCSRst2,
              TxPCSRst2 => Loc_TxPCSRst3,
              TxPCSRst3 => Loc_TxPCSRst4,
              Tx3_N => GTP4_TX_N,
              Tx3_P => GTP4_TX_P,
              RxBufRst0 => Loc_RxBufRst1,
              RxBufRst1 => Loc_RxBufRst2,
              RxBufRst2 => Loc_RxBufRst3,
              RxBufRst3 => Loc_RxBufRst4
              );

  B_ClocksOK_PEdge: PosEdge
    port map (
              Clk => UserClk,
              SigIn => ClocksOk_UserClk,
              PEdg => PLLRst
              );

  B_GTPRstSync: DffxN
    generic map (2
                 )
    port map (
              Clk => UserClk,
              E => open,
              E1 => open,
              Rst => '0',
              D => Rst,
              Q => Rst_UserClk
              );

  B_GTPClocksOkSync: DffxN
    generic map (2
                 )
    port map (
              Clk => UserClk,
              E => open,
              E1 => open,
              Rst => '0',
              D => ClocksOk,
              Q => ClocksOk_UserClk
              );

  B_GTPInFF_1: GTP_InFF
    port map (
              Clk => Clk,
              UserClk => UserClk,
              PRBSEna_I => GTPPRBSEna1,
              PRBSCntReset_I => GTPPRBSCntReset1,
              PRBSForce_I => GTPPRBSForce1,
              RxRst_I => GTPRxRst1,
              TxRst_I => GTPTxRst1,
              RxBufRst_I => GTPRxBufRst1,
              Align_I => GTPAlign1,
              RxPMARst_I => GTPRxPMARst1,
              RxPCSRst_I => GTPRxPCSRst1,
              TxPMARst_I => GTPTxPMARst1,
              TxPCSRst_I => GTPTxPCSRst1,
              PRBSRxEna_O => PRBSRxEna1(2 downto 0),
              PRBSTxEna_O => PRBSTxEna1(2 downto 0),
              PRBSCntReset_O => Loc_PRBSCntReset1,
              PRBSForce_O => Loc_PRBSForce1,
              RxRst_O => Loc_RxRst1,
              TxRst_O => Loc_TxRst1,
              RxBufRst_O => Loc_RxBufRst1,
              Align_O => Loc_Align1,
              RxPMARst_O => Loc_RxPMARst1,
              RxPCSRst_O => Loc_RxPCSRst1,
              TxPMARst_O => Loc_TxPMARst1,
              TxPCSRst_O => Loc_TxPCSRst1
              );

  B_GTPInFF_2: GTP_InFF
    port map (
              Clk => Clk,
              UserClk => UserClk,
              PRBSEna_I => GTPPRBSEna2,
              PRBSCntReset_I => GTPPRBSCntReset2,
              PRBSForce_I => GTPPRBSForce2,
              RxRst_I => GTPRxRst2,
              TxRst_I => GTPTxRst2,
              RxBufRst_I => GTPRxBufRst2,
              Align_I => GTPAlign2,
              RxPMARst_I => GTPRxPMARst2,
              RxPCSRst_I => GTPRxPCSRst2,
              TxPMARst_I => GTPTxPMARst2,
              TxPCSRst_I => GTPTxPCSRst2,
              PRBSRxEna_O => PRBSRxEna2(2 downto 0),
              PRBSTxEna_O => PRBSTxEna2(2 downto 0),
              PRBSCntReset_O => Loc_PRBSCntReset2,
              PRBSForce_O => Loc_PRBSForce2,
              RxRst_O => Loc_RxRst2,
              TxRst_O => Loc_TxRst2,
              RxBufRst_O => Loc_RxBufRst2,
              Align_O => Loc_Align2,
              RxPMARst_O => Loc_RxPMARst2,
              RxPCSRst_O => Loc_RxPCSRst2,
              TxPMARst_O => Loc_TxPMARst2,
              TxPCSRst_O => Loc_TxPCSRst2
              );

  B_GTPInFF_3: GTP_InFF
    port map (
              Clk => Clk,
              UserClk => UserClk,
              PRBSEna_I => GTPPRBSEna3,
              PRBSCntReset_I => GTPPRBSCntReset3,
              PRBSForce_I => GTPPRBSForce3,
              RxRst_I => GTPRxRst3,
              TxRst_I => GTPTxRst3,
              RxBufRst_I => GTPRxBufRst3,
              Align_I => GTPAlign3,
              RxPMARst_I => GTPRxPMARst3,
              RxPCSRst_I => GTPRxPCSRst3,
              TxPMARst_I => GTPTxPMARst3,
              TxPCSRst_I => GTPTxPCSRst3,
              PRBSRxEna_O => PRBSRxEna3(2 downto 0),
              PRBSTxEna_O => PRBSTxEna3(2 downto 0),
              PRBSCntReset_O => Loc_PRBSCntReset3,
              PRBSForce_O => Loc_PRBSForce3,
              RxRst_O => Loc_RxRst3,
              TxRst_O => Loc_TxRst3,
              RxBufRst_O => Loc_RxBufRst3,
              Align_O => Loc_Align3,
              RxPMARst_O => Loc_RxPMARst3,
              RxPCSRst_O => Loc_RxPCSRst3,
              TxPMARst_O => Loc_TxPMARst3,
              TxPCSRst_O => Loc_TxPCSRst3
              );

  B_GTPInFF_4: GTP_InFF
    port map (
              Clk => Clk,
              UserClk => UserClk,
              PRBSEna_I => GTPPRBSEna4,
              PRBSCntReset_I => GTPPRBSCntReset4,
              PRBSForce_I => GTPPRBSForce4,
              RxRst_I => GTPRxRst4,
              TxRst_I => GTPTxRst4,
              RxBufRst_I => GTPRxBufRst4,
              Align_I => GTPAlign4,
              RxPMARst_I => GTPRxPMARst4,
              RxPCSRst_I => GTPRxPCSRst4,
              TxPMARst_I => GTPTxPMARst4,
              TxPCSRst_I => GTPTxPCSRst4,
              PRBSRxEna_O => PRBSRxEna4(2 downto 0),
              PRBSTxEna_O => PRBSTxEna4(2 downto 0),
              PRBSCntReset_O => Loc_PRBSCntReset4,
              PRBSForce_O => Loc_PRBSForce4,
              RxRst_O => Loc_RxRst4,
              TxRst_O => Loc_TxRst4,
              RxBufRst_O => Loc_RxBufRst4,
              Align_O => Loc_Align4,
              RxPMARst_O => Loc_RxPMARst4,
              RxPCSRst_O => Loc_RxPCSRst4,
              TxPMARst_O => Loc_TxPMARst4,
              TxPCSRst_O => Loc_TxPCSRst4
              );

  B_GTPOutFF_1: GTP_OutFF
    port map (
              Clk => Clk,
              UserClk => UserClk,
              PRBSErrCnt_I => Loc_PRBSErrCnt1(15 downto 0),
              RxClkCorCnt_I => Loc_RxClkCorCnt1(1 downto 0),
              RxBufStatus_I => Loc_RxBufStatus1(2 downto 0),
              TxBufStatus_I => Loc_TxBufStatus1(1 downto 0),
              IsAligned_I => Loc_IsAligned1,
              FaultsDispError_I => Loc_FaultsDispError1,
              FaultsNotInTable_I => Loc_FaultsNotInTable1,
              RxRstDone_I => Loc_RxRstDone1,
              TxRstDone_I => Loc_TxRstDone1,
              PRBSErrCnt_O => GTPPRBSErrCnt1(15 downto 0),
              RxClkCorCnt_O => GTPRxClkCorCnt1(1 downto 0),
              RxBufStatus_O => GTPRxBufStatus1(2 downto 0),
              TxBufStatus_O => GTPTxBufStatus1(1 downto 0),
              IsAligned_O => GTPIsAligned1,
              FaultsDispError_O => GTPFaultsDispError1,
              FaultsNotInTable_O => GTPFaultsNotInTable1,
              RxRstDone_O => GTPRxRstDone1,
              TxRstDone_O => GTPTxRstDone1
              );

  B_GTPOutFF_2: GTP_OutFF
    port map (
              Clk => Clk,
              UserClk => UserClk,
              PRBSErrCnt_I => Loc_PRBSErrCnt2(15 downto 0),
              RxClkCorCnt_I => Loc_RxClkCorCnt2(1 downto 0),
              RxBufStatus_I => Loc_RxBufStatus2(2 downto 0),
              TxBufStatus_I => Loc_TxBufStatus2(1 downto 0),
              IsAligned_I => Loc_IsAligned2,
              FaultsDispError_I => Loc_FaultsDispError2,
              FaultsNotInTable_I => Loc_FaultsNotInTable2,
              RxRstDone_I => Loc_RxRstDone2,
              TxRstDone_I => Loc_TxRstDone2,
              PRBSErrCnt_O => GTPPRBSErrCnt2(15 downto 0),
              RxClkCorCnt_O => GTPRxClkCorCnt2(1 downto 0),
              RxBufStatus_O => GTPRxBufStatus2(2 downto 0),
              TxBufStatus_O => GTPTxBufStatus2(1 downto 0),
              IsAligned_O => GTPIsAligned2,
              FaultsDispError_O => GTPFaultsDispError2,
              FaultsNotInTable_O => GTPFaultsNotInTable2,
              RxRstDone_O => GTPRxRstDone2,
              TxRstDone_O => GTPTxRstDone2
              );

  B_GTPOutFF_3: GTP_OutFF
    port map (
              Clk => Clk,
              UserClk => UserClk,
              PRBSErrCnt_I => Loc_PRBSErrCnt3(15 downto 0),
              RxClkCorCnt_I => Loc_RxClkCorCnt3(1 downto 0),
              RxBufStatus_I => Loc_RxBufStatus3(2 downto 0),
              TxBufStatus_I => Loc_TxBufStatus3(1 downto 0),
              IsAligned_I => Loc_IsAligned3,
              FaultsDispError_I => Loc_FaultsDispError3,
              FaultsNotInTable_I => Loc_FaultsNotInTable3,
              RxRstDone_I => Loc_RxRstDone3,
              TxRstDone_I => Loc_TxRstDone3,
              PRBSErrCnt_O => GTPPRBSErrCnt3(15 downto 0),
              RxClkCorCnt_O => GTPRxClkCorCnt3(1 downto 0),
              RxBufStatus_O => GTPRxBufStatus3(2 downto 0),
              TxBufStatus_O => GTPTxBufStatus3(1 downto 0),
              IsAligned_O => GTPIsAligned3,
              FaultsDispError_O => GTPFaultsDispError3,
              FaultsNotInTable_O => GTPFaultsNotInTable3,
              RxRstDone_O => GTPRxRstDone3,
              TxRstDone_O => GTPTxRstDone3
              );

  B_GTPOutFF_4: GTP_OutFF
    port map (
              Clk => Clk,
              UserClk => UserClk,
              PRBSErrCnt_I => Loc_PRBSErrCnt4(15 downto 0),
              RxClkCorCnt_I => Loc_RxClkCorCnt4(1 downto 0),
              RxBufStatus_I => Loc_RxBufStatus4(2 downto 0),
              TxBufStatus_I => Loc_TxBufStatus4(1 downto 0),
              IsAligned_I => Loc_IsAligned4,
              FaultsDispError_I => Loc_FaultsDispError4,
              FaultsNotInTable_I => Loc_FaultsNotInTable4,
              RxRstDone_I => Loc_RxRstDone4,
              TxRstDone_I => Loc_TxRstDone4,
              PRBSErrCnt_O => GTPPRBSErrCnt4(15 downto 0),
              RxClkCorCnt_O => GTPRxClkCorCnt4(1 downto 0),
              RxBufStatus_O => GTPRxBufStatus4(2 downto 0),
              TxBufStatus_O => GTPTxBufStatus4(1 downto 0),
              IsAligned_O => GTPIsAligned4,
              FaultsDispError_O => GTPFaultsDispError4,
              FaultsNotInTable_O => GTPFaultsNotInTable4,
              RxRstDone_O => GTPRxRstDone4,
              TxRstDone_O => GTPTxRstDone4
              );

   SFPOK(3 downto 0) <= (not SFPLOS_UserClk(3 downto 0))
             and ( SFPPrsnt_UserClk(3 downto 0));

  process (UserClk)
    variable visual_C28_int : visual_C28_intern_type;
  begin
    if (UserClk'event and UserClk = '1') then
      for visual_C28_index in 1 to 2 - 1 loop
        visual_C28_int(2 - visual_C28_index) := visual_C28_int(2 -
   visual_C28_index - 1);
      end loop;
      visual_C28_int(0) := (SFPLOS(3 downto 0));
     SFPLOS_UserClk(3 downto 0) <= visual_C28_int(2 - 1);

    end if;
  end process;

  process (UserClk)
    variable visual_C30_int : visual_C30_intern_type;
  begin
    if (UserClk'event and UserClk = '1') then
      for visual_C30_index in 1 to 2 - 1 loop
        visual_C30_int(2 - visual_C30_index) := visual_C30_int(2 -
   visual_C30_index - 1);
      end loop;
      visual_C30_int(0) := (SFPPrsnt(3 downto 0));
     SFPPrsnt_UserClk(3 downto 0) <= visual_C30_int(2 - 1);

    end if;
  end process;

  process (TxData1 , TxTestData , GTPTestEna1)
   begin
     case GTPTestEna1 is
       when '0' =>
         Loc_TxData1(15 downto 0) <=  TxData1(15 downto 0);
       when others =>
         Loc_TxData1(15 downto 0) <=  TxTestData(15 downto 0);
     end case;
   end process;

  process (TxStr1 , TxTestStr , GTPTestEna1)
   begin
     case GTPTestEna1 is
       when '0' =>
         Loc_TxStr1 <=  TxStr1;
       when others =>
         Loc_TxStr1 <=  TxTestStr;
     end case;
   end process;

  process (TxMarker1 , TxTestMkr , GTPTestEna1)
   begin
     case GTPTestEna1 is
       when '0' =>
         Loc_TxMkr1 <=  TxMarker1;
       when others =>
         Loc_TxMkr1 <=  TxTestMkr;
     end case;
   end process;

  process (TxData2 , TxTestData , GTPTestEna2)
   begin
     case GTPTestEna2 is
       when '0' =>
         Loc_TxData2(15 downto 0) <=  TxData2(15 downto 0);
       when others =>
         Loc_TxData2(15 downto 0) <=  TxTestData(15 downto 0);
     end case;
   end process;

  process (TxStr2 , TxTestStr , GTPTestEna2)
   begin
     case GTPTestEna2 is
       when '0' =>
         Loc_TxStr2 <=  TxStr2;
       when others =>
         Loc_TxStr2 <=  TxTestStr;
     end case;
   end process;

  process (TxMarker2 , TxTestMkr , GTPTestEna2)
   begin
     case GTPTestEna2 is
       when '0' =>
         Loc_TxMkr2 <=  TxMarker2;
       when others =>
         Loc_TxMkr2 <=  TxTestMkr;
     end case;
   end process;

  process (TxData3 , TxTestData , GTPTestEna3)
   begin
     case GTPTestEna3 is
       when '0' =>
         Loc_TxData3(15 downto 0) <=  TxData3(15 downto 0);
       when others =>
         Loc_TxData3(15 downto 0) <=  TxTestData(15 downto 0);
     end case;
   end process;

  process (TxStr3 , TxTestStr , GTPTestEna3)
   begin
     case GTPTestEna3 is
       when '0' =>
         Loc_TxStr3 <=  TxStr3;
       when others =>
         Loc_TxStr3 <=  TxTestStr;
     end case;
   end process;

  process (TxMarker3 , TxTestMkr , GTPTestEna3)
   begin
     case GTPTestEna3 is
       when '0' =>
         Loc_TxMkr3 <=  TxMarker3;
       when others =>
         Loc_TxMkr3 <=  TxTestMkr;
     end case;
   end process;

  process (TxData4 , TxTestData , GTPTestEna4)
   begin
     case GTPTestEna4 is
       when '0' =>
         Loc_TxData4(15 downto 0) <=  TxData4(15 downto 0);
       when others =>
         Loc_TxData4(15 downto 0) <=  TxTestData(15 downto 0);
     end case;
   end process;

  process (TxStr4 , TxTestStr , GTPTestEna4)
   begin
     case GTPTestEna4 is
       when '0' =>
         Loc_TxStr4 <=  TxStr4;
       when others =>
         Loc_TxStr4 <=  TxTestStr;
     end case;
   end process;

  process (TxMarker4 , TxTestMkr , GTPTestEna4)
   begin
     case GTPTestEna4 is
       when '0' =>
         Loc_TxMkr4 <=  TxMarker4;
       when others =>
         Loc_TxMkr4 <=  TxTestMkr;
     end case;
   end process;

  SFPLOS(3 downto 0) <= not (SFPLOS_N(3 downto 0));

  SFPTxFault(3 downto 0) <= not (SFPTxFault_N(3 downto 0));

   GTPFault1 <= ( GTPFaultsSRFF1(15)) or ( GTPFaultsSRFF1(14))
             or ( GTPFaultsSRFF1(13)) or ( GTPFaultsSRFF1(12))
             or ( GTPFaultsSRFF1(11)) or ( GTPFaultsSRFF1(10))
             or ( GTPFaultsSRFF1(9)) or ( GTPFaultsSRFF1(8))
             or ( GTPFaultsSRFF1(7)) or ( GTPFaultsSRFF1(6))
             or ( GTPFaultsSRFF1(5)) or ( GTPFaultsSRFF1(4))
             or ( GTPFaultsSRFF1(3)) or ( GTPFaultsSRFF1(2))
             or ( GTPFaultsSRFF1(1)) or ( GTPFaultsSRFF1(0));

   GTPFault2 <= ( GTPFaultsSRFF2(15)) or ( GTPFaultsSRFF2(14))
             or ( GTPFaultsSRFF2(13)) or ( GTPFaultsSRFF2(12))
             or ( GTPFaultsSRFF2(11)) or ( GTPFaultsSRFF2(10))
             or ( GTPFaultsSRFF2(9)) or ( GTPFaultsSRFF2(8))
             or ( GTPFaultsSRFF2(7)) or ( GTPFaultsSRFF2(6))
             or ( GTPFaultsSRFF2(5)) or ( GTPFaultsSRFF2(4))
             or ( GTPFaultsSRFF2(3)) or ( GTPFaultsSRFF2(2))
             or ( GTPFaultsSRFF2(1)) or ( GTPFaultsSRFF2(0));

   GTPFault3 <= ( GTPFaultsSRFF3(15)) or ( GTPFaultsSRFF3(14))
             or ( GTPFaultsSRFF3(13)) or ( GTPFaultsSRFF3(12))
             or ( GTPFaultsSRFF3(11)) or ( GTPFaultsSRFF3(10))
             or ( GTPFaultsSRFF3(9)) or ( GTPFaultsSRFF3(8))
             or ( GTPFaultsSRFF3(7)) or ( GTPFaultsSRFF3(6))
             or ( GTPFaultsSRFF3(5)) or ( GTPFaultsSRFF3(4))
             or ( GTPFaultsSRFF3(3)) or ( GTPFaultsSRFF3(2))
             or ( GTPFaultsSRFF3(1)) or ( GTPFaultsSRFF3(0));

   GTPFault4 <= ( GTPFaultsSRFF4(15)) or ( GTPFaultsSRFF4(14))
             or ( GTPFaultsSRFF4(13)) or ( GTPFaultsSRFF4(12))
             or ( GTPFaultsSRFF4(11)) or ( GTPFaultsSRFF4(10))
             or ( GTPFaultsSRFF4(9)) or ( GTPFaultsSRFF4(8))
             or ( GTPFaultsSRFF4(7)) or ( GTPFaultsSRFF4(6))
             or ( GTPFaultsSRFF4(5)) or ( GTPFaultsSRFF4(4))
             or ( GTPFaultsSRFF4(3)) or ( GTPFaultsSRFF4(2))
             or ( GTPFaultsSRFF4(1)) or ( GTPFaultsSRFF4(0));

   Faults <= ( GTPFault1) or ( GTPFault2) or ( GTPFault3) or ( GTPFault4);

   gtp1_faults_dispError <= ( GTPFaultsDispError1) and ( SFPPrsnt(0));

   gtp1_faults_notInTable <= ( GTPFaultsNotInTable1) and ( SFPPrsnt(0));

   gtp2_faults_dispError <= ( GTPFaultsDispError2) and ( SFPPrsnt(1));

   gtp2_faults_notInTable <= ( GTPFaultsNotInTable2) and ( SFPPrsnt(1));

   gtp3_faults_dispError <= ( GTPFaultsDispError3) and ( SFPPrsnt(2));

   gtp3_faults_notInTable <= ( GTPFaultsNotInTable3) and ( SFPPrsnt(2));

   gtp4_faults_dispError <= ( GTPFaultsDispError4) and ( SFPPrsnt(3));

   gtp4_faults_notInTable <= ( GTPFaultsNotInTable4) and ( SFPPrsnt(3));
end GTPBlock;