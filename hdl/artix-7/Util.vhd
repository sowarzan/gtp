----------------------------------------------------
--
--  Library Name :  CommonVisual
--  Unit    Name :  Util
--  Unit    Type :  Package
--
------------------------------------------------------
------------------------------------------
------------------------------------------
-- Date        : Wed Mar 05 14:09:41 2003
--
-- Author      : J.C. Molendijk / T. Levens
--
-- Company     : CERN, BE-RF
--
-- Description : Utility functions
--
------------------------------------------
------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package Util is

    -- Array types
    type natural_array_t is array (natural range <>) of natural;

    -- Conversion function
    function bool_to_stdlogic(InBool : boolean)
        return std_logic;

    function stdlogic_to_bool(InStdLogic : std_logic)
        return boolean;

    -- Binary logarithm -- NOTE: returns Log2(x+1)
    function Log2(x : natural)
        return natural;

    -- Functions to find max/min values
    function Max(x, y : natural)
        return natural;

    function Min(x, y : natural)
        return natural;

    -- Function to select between two values based on a boolean.
    function TFSel(t, f : natural; sel : boolean)
        return natural;

    -- Gray to Binary
    function F_Gray2Bin(Gray : std_logic_vector)
        return std_logic_vector;
	function F_Gray2Bin(Gray : unsigned)
		return unsigned;

    -- Binary to Gray
    function F_Bin2Gray(Bin : std_logic_vector)
        return std_logic_vector;
	function F_Bin2Gray(Bin : unsigned)
		return unsigned;


	-- Log2 size function. Returns ceil(log2(x)). NOTE: Works up to 64 bits.
	function F_log2 (A : natural)
		return natural;
end;



------------------------------------------
------------------------------------------
-- Date        : Wed Mar 05 14:13:30 2003
--
-- Author      : J.C. Molendijk / T. Levens
--
-- Company     : CERN, BE-RF
--
-- Description : Utility functions
--
------------------------------------------
------------------------------------------

package body  Util  is

    -- Conversion from boolean to std_logic
    --   True  => '1'
    --   False => '0'
    function bool_to_stdlogic(InBool : boolean) return std_logic is
        variable result: std_logic;
    begin
        if InBool = true
            then result := '1';
            else result := '0';
        end if;
        return result;
    end bool_to_stdlogic;

    -- Conversion from std_logic to boolean
    --   '1' => True
    --   '0' => False
    function stdlogic_to_bool(InStdLogic : std_logic) return boolean is
        variable result: boolean;
    begin
        if InStdLogic = '1'
            then result := true;
            else result := false;
        end if;
        return result;
    end stdlogic_to_bool;

    -- Calculates the binary logarithm of a number.
    --
    -- NOTE: Unlike the mathematical log operator, this function returns the
    --       number of bits required to store the number specified. So be
    --       careful! For example, while this behaves as expected:
    --
    --          Log2(1023) => 10
    --
    --       But 10 bits only gives you the range 0..1023, so to store the
    --       value 1024 you need 11 bits! So:
    --
    --          Log2(1024) => 11
    --
    function Log2(x : natural) return natural is
        variable xx : natural := x;
        variable z  : natural := 0;
    begin
        while true loop
            if xx = 0 then
                exit;
            elsif xx > 0 then
                z := z + 1;
                xx := xx / 2;
            end if;
        end loop;
        return z;
    end Log2;

    -- Finds the minimum or maximum of two naturals
    function Max(x, y : natural) return natural is
    begin
        if x > y then
            return x;
        else
            return y;
        end if;
    end Max;

    function Min(x, y : natural) return natural is
    begin
        if x < y then
            return x;
        else
            return y;
        end if;
    end Min;

    -- Selects an input based on sel.
    function TFSel(t, f : natural; sel : boolean) return natural is
    begin
        if sel = true then
            return t;
        else
            return f;
        end if;
    end TFSel;

    -- Gray to Binary
    function F_Gray2Bin(Gray : std_logic_vector) return std_logic_vector is
    variable Bin : std_logic_vector(Gray'left downto 0);
    begin
        for i in 0 to Gray'left loop
            Bin(i) := '0';
            for j in i to Gray'left loop
                Bin(i) := Bin(i) xor Gray(j);
            end loop;  -- j
        end loop;  -- i
        return Bin;
    end F_Gray2Bin;

	function F_Gray2Bin(Gray : unsigned) return unsigned is
	begin
		return unsigned(F_Gray2Bin(std_logic_vector(Gray)));
	end F_Gray2Bin;

    -- Binary to Gray
    function F_Bin2Gray(Bin : std_logic_vector) return std_logic_vector is
    begin
        return Bin(Bin'left) & (Bin(Bin'left-1 downto 0) xor Bin(Bin'left downto 1));
    end F_Bin2Gray;

	function F_Bin2Gray(Bin : unsigned ) return unsigned is
	begin
		return unsigned(F_Bin2Gray(std_logic_vector(Bin)));
	end F_Bin2Gray;

	-- Log2 size function. Returns ceil(log2(x))
	function F_log2 (A : natural) return natural is
	begin
		for I in 1 to 64 loop               -- Works for up to 64 bits
			if (2**I >= A) then
				return(I);
			end if;
		end loop;
		return(63);
	end function F_log2;

end Util;