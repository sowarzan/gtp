//////////////////////////////////////////////////////////////////////////////
// Title      : GTP_ReadRx
// Project    : GTP
// Date       : 2023-03-17
//////////////////////////////////////////////////////////////////////////////
// Docs: https://wikis.cern.ch/display/BERF/ObsBox+Gen1
//////////////////////////////////////////////////////////////////////////////
// File       : GTP_ReadRx.sv
// Author     : Sebastian Owarzany
// Company    : CERN
// Platform   : multiplatform
// Standard   : SystemVerilog IEEE 1800
//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2021-2022 CERN. All rights reserved.
//////////////////////////////////////////////////////////////////////////////

`default_nettype none

module GTP_ReadRx (
  input  wire        clk_150mhz,
  input  wire        rst_150mhz,
  input  wire        rxStr_i,
  input  wire        rxMrk_i,
  input  wire [15:0] rxData_i,
  output reg  [31:0] rxHeader_o,
  output reg  [31:0] rxSourceID_o,
  output reg  [31:0] rxBlockSize_o,
  output reg  [31:0] rxTurn_o,
  output reg  [31:0] rxTags_o,
  output reg  [31:0] rxCycle_o,
  output reg  [15:0] rxData_o,
  output reg         rxCrcOk_o
);

  reg [2:0] currentState, nextState; // Current FSM state

  localparam IDLE         = 3'b000, // Local parameters for FSM states
             GET_COMMA    = 3'b001,
             GET_HEADER   = 3'b010,
             GET_DATA     = 3'b011,
             GET_CRC      = 3'b100;

  logic incHeadCnt_l, // If high start increment HeadCnt_l
        incDataCnt_l, // If high start increment DataCnt_l
        dataValid_l; //

  wire [15:0] data_l;    // Data collected from input after async CDC FIFO

  logic [31:0] dataCnt;            // Counter for num16bData

  logic [4:0] headCnt_l = '{default:0}; // Counter for frame generation for GTP_LinkEncoder

  wire [31:0] header_l,
              sourceID_l,
              blockSize_l,
              turn_l,
              tags_l,
              cycle_l,
              crc_l,
              calcCrc_l,
              num16bData;

  reg getCRC_l,   // CRC add flag for FSM
      crcEna_l,   // Count CRC flag for CRC32_D16 module
      cntRst_l;   // Restart counters: headCnt_l and dataCnt

  always @( * ) begin : fsmTransactionProcess
      case( currentState )
        IDLE : begin
          nextState = GET_COMMA;
        end
        GET_COMMA : begin
          if ( !rxMrk_i && rxStr_i ) nextState = GET_HEADER;
        end
        GET_HEADER : begin
          if ( rxStr_i && ( headCnt_l >= 5'b01111 )) nextState = GET_DATA;
        end
        GET_DATA : begin
          if ( rxStr_i && (dataCnt >= num16bData )) nextState = GET_CRC;
        end
        GET_CRC : begin
          if ( rxMrk_i && !rxStr_i && (headCnt_l > 5'b10001 )) nextState = GET_COMMA;
        end
        default : nextState = GET_COMMA;
      endcase
  end

  always @( * ) begin  : fsmExecutionProcess
    case( currentState )
      IDLE : begin  // Default state after reset
        cntRst_l     <= 1'b1;
        incHeadCnt_l <= 1'b0;
        incDataCnt_l <= 1'b0;
        dataValid_l  <= 1'b0;
        getCRC_l     <= 1'b0;
      end
      GET_COMMA : begin // If we don't have enough amount of data in FIFO continuously send commas
        cntRst_l     <= 1'b1;
        incHeadCnt_l <= 1'b0;
        incDataCnt_l <= 1'b0;
        dataValid_l  <= 1'b0;
        getCRC_l     <= 1'b0;
      end
      GET_HEADER : begin // If we have enough amount of data in FIFO start sending data frame; data from "Header" to "Revolution-frequency"
        cntRst_l     <= 1'b0;
        incHeadCnt_l <= 1'b1;
        incDataCnt_l <= 1'b0;
        dataValid_l  <= 1'b0;
        getCRC_l     <= 1'b0;
      end
      GET_DATA : begin // After sending first part of dataframe send incoming user data collected in FIFO
        cntRst_l     <= 1'b0;
        incHeadCnt_l <= 1'b0;
        incDataCnt_l <= 1'b1;
        dataValid_l  <= 1'b1;
        getCRC_l     <= 1'b0;
      end
      GET_CRC : begin // If we send amount of user data definite by num16bData send calculated CRC
        cntRst_l     <= 1'b0;
        incHeadCnt_l <= 1'b1;
        incDataCnt_l <= 1'b0;
        dataValid_l  <= 1'b0;
        getCRC_l     <= 1'b1;
      end
    endcase
  end

  always_ff @( posedge clk_150mhz ) begin : fsmTransactionSyncProcess
    if ( rst_150mhz ) currentState <= IDLE;
    else              currentState <= nextState;
  end

  always_ff @( posedge clk_150mhz ) begin :  countersSyncProcess
    if (rst_150mhz || cntRst_l) begin
      headCnt_l <= '{default:0};
      dataCnt   <= '{default:0};
    end
    else begin
      if      ( incHeadCnt_l ) headCnt_l <= headCnt_l + 1;
      else if ( incDataCnt_l ) dataCnt   <= dataCnt   + 1;
    end
  end

  always_ff @( posedge clk_150mhz ) begin : outputsSyncProcess
    if ( rst_150mhz ) begin
      rxSourceID_o <= {32{1'b0}};
      rxCycle_o    <= {32{1'b0}};
      rxTurn_o     <= {32{1'b0}};
      rxTags_o     <= {32{1'b0}};
      rxData_o     <= {16{1'b0}};
      rxCrcOk_o    <= 1'b0;
    end else begin
      rxSourceID_o  <= sourceID_l;
      rxBlockSize_o <= blockSize_l;
      rxCycle_o     <= cycle_l;
      rxTurn_o      <= turn_l;
      rxTags_o      <= tags_l;
      // send data if proper
      if ( dataValid_l ) rxData_o <= rxData_i;
      else               rxData_o <= {16{1'b0}};
      // check CRC
      if ( crc_l == calcCrc_l ) rxCrcOk_o <= 1'b1;
      else                      rxCrcOk_o <= 1'b0;
    end
  end

  GTP_LinkDecoder GTP_LinkDecoder_inst(
  .headCnt_i(headCnt_l),
  .data_i(rxData_i),
  .header_o(header_l),
  .sourceID_o(sourceID_l),
  .blockSize_o(blockSize_l),
  .turn_o(turn_l),
  .tags_o(tags_l),
  .cycle_o(cycle_l),
  .crc_o(crc_l)
);

  CRC32_D16 #(
    .InitVal('{default:1})
  ) CRC32_D16_inst (
    .clk(clk_150mhz),
    .rst(rst_150mhz),
    .data_i(rxData_i),
    .crcEna_i(crcEna_l),
    .clear_i(rxMrk_i),
    .crc_o(calcCrc_l)
  ); // CRC32_D16_inst

  assign crcEna_l         = ( rxStr_i ) && ( !getCRC_l ); // Flag for calculate CRC
  assign num16bData[31:0] = {blockSize_l[30:0], 1'b1}; // multipy blockSize_l by 2

 // `include "GTP_ReadRx_assertions.svh"

endmodule // GTP_ReadRx