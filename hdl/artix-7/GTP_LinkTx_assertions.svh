//////////////////////////////////////////////////////////////////////////////
// Title      : GTP_LinkTx_assertions
// Project    : GTP
// Date       : 2023-03-31
//////////////////////////////////////////////////////////////////////////////
// Docs: https://wikis.cern.ch/display/BERF/ObsBox+Gen1
//////////////////////////////////////////////////////////////////////////////
// File       : GTP_LinkTx_assertions.sv
// Author     : Sebastian Owarzany
// Company    : CERN
// Platform   : multiplatform
// Standard   : SystemVerilog IEEE 1800
//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2021-2022 CERN. All rights reserved.
//////////////////////////////////////////////////////////////////////////////

// Send at least 3 (K28.5 + D5.6/D16.2) comma before (K23.7 + K23.7) comma
property three_comma_check;
  @( posedge clk_150mhz ) disable iff ( rst_150mhz )
  ( mkr_l ) |->
  ( $past( mkr_l, 2 ) == 1'b0 ) &&
  ( $past( mkr_l, 1 ) == 1'b0 ) &&
  ( $past( mkr_l )    == 1'b0 );
endproperty assume property ( three_comma_check );

// Send comma (K23.7 + K23.7) befor sending sata frame
property mkr_str_check;
  @( posedge clk_150mhz ) disable iff ( rst_150mhz )
  ( mkr_l ) |->
  ##1 ( str_l == 1'b1 );
endproperty assume property ( mkr_str_check );

// Check if IDLE state work proper
property IDLE_check;
  @( posedge clk_150mhz ) disable iff ( rst_150mhz )
  ( currentState == IDLE ) |->
  ( !incHeadCnt_l && !incDataCnt_l && !str_l && !mkr_l && !addCRC_l );
endproperty assume property ( IDLE_check );

// Check if SEND_K23 state work proper
property SEND_K23_check;
  @( posedge clk_150mhz ) disable iff ( rst_150mhz )
  ( currentState == SEND_K23 ) |->
  ( !incHeadCnt_l && !incDataCnt_l && !str_l && mkr_l && !addCRC_l );
endproperty assume property ( SEND_K23_check );

// Check if SEND_HEADER state work proper
property SEND_HEADER_check;
  @( posedge clk_150mhz ) disable iff ( rst_150mhz )
  ( currentState == SEND_HEADER ) |->
  ( incHeadCnt_l && !incDataCnt_l && str_l && !mkr_l && !addCRC_l );
endproperty assume property ( SEND_HEADER_check );

// Check if SEND_DATA state work proper
property SEND_DATA_check;
  @( posedge clk_150mhz ) disable iff ( rst_150mhz )
  ( currentState == SEND_DATA ) |->
  ( !incHeadCnt_l && incDataCnt_l && str_l && !mkr_l && !addCRC_l );
endproperty assume property ( SEND_DATA_check );

// Check if SEND_CRC state work proper
property SEND_CRC_check;
  @( posedge clk_150mhz ) disable iff ( rst_150mhz )
  ( currentState == SEND_CRC ) |->
  ( incHeadCnt_l && !incDataCnt_l && str_l && !mkr_l && addCRC_l );
endproperty assume property ( SEND_CRC_check );

// FIFO level cannot go down without a fifoRead_l
property FifoLevel_check;
   @( posedge rst_125mhz ) disable iff ( rst_125mhz )
   ( !fifoRead_l ) |->
   ##2 ( fifoDataCnt >= $past( fifoDataCnt ) );
endproperty assume property ( FifoLevel_check );

// headCnt_l cannot go up without a incHeadCnt_l
property headCnt_l_check;
   @( posedge clk_150mhz ) disable iff ( clk_150mhz )
   ( !incHeadCnt_l ) |->
   ##1 ( headCnt_l >= $past( headCnt_l ) );
endproperty assume property ( headCnt_l_check );

// dataCnt cannot go up without a incDataCnt_l
property dataCnt_check;
   @( posedge clk_150mhz ) disable iff ( clk_150mhz )
   ( !incDataCnt_l ) |->
   ##1 ( dataCnt >= $past( dataCnt ) );
endproperty assume property ( dataCnt_check );