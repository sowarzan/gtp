library ieee;
use ieee.STD_LOGIC_1164.all;
use ieee.NUMERIC_STD.all;
library work;
use work.GTP_TILE_PKG.all;

entity GTPBlock_wrap is
  generic (
    G_RATE : string
  );
  port (
    Clk          : in std_logic;
    Rst          : in std_logic;
    RefClk       : in std_logic;
    UserClk      : in std_logic;

    ClocksOk     : in std_logic;
    ClearFaults  : in std_logic;
    SFPPrsnt     : in std_logic_vector(3 downto 0 );
    SFPTxFault_N : in std_logic_vector(3 downto 0 );
    SFPLOS_N     : in std_logic_vector(3 downto 0 );

    TxMarker1    : in std_logic;
    TxMarker2    : in std_logic;
    TxMarker3    : in std_logic;
    TxMarker4    : in std_logic;

    TxStr1       : in std_logic;
    TxStr2       : in std_logic;
    TxStr3       : in std_logic;
    TxStr4       : in std_logic;

    TxData1      : in std_logic_vector(15 downto 0 );
    TxData2      : in std_logic_vector(15 downto 0 );
    TxData3      : in std_logic_vector(15 downto 0 );
    TxData4      : in std_logic_vector(15 downto 0 );

    GTP4_RX_P    : in std_logic;
    GTP4_RX_N    : in std_logic;
    GTP3_RX_P    : in std_logic;
    GTP3_RX_N    : in std_logic;
    GTP2_RX_P    : in std_logic;
    GTP2_RX_N    : in std_logic;
    GTP1_RX_P    : in std_logic;
    GTP1_RX_N    : in std_logic;

    GTP4_TX_P    : out std_logic;
    GTP4_TX_N    : out std_logic;
    GTP3_TX_P    : out std_logic;
    GTP3_TX_N    : out std_logic;
    GTP2_TX_P    : out std_logic;
    GTP2_TX_N    : out std_logic;
    GTP1_TX_P    : out std_logic;
    GTP1_TX_N    : out std_logic;

    RxMarker1    : out std_logic;
    RxMarker2    : out std_logic;
    RxMarker3    : out std_logic;
    RxMarker4    : out std_logic;

    RxStr1       : out std_logic;
    RxStr2       : out std_logic;
    RxStr3       : out std_logic;
    RxStr4       : out std_logic;

    RxData1      : out std_logic_vector(15 downto 0 );
    RxData2      : out std_logic_vector(15 downto 0 );
    RxData3      : out std_logic_vector(15 downto 0 );
    RxData4      : out std_logic_vector(15 downto 0 );

    gtp_RdMem    : in std_logic;
    gtp_WrMem    : in std_logic;
    gtp_WrData   : in std_logic_vector(15 downto 0 );
    gtp_Addr     : in std_logic_vector(5 downto 1 );
    gtp_RdData   : out std_logic_vector(15 downto 0 );
    gtp_RdDone   : out std_logic;
    gtp_WrDone   : out std_logic;

    Debug        : out std_logic_vector(31 downto 0 );
    Faults       : out std_logic;
    SFPTxEna     : out std_logic_vector(3 downto 0 );
    PLLLock      : out std_logic
  ); -- GTPBlock_wrap
end;

architecture GTPBlock_wrap of GTPBlock_wrap is

  function string_to_g_rate(X : string)
            return GTP_RATE_TYPE is
    begin
      if X = "GBPS_1_0" then
        return GBPS_1_0 ;
      end if;
      if X = "GBPS_2_0" then
        return GBPS_2_0;
      end if;
      if X = "GBPS_2_5" then
        return GBPS_2_5;
      end if;
      if X = "GBPS_3_0" then
        return GBPS_3_0;
      end if;
      return GBPS_3_0;
  end string_to_g_rate;

begin
  GTPBlock_inst : entity work.GTPBlock
  generic map (
    G_RATE => string_to_g_rate(G_RATE)
  ) port map (
    gtp_WrData   => gtp_WrData,
    Clk          => Clk,
    TxStr1       => TxStr1,
    TxStr2       => TxStr2,
    TxStr3       => TxStr3,
    TxStr4       => TxStr4,
    Debug        => Debug,
    Rst          => Rst,
    gtp_Addr     => gtp_Addr,
    gtp_WrDone   => gtp_WrDone,
    SFPTxFault_N => SFPTxFault_N,
    RxStr1       => RxStr1,
    RxStr2       => RxStr2,
    RxStr3       => RxStr3,
    RxStr4       => RxStr4,
    RxData1      => RxData1,
    RxData2      => RxData2,
    GTP4_TX_P    => GTP4_TX_P,
    RxData3      => RxData3,
    RxData4      => RxData4,
    GTP4_RX_P    => GTP4_RX_P,
    SFPTxEna     => SFPTxEna,
    GTP4_TX_N    => GTP4_TX_N,
    GTP4_RX_N    => GTP4_RX_N,
    GTP3_TX_P    => GTP3_TX_P,
    GTP3_RX_P    => GTP3_RX_P,
    gtp_RdData   => gtp_RdData,
    GTP3_TX_N    => GTP3_TX_N,
    GTP3_RX_N    => GTP3_RX_N,
    RefClk       => RefClk,
    gtp_RdMem    => gtp_RdMem,
    GTP2_TX_P    => GTP2_TX_P,
    GTP2_RX_P    => GTP2_RX_P,
    GTP2_TX_N    => GTP2_TX_N,
    GTP2_RX_N    => GTP2_RX_N,
    SFPLOS_N     => SFPLOS_N,
    GTP1_TX_P    => GTP1_TX_P,
    GTP1_RX_P    => GTP1_RX_P,
    RxMarker1    => RxMarker1,
    RxMarker2    => RxMarker2,
    RxMarker3    => RxMarker3,
    RxMarker4    => RxMarker4,
    GTP1_TX_N    => GTP1_TX_N,
    GTP1_RX_N    => GTP1_RX_N,
    gtp_RdDone   => gtp_RdDone,
    SFPPrsnt     => SFPPrsnt,
    ClocksOk     => ClocksOk,
    Faults       => Faults,
    TxData1      => TxData1,
    TxData2      => TxData2,
    TxData3      => TxData3,
    TxData4      => TxData4,
    UserClk      => UserClk,
    TxMarker1    => TxMarker1,
    TxMarker2    => TxMarker2,
    TxMarker3    => TxMarker3,
    TxMarker4    => TxMarker4,
    gtp_WrMem    => gtp_WrMem,
    ClearFaults  => ClearFaults,
    PLLLock      => PLLLock
  ); -- GTPBlock
  end GTPBlock_wrap ; -- GTPBlock_wrap