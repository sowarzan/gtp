library ieee;
use ieee.STD_LOGIC_1164.all;
entity FanIn32 is
  generic (
           X : std_logic := '0'
           );
  port (
        O : out std_logic_vector(31 downto 0 );
        I0 : in std_logic := X;
        I1 : in std_logic := X;
        I2 : in std_logic := X;
        I3 : in std_logic := X;
        I4 : in std_logic := X;
        I5 : in std_logic := X;
        I6 : in std_logic := X;
        I7 : in std_logic := X;
        I8 : in std_logic := X;
        I9 : in std_logic := X;
        I10 : in std_logic := X;
        I11 : in std_logic := X;
        I12 : in std_logic := X;
        I13 : in std_logic := X;
        I14 : in std_logic := X;
        I15 : in std_logic := X;
        I16 : in std_logic := X;
        I17 : in std_logic := X;
        I18 : in std_logic := X;
        I19 : in std_logic := X;
        I20 : in std_logic := X;
        I21 : in std_logic := X;
        I22 : in std_logic := X;
        I23 : in std_logic := X;
        I24 : in std_logic := X;
        I25 : in std_logic := X;
        I26 : in std_logic := X;
        I27 : in std_logic := X;
        I28 : in std_logic := X;
        I29 : in std_logic := X;
        I30 : in std_logic := X;
        I31 : in std_logic := X
        );


end FanIn32;


architecture FanIn32 of FanIn32 is


begin

  O(31) <= I31;
  O(30) <= I30;
  O(29) <= I29;
  O(28) <= I28;
  O(27) <= I27;
  O(26) <= I26;
  O(25) <= I25;
  O(24) <= I24;
  O(23) <= I23;
  O(22) <= I22;
  O(21) <= I21;
  O(20) <= I20;
  O(19) <= I19;
  O(18) <= I18;
  O(17) <= I17;
  O(16) <= I16;
  O(15) <= I15;
  O(14) <= I14;
  O(13) <= I13;
  O(12) <= I12;
  O(11) <= I11;
  O(10) <= I10;
  O(9) <= I9;
  O(8) <= I8;
  O(7) <= I7;
  O(6) <= I6;
  O(5) <= I5;
  O(4) <= I4;
  O(3) <= I3;
  O(2) <= I2;
  O(1) <= I1;
  O(0) <= I0;
end FanIn32;