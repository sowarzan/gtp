----------------------------------------------------
--
--  Library Name :  GTP
--  Unit    Name :  GTP_TILE_PKG
--  Unit    Type :  Package
--
------------------------------------------------------
------------------------------------------
------------------------------------------
-- Date        : Fri May 23 11:13:17 2014
--
-- Author      : Tom Levens <tom.levens@cern.ch>
--
-- Company     : CERN, BE-RF-FB
--
-- Description : Functions for GTP_TILE genericisation
--
------------------------------------------
------------------------------------------

package GTP_TILE_PKG  is

  -- ENUMERATIO TYPE
  type GTP_RATE_TYPE is (
      GBPS_1_0,
      GBPS_2_0,
      GBPS_2_5,
      GBPS_3_0
  );

  -- FUNCTIONS FOR SPARTAN6 (GTP_TILE_S6)
  function F_S6_PLL_TRXDIVSEL_OUT (R: GTP_RATE_TYPE) return integer;
  function F_S6_TX_TDCC_CFG       (R: GTP_RATE_TYPE) return bit_vector;
  function F_S6_PMA_RX_CFG        (R: GTP_RATE_TYPE) return bit_vector;
  function F_S6_PLL_DIVSEL_FB     (R: GTP_RATE_TYPE) return integer;
  function F_S6_PLL_DIVSEL_REF    (R: GTP_RATE_TYPE) return integer;

  -- FUNCTIONS FOR VIRTEX5 (GTP_TILE)
  function F_V5_SIM_PLL_PERDIV2   (R: GTP_RATE_TYPE) return bit_vector;
  function F_V5_PLL_TRXDIVSEL_OUT (R: GTP_RATE_TYPE) return integer;
  function F_V5_PLL_DIVSEL_FB     (R: GTP_RATE_TYPE) return integer;
  function F_V5_PLL_DIVSEL_REF    (R: GTP_RATE_TYPE) return integer;

  -- FUNCTIONS FOR ARTIX7 (GTP_TILE_A7)
  function F_A7_PLL_FBDIV_IN      (R: GTP_RATE_TYPE) return integer;
  function F_A7_RXCDR_CFG         (R: GTP_RATE_TYPE) return bit_vector;
  function F_A7_TRXOUT_DIV        (R: GTP_RATE_TYPE) return integer;

end;

------------------------------------------
------------------------------------------
-- Date        : Fri May 23 11:18:32 2014
--
-- Author      : Tom Levens <tom.levens@cern.ch>
--
-- Company     : CERN, BE-RF-FB
--
------------------------------------------
------------------------------------------

package body  GTP_TILE_PKG  is

  ------------------------------------------
  -- FUNCTIONS FOR SPARTAN6 (GTP_TILE_S6)
  ------------------------------------------
  function F_S6_PLL_TRXDIVSEL_OUT (R: GTP_RATE_TYPE) return integer is
  begin
      case R is
          when GBPS_2_5 => return 1; -- 2.5 GBPS
          when GBPS_2_0 => return 1; -- 2.0 GBPS
          when others   => return 2; -- 1.0 GBPS
      end case;
  end F_S6_PLL_TRXDIVSEL_OUT;

  function F_S6_TX_TDCC_CFG (R: GTP_RATE_TYPE) return bit_vector is
  begin
      case R is
          when GBPS_2_5 => return "11"; -- 2.5 GBPS
          when GBPS_2_0 => return "11"; -- 2.0 GBPS
          when others   => return "00"; -- 1.0 GBPS
      end case;
  end F_S6_TX_TDCC_CFG;

  function F_S6_PMA_RX_CFG (R: GTP_RATE_TYPE) return bit_vector is
  begin
      case R is
          when GBPS_2_5 => return X"05ce089"; -- 2.5 GBPS
          when GBPS_2_0 => return X"05ce089"; -- 2.0 GBPS
          when others   => return X"05ce049"; -- 1.0 GBPS
      end case;
  end F_S6_PMA_RX_CFG;

  function F_S6_PLL_DIVSEL_FB (R: GTP_RATE_TYPE) return integer is
  begin
      case R is
          when GBPS_2_5 => return 5; -- 2.5 GBPS
          when GBPS_2_0 => return 2; -- 2.0 GBPS
          when others   => return 2; -- 1.0 GBPS
      end case;
  end F_S6_PLL_DIVSEL_FB;

  function F_S6_PLL_DIVSEL_REF (R: GTP_RATE_TYPE) return integer is
  begin
      case R is
          when GBPS_2_5 => return 2; -- 2.5 GBPS
          when GBPS_2_0 => return 1; -- 2.0 GBPS
          when others   => return 1; -- 1.0 GBPS
      end case;
  end F_S6_PLL_DIVSEL_REF;
  ------------------------------------------
  -- FUNCTIONS FOR SPARTAN6 (GTP_TILE_S6)
  ------------------------------------------

  ------------------------------------------
  -- FUNCTIONS FOR VIRTEX5 (GTP_TILE)
  ------------------------------------------
  function F_V5_SIM_PLL_PERDIV2 (R: GTP_RATE_TYPE) return bit_vector is
  begin
      case R is
          when GBPS_2_5 => return X"190"; -- 2.5 GBPS
          when GBPS_2_0 => return X"1F4"; -- 2.0 GBPS
          when others   => return X"1F4"; -- 1.0 GBPS
      end case;
  end F_V5_SIM_PLL_PERDIV2;

  function F_V5_PLL_TRXDIVSEL_OUT (R: GTP_RATE_TYPE) return integer is
  begin
      case R is
          when GBPS_2_5 => return 1; -- 2.5 GBPS
          when GBPS_2_0 => return 1; -- 2.0 GBPS
          when others   => return 2; -- 1.0 GBPS
      end case;
  end F_V5_PLL_TRXDIVSEL_OUT;

  function F_V5_PLL_DIVSEL_FB (R: GTP_RATE_TYPE) return integer is
  begin
      case R is
          when GBPS_2_5 => return 5; -- 2.5 GBPS
          when GBPS_2_0 => return 2; -- 2.0 GBPS
          when others   => return 2; -- 1.0 GBPS
      end case;
  end F_V5_PLL_DIVSEL_FB;

  function F_V5_PLL_DIVSEL_REF (R: GTP_RATE_TYPE) return integer is
  begin
      case R is
          when GBPS_2_5 => return 2; -- 2.5 GBPS
          when GBPS_2_0 => return 1; -- 2.0 GBPS
          when others   => return 1; -- 1.0 GBPS
      end case;
  end F_V5_PLL_DIVSEL_REF;
  ------------------------------------------
  -- END FUNCTIONS FOR VIRTEX5 (GTP_TILE)
  ------------------------------------------


  ------------------------------------------
  -- FUNCTIONS FOR ARTIX7 (GTP_TILE_A7)
  ------------------------------------------
  function F_A7_RXCDR_CFG (R: GTP_RATE_TYPE) return bit_vector is
  begin
      case R is
          when GBPS_3_0 => return X"0001107FE206021081010"; -- 3.0 GBPS
          when GBPS_2_5 => return X"0000107FE206001041010"; -- 2.5 GBPS
          when GBPS_2_0 => return X"0000107FE206001041010"; -- 2.0 GBPS
          when others   => return X"0000107FE106001041010"; -- 1.0 GBPS
      end case;
  end F_A7_RXCDR_CFG;

  function F_A7_TRXOUT_DIV (R: GTP_RATE_TYPE) return integer is
  begin
      case R is
          when GBPS_3_0 => return 2; -- 3.0 GBPS
          when GBPS_2_5 => return 2; -- 2.5 GBPS
          when GBPS_2_0 => return 2; -- 2.0 GBPS
          when others   => return 4; -- 1.0 GBPS
      end case;
  end F_A7_TRXOUT_DIV;

  function F_A7_PLL_FBDIV_IN (R: GTP_RATE_TYPE) return integer is
  begin
      case R is
          when GBPS_3_0 => return 3; -- 3.0 GBPS
          when GBPS_2_5 => return 5; -- 2.5 GBPS
          when GBPS_2_0 => return 4; -- 2.0 GBPS
          when others   => return 4; -- 1.0 GBPS
      end case;
  end F_A7_PLL_FBDIV_IN;
  ------------------------------------------
  -- END FUNCTIONS FOR ARTIX7 (GTP_TILE_A7)
  ------------------------------------------

end;

----------------------------------------------------
--
--      VHDL code generated by Visual Elite
--
--  Design Unit:
--  ------------
--      Unit    Name  :  GTP_RstSeq
--      Library Name  :  GTP
--
--      Creation Date :  Wed Apr 19 17:29:37 2017
--      Version       :  2012.09 v4.4.1 build 33. Date: Nov  1 2012. License: 2012.11
--
--  Options Used:
--  -------------
--      Target
--         Language   :  As Is
--         Purpose    :  Synthesis
--         Vendor     :  Generic
--
--      Style
--         Use Procedures                 :  No
--         Code Destination               :  Combined file
--         Attach Packages                :  Yes
--         Generate Entity                :  Yes
--         Attach Directives              :  Yes
--         Structural                     :  No
--         Configuration Specification    :  No
--         library name in
--         Configuration Specification    :  No
--         Configuration Declaration      :  None
--         IF for state selection         :  No
--         Preserve spacing for free text :  Yes
--         Declaration alignment          :  No
--
----------------------------------------------------
----------------------------------------------------
--
--  Library Name :  GTP
--  Unit    Name :  GTP_RstSeq
--  Unit    Type :  State Machine
--
------------------------------------------------------

library ieee;
use ieee.STD_LOGIC_1164.all;
use ieee.NUMERIC_STD.all;
entity GTP_RstSeq is
port (
      Clk : in std_logic;
      Rst : in std_logic;
      Invalid : in std_logic;
      RstRxCDR_in : in std_logic := '0';
      RstRx_in : in std_logic := '0';
      RstRxCDR_out : out std_logic;
      RstRx_out : out std_logic;
      RstDone : in std_logic;
      Sta : out std_logic_vector(2 downto 0 );
      Disable : in std_logic
      );

end GTP_RstSeq;


architecture GTP_RstSeq of GTP_RstSeq is

signal GoodCnt : unsigned(3 downto 0 );
signal BadCnt : unsigned(7 downto 0 );
signal RstRxCDR : std_logic;
signal RstRx : std_logic;


constant ACQ : std_logic_vector(2 downto 0 ) := "000";
constant CHECK : std_logic_vector(2 downto 0 ) := "001";
constant DISABLE_RST : std_logic_vector(2 downto 0 ) := "010";
constant SYNC : std_logic_vector(2 downto 0 ) := "011";
constant WAITforLOWRSTDONE : std_logic_vector(2 downto 0 ) := "100";
constant WAITforRSTDONE : std_logic_vector(2 downto 0 ) := "101";
signal visual_ACQ_current : std_logic_vector(2 downto 0 );


begin



-- Synchronous process
GTP_RstSeq_ACQ:
process (Clk)
begin

  if (rising_edge(Clk)) then

    RstRx <= '0';
    RstRxCDR <= '0';

    if (Rst = '1' or RstRxCDR_in = '1') then
      GoodCnt <= (others => '0');
      BadCnt <= (others => '0');
      RstRxCDR <= '1';
      visual_ACQ_current <= WAITforLOWRSTDONE;
    elsif (RstRx_in = '1') then
      RstRx <= '1';
      visual_ACQ_current <= WAITforLOWRSTDONE;
    else

      case visual_ACQ_current is
        when ACQ =>
          if (Disable = '1') then
            visual_ACQ_current <= DISABLE_RST;
          elsif (BadCnt = X"FF") then
            RstRxCDR <= '1';
            visual_ACQ_current <= WAITforLOWRSTDONE;
          elsif (Invalid = '1') then
            GoodCnt <= X"0";
            BadCnt <= BadCnt + 1;
            visual_ACQ_current <= ACQ;
          elsif (GoodCnt = X"3") then
            GoodCnt <= X"0";
            BadCnt <= X"00";
            visual_ACQ_current <= SYNC;
          else
            GoodCnt <= GoodCnt + 1;
            visual_ACQ_current <= ACQ;
          end if;

        when CHECK =>
          if (Disable = '1') then
            visual_ACQ_current <= DISABLE_RST;
          elsif (BadCnt = X"03") then
            GoodCnt <= X"0";
            BadCnt <= X"00";
            RstRx <= '1';
            visual_ACQ_current <= ACQ;
          elsif (Invalid = '1') then
            BadCnt <= BadCnt + 1;
            GoodCnt <= X"0";
            visual_ACQ_current <= CHECK;
          elsif (GoodCnt = X"4") then
            GoodCnt <= X"0";
            BadCnt <= X"00";
            visual_ACQ_current <= SYNC;
          else
            GoodCnt <= GoodCnt + 1;
            visual_ACQ_current <= CHECK;
          end if;

        when DISABLE_RST =>
          if (Disable = '0') then
            RstRxCDR <= '1';
            visual_ACQ_current <= WAITforLOWRSTDONE;
          else
            visual_ACQ_current <= DISABLE_RST;
          end if;

        when SYNC =>
          if (Invalid = '1') then
            visual_ACQ_current <= CHECK;
          else
            visual_ACQ_current <= SYNC;
          end if;

        when WAITforLOWRSTDONE =>
          if (RstDone = '0') then
            visual_ACQ_current <= WAITforRSTDONE;
          else
            visual_ACQ_current <= WAITforLOWRSTDONE;
          end if;

        when WAITforRSTDONE =>
          if (RstDone = '1') then
            GoodCnt <= X"0";
            BadCnt <= X"00";
            visual_ACQ_current <= ACQ;
          else
            visual_ACQ_current <= WAITforRSTDONE;
          end if;

        when others =>

          visual_ACQ_current <= ACQ;
      end case;
    end if;
  end if;
end process GTP_RstSeq_ACQ;

Sta <= visual_ACQ_current;
RstRx_out <= RstRx;
RstRxCDR_out <= RstRxCDR;
end GTP_RstSeq;
----------------------------------------------------
--
--  Library Name :  CommonVisual
--  Unit    Name :  PosEdge
--  Unit    Type :  Text Unit
--
------------------------------------------------------
------------------------------------------
------------------------------------------
-- Date        : Thu Jun 12 10:26:24 2003
--
-- Author      : J.C.Molendijk
--
-- Company     :
--
-- Description : A Positive Edge Detector
--
------------------------------------------
------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity  PosEdge  is

port(Clk,SigIn : in  std_logic;
   PEdg      : out std_logic);

end;


------------------------------------------
------------------------------------------
-- Date        : Thu Jun 12 10:29:33 2003
--
-- Author      : J.C.Molendijk
--
-- Company     :
--
-- Description : A Positive Edge Detector
--
------------------------------------------
------------------------------------------
architecture  V1  of  PosEdge  is

signal SR: std_logic_vector(1 downto 0):="00";

begin

process(Clk)
  begin
    if Clk'event and Clk = '1'
      then
        SR(1) <= SR(0);
        SR(0) <= SigIn;
      if SR(1) = '0' and SR(0) = '1'
        then PEdg <= '1';
        else PEdg <= '0';
      end if;
    end if;
  end process;

end;


----------------------------------------------------
--
--  Library Name :  CommonVisual
--  Unit    Name :  RSFF
--  Unit    Type :  Text Unit
--
------------------------------------------------------
----------------------------------------------------------------
----------------------------------------------------------------
-- Date        : Wed Jun 11 17:46:39 2003
--
-- Author      : J.C.Molendijk
--
-- Company     : CERN BE-RF-CS
--
-- Description : A Synchronous Set Reset FF (Priority to Reset)
--
----------------------------------------------------------------
-- 20091030 : Addition of the pin reset with
--            default value '0'.
--            Replacement of Clk'event and Clk='1'
--            by rising_edge(Clk).(Done by Greg)
--
----------------------------------------------------------------
----------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity RSFF is
port (Clk,Set,Clr: in std_logic;
    Rst : in std_logic:='0'; --Reset active high, port can be left open is unused
    Q: out std_logic);

end;

----------------------------------------------------------------
----------------------------------------------------------------
-- Date        : Wed Jun 11 17:46:39 2003
--
-- Author      : J.C.Molendijk
--
-- Company     : CERN BE-RF-CS
--
-- Description : A Synchronous Set Reset FF (Priority to Reset)
--
----------------------------------------------------------------
-- 20091030 : Addition of the pin reset with
--            default value '0'.
--            Replacement of Clk'event and Clk='1'
--            by rising_edge(Clk).(Done by Greg)
--
----------------------------------------------------------------
----------------------------------------------------------------
architecture  V1 of RSFF is
begin

process(Clk)
begin
  if rising_edge(Clk) then
      if Clr = '1' or Rst='1' then
        Q <= '0';
      elsif Set = '1'then
        Q <= '1';
      end if;
  end if;
end process;

end;


library ieee;
use ieee.STD_LOGIC_1164.all;
use ieee.NUMERIC_STD.all;
entity GTP_RxLogic is
port (
      GTPData : in std_logic_vector(15 downto 0 );
      RxData : out std_logic_vector(15 downto 0 );
      GTPIsK : in std_logic_vector(1 downto 0 );
      UsrClk : in std_logic;
      RxStr : out std_logic;
      RxCommaMisalign : out std_logic;
      RxDoubleComma : out std_logic;
      RxComma : out std_logic;
      Valid : in std_logic
      );


end GTP_RxLogic;


architecture GTP_RxLogic of GTP_RxLogic is

signal IsK0 : std_logic;
signal IsDoubleK : std_logic;
signal IsK1 : std_logic;
signal IsKMis : std_logic;
signal IsD : std_logic;
signal IsK : std_logic;
signal IsValidKMis : std_logic;
signal IsValidK : std_logic;
signal IsValidDoubleK : std_logic;
signal IsValidD : std_logic;


begin

process (UsrClk)
begin
if (UsrClk'event and UsrClk = '1') then
      RxData(15 downto 0) <= (GTPData(15 downto 0));


end if;
end process;

process (UsrClk)
begin
if (UsrClk'event and UsrClk = '1') then
      RxStr <= (IsValidD);


end if;
end process;

 IsValidD <= ( IsD) and ( Valid);

process (UsrClk)
begin
if (UsrClk'event and UsrClk = '1') then
      RxComma <= (IsValidK);


end if;
end process;

 IsValidK <= ( IsK) and ( Valid);

 IsD <= (not IsK0) and (not IsK1);

 IsK <= ( IsK0) and (not IsK1);

IsK0 <= GTPIsK(0);
IsK1 <= GTPIsK(1);

 IsKMis <= (not IsK0) and ( IsK1);

process (UsrClk)
begin
if (UsrClk'event and UsrClk = '1') then
      RxCommaMisalign <= (IsValidKMis);


end if;
end process;

 IsValidKMis <= ( IsKMis) and ( Valid);

 IsDoubleK <= ( IsK0) and ( IsK1);

 IsValidDoubleK <= ( IsDoubleK) and ( Valid);

process (UsrClk)
begin
if (UsrClk'event and UsrClk = '1') then
      RxDoubleComma <= (IsValidDoubleK);


end if;
end process;
end GTP_RxLogic;

library ieee;
use ieee.STD_LOGIC_1164.all;
use ieee.NUMERIC_STD.all;
entity GTP_CommaTable is
port (
      Mode : in std_logic_vector(1 downto 0 );
      Disp : in std_logic;
      Comma : out std_logic_vector(19 downto 0 );
      DispInv : out std_logic
      );

end GTP_CommaTable;


architecture GTP_CommaTable of GTP_CommaTable is


begin
GTP_CommaTable:
process (Mode, Disp)
begin

  if (Mode = "01") AND
     (Disp = '0') then
    Comma <= "0011111001" & "1001000101";
    DispInv <= '0';
  --  Ch Bond (K28.1, D16.2)
  elsif
        (Mode = "01") AND
        (Disp = '1') then
    Comma <= "1100000110" & "0110110101";
    DispInv <= '0';
  --  Ch Bond (K28.1, D16.2)
  elsif
        (Mode = "10") AND
        (Disp = '0') then
    Comma <= "1110101000" & "1110101000";
    DispInv <= '0';
  --  Car Ext (K23.7, K23.7)
  elsif
        (Mode = "10") AND
        (Disp = '1') then
    Comma <= "0001010111" & "0001010111";
    DispInv <= '0';
  --  Car Ext (K23.7, K23.7)
  elsif
        (Disp = '0') then
    Comma <= "0011111010" & "1001000101";
    DispInv <= '0';
  else
    --  Comma (K28.5, D16.2)

    --  Comma (K28.5, D05.6)
    Comma <= "1100000101" & "1010010110";
    DispInv <= '1';
  end if ;
end process GTP_CommaTable;

end GTP_CommaTable;

library ieee;
use ieee.STD_LOGIC_1164.all;
use ieee.NUMERIC_STD.all;
entity GTP_Dx7Table is
port (
      In4b : in std_logic_vector(3 downto 0 );
      Out4b : out std_logic_vector(3 downto 0 );
      In6b : in std_logic_vector(5 downto 0 )
      );

end GTP_Dx7Table;


architecture GTP_Dx7Table of GTP_Dx7Table is


begin
GTP_Dx7Table:
process (In4b, In6b)
begin

  if (In4b = "1110") AND
     (In6b = "100011") then
    Out4b <= "0111";
  --  D17.7-
  elsif
        (In4b = "1110") AND
        (In6b = "010011") then
    Out4b <= "0111";
  --  D18.7-
  elsif
        (In4b = "1110") AND
        (In6b = "001011") then
    Out4b <= "0111";
  --  D20.7-
  elsif
        (In4b = "0001") AND
        (In6b = "110100") then
    Out4b <= "1000";
  --  D11.7+
  elsif
        (In4b = "0001") AND
        (In6b = "101100") then
    Out4b <= "1000";
  --  D13.7+
  elsif
        (In4b = "0001") AND
        (In6b = "011100") then
    Out4b <= "1000";
  else
    --  D14.7+

    Out4b <= In4b;
  end if ;
end process GTP_Dx7Table;

end GTP_Dx7Table;

library ieee;
use ieee.STD_LOGIC_1164.all;
use ieee.NUMERIC_STD.all;
entity GTP_3b4bTable is
port (
      In3b : in std_logic_vector(2 downto 0 );
      Out4b : out std_logic_vector(3 downto 0 );
      DatFlip : out std_logic;
      DispFlip : out std_logic
      );

end GTP_3b4bTable;


architecture GTP_3b4bTable of GTP_3b4bTable is


begin
GTP_3b4bTable:
process (In3b)
begin

  if (In3b = "000") then
    Out4b <= "1011";
    DatFlip <= '1';
    DispFlip <= '1';
  --  Dx.0
  elsif (In3b = "001") then
    Out4b <= "1001";
    DatFlip <= '0';
    DispFlip <= '0';
  --  Dx.1
  elsif (In3b = "010") then
    Out4b <= "0101";
    DatFlip <= '0';
    DispFlip <= '0';
  --  Dx.2
  elsif (In3b = "011") then
    Out4b <= "1100";
    DatFlip <= '1';
    DispFlip <= '0';
  --  Dx.3
  elsif (In3b = "100") then
    Out4b <= "1101";
    DatFlip <= '1';
    DispFlip <= '1';
  --  Dx.4
  elsif (In3b = "101") then
    Out4b <= "1010";
    DatFlip <= '0';
    DispFlip <= '0';
  --  Dx.5
  elsif (In3b = "110") then
    Out4b <= "0110";
    DatFlip <= '0';
    DispFlip <= '0';
  --  Dx.6
  elsif (In3b = "111") then
    Out4b <= "1110";
    DatFlip <= '1';
    DispFlip <= '1';
  else
    --  Dx.7 (Primary)

    Out4b <= "0000";
    DatFlip <= '0';
    DispFlip <= '0';
  end if ;
end process GTP_3b4bTable;

end GTP_3b4bTable;

library ieee;
use ieee.STD_LOGIC_1164.all;
use ieee.NUMERIC_STD.all;
entity GTP_5b6bTable is
port (
      In5b : in std_logic_vector(4 downto 0 );
      Out6b : out std_logic_vector(5 downto 0 );
      DatFlip : out std_logic;
      DispFlip : out std_logic
      );

end GTP_5b6bTable;


architecture GTP_5b6bTable of GTP_5b6bTable is


begin
GTP_5b6bTable:
process (In5b)
begin

  if (In5b = "00000") then
    Out6b <= "100111";
    DatFlip <= '1';
    DispFlip <= '1';
  --  D0
  elsif (In5b = "00001") then
    Out6b <= "011101";
    DatFlip <= '1';
    DispFlip <= '1';
  --  D1
  elsif (In5b = "00010") then
    Out6b <= "101101";
    DatFlip <= '1';
    DispFlip <= '1';
  --  D2
  elsif (In5b = "00011") then
    Out6b <= "110001";
    DatFlip <= '0';
    DispFlip <= '0';
  --  D3
  elsif (In5b = "00100") then
    Out6b <= "110101";
    DatFlip <= '1';
    DispFlip <= '1';
  --  D4
  elsif (In5b = "00101") then
    Out6b <= "101001";
    DatFlip <= '0';
    DispFlip <= '0';
  --  D5
  elsif (In5b = "00110") then
    Out6b <= "011001";
    DatFlip <= '0';
    DispFlip <= '0';
  --  D6
  elsif (In5b = "00111") then
    Out6b <= "111000";
    DatFlip <= '1';
    DispFlip <= '0';
  --  D7
  elsif (In5b = "01000") then
    Out6b <= "111001";
    DatFlip <= '1';
    DispFlip <= '1';
  --  D8
  elsif (In5b = "01001") then
    Out6b <= "100101";
    DatFlip <= '0';
    DispFlip <= '0';
  --  D9
  elsif (In5b = "01010") then
    Out6b <= "010101";
    DatFlip <= '0';
    DispFlip <= '0';
  --  D10
  elsif (In5b = "01011") then
    Out6b <= "110100";
    DatFlip <= '0';
    DispFlip <= '0';
  --  D11
  elsif (In5b = "01100") then
    Out6b <= "001101";
    DatFlip <= '0';
    DispFlip <= '0';
  --  D12
  elsif (In5b = "01101") then
    Out6b <= "101100";
    DatFlip <= '0';
    DispFlip <= '0';
  --  D13
  elsif (In5b = "01110") then
    Out6b <= "011100";
    DatFlip <= '0';
    DispFlip <= '0';
  --  D14
  elsif (In5b = "01111") then
    Out6b <= "010111";
    DatFlip <= '1';
    DispFlip <= '1';
  --  D15
  elsif (In5b = "10000") then
    Out6b <= "011011";
    DatFlip <= '1';
    DispFlip <= '1';
  --  D16
  elsif (In5b = "10001") then
    Out6b <= "100011";
    DatFlip <= '0';
    DispFlip <= '0';
  --  D17
  elsif (In5b = "10010") then
    Out6b <= "010011";
    DatFlip <= '0';
    DispFlip <= '0';
  --  D18
  elsif (In5b = "10011") then
    Out6b <= "110010";
    DatFlip <= '0';
    DispFlip <= '0';
  --  D19
  elsif (In5b = "10100") then
    Out6b <= "001011";
    DatFlip <= '0';
    DispFlip <= '0';
  --  D20
  elsif (In5b = "10101") then
    Out6b <= "101010";
    DatFlip <= '0';
    DispFlip <= '0';
  --  D21
  elsif (In5b = "10110") then
    Out6b <= "011010";
    DatFlip <= '0';
    DispFlip <= '0';
  --  D22
  elsif (In5b = "10111") then
    Out6b <= "111010";
    DatFlip <= '1';
    DispFlip <= '1';
  --  D23
  elsif (In5b = "11000") then
    Out6b <= "110011";
    DatFlip <= '1';
    DispFlip <= '1';
  --  D24
  elsif (In5b = "11001") then
    Out6b <= "100110";
    DatFlip <= '0';
    DispFlip <= '0';
  --  D25
  elsif (In5b = "11010") then
    Out6b <= "010110";
    DatFlip <= '0';
    DispFlip <= '0';
  --  D26
  elsif (In5b = "11011") then
    Out6b <= "110110";
    DatFlip <= '1';
    DispFlip <= '1';
  --  D27
  elsif (In5b = "11100") then
    Out6b <= "001110";
    DatFlip <= '0';
    DispFlip <= '0';
  --  D28
  elsif (In5b = "11101") then
    Out6b <= "101110";
    DatFlip <= '1';
    DispFlip <= '1';
  --  D29
  elsif (In5b = "11110") then
    Out6b <= "011110";
    DatFlip <= '1';
    DispFlip <= '1';
  --  D30
  elsif (In5b = "11111") then
    Out6b <= "101011";
    DatFlip <= '1';
    DispFlip <= '1';
  else
    --  D31

    Out6b <= "000000";
    DatFlip <= '0';
    DispFlip <= '0';
  end if ;
end process GTP_5b6bTable;

end GTP_5b6bTable;

library ieee;
use ieee.STD_LOGIC_1164.all;
use ieee.NUMERIC_STD.all;
entity GTP_8b10bEnc is
port (
      In_8b : in std_logic_vector(7 downto 0 );
      In_RunDisp : in std_logic;
      Out_RunDisp : out std_logic;
      Out_10b : out std_logic_vector(9 downto 0 )
      );


end GTP_8b10bEnc;


use work.all;
architecture GTP_8b10bEnc of GTP_8b10bEnc is

signal DispFlip_6b : std_logic;
signal Loc_Out_6b : std_logic_vector(5 downto 0 );
signal Loc_RunDisp : std_logic;
signal DispFlip_4b : std_logic;
signal Loc_RunDisp_4b : std_logic;
signal Out_4b : std_logic_vector(3 downto 0 );
signal Loc_RunDisp_6b : std_logic;
signal Loc_Out_4b : std_logic_vector(3 downto 0 );
signal DatFlip_6b : std_logic;
signal DatFlip_4b : std_logic;
signal Out_6b : std_logic_vector(5 downto 0 );
component GTP_5b6bTable
    port (
          In5b : in std_logic_vector(4 downto 0 );
          Out6b : out std_logic_vector(5 downto 0 );
          DatFlip : out std_logic;
          DispFlip : out std_logic
          );
end component;
component GTP_3b4bTable
    port (
          In3b : in std_logic_vector(2 downto 0 );
          Out4b : out std_logic_vector(3 downto 0 );
          DatFlip : out std_logic;
          DispFlip : out std_logic
          );
end component;
component GTP_Dx7Table
    port (
          In4b : in std_logic_vector(3 downto 0 );
          Out4b : out std_logic_vector(3 downto 0 );
          In6b : in std_logic_vector(5 downto 0 )
          );
end component;

-- Start Configuration Specification
-- ++ for all : GTP_5b6bTable use entity work.GTP_5b6bTable(GTP_5b6bTable);
-- ++ for all : GTP_3b4bTable use entity work.GTP_3b4bTable(GTP_3b4bTable);
-- ++ for all : GTP_Dx7Table use entity work.GTP_Dx7Table(GTP_Dx7Table);
-- End Configuration Specification

begin

B_5b6bTable: GTP_5b6bTable
  port map (
            In5b => In_8b(4 downto 0),
            Out6b => Out_6b(5 downto 0),
            DatFlip => DatFlip_6b,
            DispFlip => DispFlip_6b
            );

B_3b4bTable: GTP_3b4bTable
  port map (
            In3b => In_8b(7 downto 5),
            Out4b => Out_4b(3 downto 0),
            DatFlip => DatFlip_4b,
            DispFlip => DispFlip_4b
            );

B_Dx7Table: GTP_Dx7Table
  port map (
            In4b => Loc_Out_4b(3 downto 0),
            Out4b => Out_10b(3 downto 0),
            In6b => Loc_Out_6b(5 downto 0)
            );

process (Out_6b , Loc_RunDisp_6b)
 begin
   case Loc_RunDisp_6b is
     when '0' =>
       Loc_Out_6b(5 downto 0) <=  Out_6b(5 downto 0);
     when others =>
       Loc_Out_6b(5 downto 0) <= not Out_6b(5 downto 0);
   end case;
 end process;

process (Out_4b , Loc_RunDisp_4b)
 begin
   case Loc_RunDisp_4b is
     when '0' =>
       Loc_Out_4b(3 downto 0) <=  Out_4b(3 downto 0);
     when others =>
       Loc_Out_4b(3 downto 0) <= not Out_4b(3 downto 0);
   end case;
 end process;

process (In_RunDisp , DispFlip_6b)
 begin
   case DispFlip_6b is
     when '0' =>
       Loc_RunDisp <=  In_RunDisp;
     when others =>
       Loc_RunDisp <= not In_RunDisp;
   end case;
 end process;

 Loc_RunDisp_6b <= ( In_RunDisp) and ( DatFlip_6b);

process (Loc_RunDisp , DispFlip_4b)
 begin
   case DispFlip_4b is
     when '0' =>
       Out_RunDisp <=  Loc_RunDisp;
     when others =>
       Out_RunDisp <= not Loc_RunDisp;
   end case;
 end process;

 Loc_RunDisp_4b <= ( Loc_RunDisp) and ( DatFlip_4b);

Out_10b(9 downto 4) <= Loc_Out_6b(5 downto 0);
end GTP_8b10bEnc;

library ieee;
use ieee.STD_LOGIC_1164.all;
use ieee.NUMERIC_STD.all;
entity GTP_16b20bEnc is
port (
      In_16b : in std_logic_vector(15 downto 0 );
      Clk : in std_logic;
      Out_20b : out std_logic_vector(19 downto 0 );
      Rst : in std_logic;
      KMode : in std_logic_vector(1 downto 0 );
      Str : in std_logic
      );


end GTP_16b20bEnc;


use work.all;
architecture GTP_16b20bEnc of GTP_16b20bEnc is

signal Loc_Out : std_logic_vector(19 downto 0 );
signal KMode1 : std_logic_vector(1 downto 0 );
signal RunDispD : std_logic;
signal Str1 : std_logic;
signal KOut : std_logic_vector(19 downto 0 );
signal Loc_RunDisp : std_logic;
signal RunDispK : std_logic;
signal RunDisp : std_logic;
signal DOut : std_logic_vector(19 downto 0 );
signal RunDisp1 : std_logic;
signal DispInv : std_logic;
signal Loc_In : std_logic_vector(15 downto 0 );
component GTP_8b10bEnc
    port (
          In_8b : in std_logic_vector(7 downto 0 );
          In_RunDisp : in std_logic;
          Out_RunDisp : out std_logic;
          Out_10b : out std_logic_vector(9 downto 0 )
          );
end component;
component GTP_CommaTable
    port (
          Mode : in std_logic_vector(1 downto 0 );
          Disp : in std_logic;
          Comma : out std_logic_vector(19 downto 0 );
          DispInv : out std_logic
          );
end component;

-- Start Configuration Specification
-- ++ for all : GTP_8b10bEnc use entity work.GTP_8b10bEnc(GTP_8b10bEnc);
-- ++ for all : GTP_CommaTable use entity work.GTP_CommaTable(GTP_CommaTable);
-- End Configuration Specification

begin

B_8b10bEnc_LSB: GTP_8b10bEnc
  port map (
            In_8b => Loc_In(7 downto 0),
            In_RunDisp => RunDisp,
            Out_RunDisp => Loc_RunDisp,
            Out_10b => DOut(19 downto 10)
            );

B_8b10bEnc_MSB: GTP_8b10bEnc
  port map (
            In_8b => Loc_In(15 downto 8),
            In_RunDisp => Loc_RunDisp,
            Out_RunDisp => RunDispD,
            Out_10b => DOut(9 downto 0)
            );

C13: GTP_CommaTable
  port map (
            Mode => KMode1(1 downto 0),
            Disp => RunDisp,
            Comma => KOut(19 downto 0),
            DispInv => DispInv
            );

process (Clk)
begin
if (Clk'event and Clk = '1') then
      Out_20b(19 downto 0) <= (Loc_Out(19 downto 0));


end if;
end process;

process (DOut , KOut , Str1)
 begin
   case Str1 is
     when '1' =>
       Loc_Out(19 downto 0) <=  DOut(19 downto 0);
     when others =>
       Loc_Out(19 downto 0) <=  KOut(19 downto 0);
   end case;
 end process;

process (Clk , Rst)
begin
 if (Rst = '1') then
    RunDisp <= '0';
 elsif (Clk'event and Clk = '1') then
      RunDisp <= (RunDisp1);


end if;
end process;

process (Clk)
begin
if (Clk'event and Clk = '1') then
      Loc_In(15 downto 0) <= (In_16b(15 downto 0));


end if;
end process;

process (Clk)
begin
if (Clk'event and Clk = '1') then
      Str1 <= (Str);


end if;
end process;

process (RunDispD , RunDispK , Str1)
 begin
   case Str1 is
     when '1' =>
       RunDisp1 <=  RunDispD;
     when others =>
       RunDisp1 <=  RunDispK;
   end case;
 end process;

process (Clk)
begin
if (Clk'event and Clk = '1') then
      KMode1(1 downto 0) <= (KMode(1 downto 0));


end if;
end process;

process (RunDisp , DispInv)
 begin
   case DispInv is
     when '0' =>
       RunDispK <=  RunDisp;
     when others =>
       RunDispK <= not RunDisp;
   end case;
 end process;
end GTP_16b20bEnc;
----------------------------------------------------
--
--  Library Name :  GTP
--  Unit    Name :  GTP_TILE_A7_SYNC_BLOCK
--  Unit    Type :  Text Unit
--
------------------------------------------------------
--////////////////////////////////////////////////////////////////////////////////
--//   ____  ____
--//  /   /\/   /
--// /___/  \  /    Vendor: Xilinx
--// \   \   \/     Version : 2.7
--//  \   \         Application : 7 Series FPGAs Transceivers Wizard
--//  /   /         Filename :gtp_tile_a7_sync_block.vhd
--// /___/   /\
--// \   \  /  \
--//  \___\/\___\
--//
--//
--
-- Description: Used on signals crossing from one clock domain to
--              another, this is a flip-flop pair, with both flops
--              placed together with RLOCs into the same slice.  Thus
--              the routing delay between the two is minimum to safe-
--              guard against metastability issues.
--
--
-- Module gtp_tile_a7_sync_block
-- Generated by Xilinx 7 Series FPGAs Transceivers Wizard
--
--
-- (c) Copyright 2010-2012 Xilinx, Inc. All rights reserved.
--
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
--
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
--
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
--
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.





library ieee;
use ieee.std_logic_1164.all;

library unisim;
use unisim.vcomponents.all;

entity GTP_TILE_A7_SYNC_BLOCK is
generic (
  INITIALISE : bit_vector(1 downto 0) := "00"
);
port (
  clk         : in  std_logic;          -- clock to be sync'ed to
  data_in     : in  std_logic;          -- Data to be 'synced'
  data_out    : out std_logic           -- synced data
  );

end GTP_TILE_A7_SYNC_BLOCK;




architecture structural of gtp_tile_a7_sync_block is


-- Internal Signals
signal data_sync1 : std_logic;

-- These attributes will stop Vivado translating the desired flip-flops into an
-- SRL based shift register.
attribute ASYNC_REG                   : string;
attribute ASYNC_REG of data_sync      : label is "TRUE";
attribute ASYNC_REG of data_sync_reg  : label is "TRUE";

-- These attributes will stop timing errors being reported on the target flip-flop during back annotated SDF simulation.
attribute MSGON                      : string;
attribute MSGON of data_sync         : label is "FALSE";
attribute MSGON of data_sync_reg     : label is "FALSE";

-- These attributes will stop XST translating the desired flip-flops into an
-- SRL based shift register.
attribute shreg_extract                   : string;
attribute shreg_extract of data_sync      : label is "no";
attribute shreg_extract of data_sync_reg  : label is "no";


begin

data_sync : FD
generic map (
  INIT => INITIALISE(0)
)
port map (
  C    => clk,
  D    => data_in,
  Q    => data_sync1
);


data_sync_reg : FD
generic map (
  INIT => INITIALISE(1)
)
port map (
  C    => clk,
  D    => data_sync1,
  Q    => data_out
);


end structural;




----------------------------------------------------
--
--  Library Name :  GTP
--  Unit    Name :  GTP_TILE_A7_RXPMARST_SEQ
--  Unit    Type :  Text Unit
--
------------------------------------------------------
------------------------------------------------------------------------------/
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version : 2.7
--  \   \         Application : 7 Series FPGAs Transceivers Wizard
--  /   /         Filename : gtp_tile_a7_rxpmarst_seq.vhd
-- /___/   /\
-- \   \  /  \
--  \___\/\___\
--
--
-- Module gtp_tile_a7_rxpmarst_seq
-- Generated by Xilinx 7 Series FPGAs Transceivers Wizard
--
--
-- (c) Copyright 2010-2012 Xilinx, Inc. All rights reserved.
--
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
--
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
--
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
--
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


ENTITY GTP_TILE_A7_RXPMARST_SEQ IS
port (
  RST                : IN  std_logic; --Please add a synchroniser if it is not generated in DRPCLK domain.
  RXPMARESET_IN      : IN  std_logic;
  RXPMARESETDONE     : IN  std_logic;
  RXPMARESET_OUT     : OUT std_logic;

  DRPCLK             : IN  std_logic;
  DRPADDR            : OUT std_logic_vector(8 downto 0);
  DRPDO              : IN  std_logic_vector(15 downto 0);
  DRPDI              : OUT std_logic_vector(15 downto 0);
  DRPRDY             : IN  std_logic;
  DRPEN              : OUT std_logic;
  DRPWE              : OUT std_logic;
  DRP_BUSY_IN        : IN  std_logic;
  DRP_PMA_BUSY_OUT   : OUT std_logic
);
END GTP_TILE_A7_RXPMARST_SEQ;



ARCHITECTURE Behavioral of gtp_tile_a7_rxpmarst_seq is


component gtp_tile_a7_sync_block
 generic (
   INITIALISE : bit_vector(1 downto 0) := "00"
 );
 port  (
           clk           : in  std_logic;
           data_in       : in  std_logic;
           data_out      : out std_logic
        );
 end component;

constant DLY                : time := 1 ns;
type state_type is (idle,
                    drp_rd,
                    wait_rd_data,
                    wr_16,
                    wait_wr_done1,
                    wait_pmareset,
                    wr_20,
                    wait_wr_done2,
                    wait_rxpmarst_low);

signal  state               : state_type := idle;
signal  next_state          : state_type := idle;
signal  rxpmareset_s        : std_logic;
signal  rxpmareset_ss       : std_logic;
signal  rxpmaresetdone_ss   : std_logic;
signal  rd_data             : std_logic_vector(15 downto 0);
signal  next_rd_data        : std_logic_vector(15 downto 0);
signal  rxpmareset_i        :std_logic;
signal  rxpmareset_o        :std_logic;
signal  drpen_o             :std_logic;
signal  drpwe_o             :std_logic;
signal  drpaddr_o           :std_logic_vector(8 downto 0);
signal  drpdi_o             :std_logic_vector(15 downto 0);
signal  drp_pma_busy_i      :std_logic;

BEGIN

sync1_RXPMARESETDONE : gtp_tile_a7_sync_block
port map
       (
          clk             =>  DRPCLK,
          data_in         =>  RXPMARESETDONE,
          data_out        =>  rxpmaresetdone_ss
       );


--output assignment
RXPMARESET_OUT      <= rxpmareset_o;
DRPEN               <= drpen_o;
DRPWE               <= drpwe_o;
DRPADDR             <= drpaddr_o;
DRPDI               <= drpdi_o;
DRP_PMA_BUSY_OUT    <= drp_pma_busy_i;

PROCESS (DRPCLK,RST)
BEGIN
  IF (RST = '1') THEN
    state               <= idle              after DLY;
    rxpmareset_s        <= '0'               after DLY;
    rxpmareset_ss       <= '0'               after DLY;
    rd_data             <=  x"0000"          after DLY;
    rxpmareset_o        <= '0'               after DLY;
  ELSIF (DRPCLK'event and DRPCLK='1') THEN
    state               <= next_state        after DLY;
    rxpmareset_s        <= RXPMARESET_IN     after DLY;
    rxpmareset_ss       <= rxpmareset_s      after DLY;
    rd_data             <= next_rd_data      after DLY;
    rxpmareset_o        <= rxpmareset_i      after DLY;
  END IF;
END PROCESS;


PROCESS (rxpmareset_ss,DRPRDY,state,rxpmaresetdone_ss)
BEGIN
  CASE state IS

    WHEN idle         =>
      IF (rxpmareset_ss='1') THEN
        next_state <= drp_rd;
      ELSE
        next_state <= idle;
END IF;

    WHEN drp_rd       =>
next_state<= wait_rd_data;

    WHEN wait_rd_data =>
      IF (DRPRDY='1')THEN
        next_state <= wr_16;
      ELSE
        next_state <= wait_rd_data;
      END IF;

    WHEN wr_16 =>
next_state <= wait_wr_done1;

    WHEN wait_wr_done1 =>
IF (DRPRDY='1') THEN
  next_state <= wait_pmareset;
      ELSE
  next_state <= wait_wr_done1;
      END IF;

    WHEN wait_pmareset =>
IF (rxpmaresetdone_ss = '0') THEN
        next_state <= wr_20;
      ELSE
        next_state <= wait_pmareset;
      END IF;

    WHEN wr_20         =>
      next_state <= wait_wr_done2;

    WHEN wait_wr_done2 =>
IF (DRPRDY='1') THEN
        next_state <= wait_rxpmarst_low;
      ELSE
        next_state <= wait_wr_done2;
END IF;

    WHEN wait_rxpmarst_low =>
      IF (rxpmareset_ss = '0') THEN
        next_state <= idle;
      ELSE
        next_state <= wait_rxpmarst_low;
END IF;

    WHEN others=>
        next_state <= idle;

  END CASE;
END PROCESS;

-- drives DRP interface and RXPMARESET_OUT
PROCESS(DRPRDY,state,rd_data,DRPDO,rxpmareset_ss,DRP_BUSY_IN)
BEGIN
-- RX_DATA_WIDTH is located at addr x"0011", [13 downto 11]
-- encoding is this : /16 = x "2", /20 = x"3", /32 = x"4", /40 = x"5"
  rxpmareset_i  <= '0';
  drpaddr_o    <= '0'& x"11"; -- 000010001
  drpen_o      <= '0';
  drpwe_o      <= '0';
  drpdi_o      <= x"0000";
  next_rd_data <= rd_data;

  CASE state IS

   --do nothing to DRP or reset
    WHEN idle   =>
       drp_pma_busy_i <= '0';

    --assert reset and issue rd
    WHEN drp_rd =>
      if(DRP_BUSY_IN = '0') then
       drp_pma_busy_i <= '1';
        drpen_o        <= '1';
 drpwe_o        <= '0';
      else
       drp_pma_busy_i <= '0';
        drpen_o        <= '0';
 drpwe_o        <= '0';
      end if;

    -- wait to load rd data
    WHEN wait_rd_data =>
       drp_pma_busy_i <= '1';
        IF (DRPRDY='1') THEN
          next_rd_data <= DRPDO;
        ELSE
          next_rd_data <= rd_data;
        END IF;

    -- write to 16-bit mode
    WHEN wr_16=>
      drp_pma_busy_i <= '1';
 drpen_o    <= '1';
drpwe_o    <= '1';
      -- Addr "00001001" [11] = '0' puts width mode in /16 or /32
     drpdi_o <= rd_data(15 downto 12) & '0' & rd_data(10 downto 0);

    --wait for write to 16-bit mode is complete
    WHEN wait_wr_done1=>
      drp_pma_busy_i <= '1';

    --assert rxpmareset and wait for rxpmaresetdone to go low
    WHEN wait_pmareset => null;
      drp_pma_busy_i <= '1';
      rxpmareset_i   <= '1';

    --write to 20-bit mode
    WHEN wr_20 =>
      drp_pma_busy_i <= '1';
      drpen_o <='1';
      drpwe_o <= '1';
      drpdi_o <= rd_data(15 downto 0); --restore user setting per prev read

    --wait to complete write to 20-bit mode
    WHEN wait_wr_done2 =>
         rxpmareset_i    <= '1';
                        drp_pma_busy_i  <= '1';

    -- wait for rxpmareset to be deasserted
    WHEN wait_rxpmarst_low =>
                         drp_pma_busy_i  <= '0';
                         if(rxpmareset_ss = '1') then
             rxpmareset_i <= '1';
                         else
             rxpmareset_i <= '0';
                         end if;

    WHEN others        => null;

   END CASE;
END PROCESS;

END Behavioral;












----------------------------------------------------
--
--  Library Name :  GTP
--  Unit    Name :  GTP_TILE_A7_GTRXRESET_SEQ
--  Unit    Type :  Text Unit
--
------------------------------------------------------
------------------------------------------------------------------------------/
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version : 2.7
--  \   \         Application : 7 Series FPGAs Transceivers Wizard
--  /   /         Filename : gtp_tile_a7_gtrxreset_seq.vhd
-- /___/   /\
-- \   \  /  \
--  \___\/\___\
--
--
-- Module gtp_tile_a7_gtrxreset_seq
-- Generated by Xilinx 7 Series FPGAs Transceivers Wizard
--
-- The reason for using this Reset Sequencer is probably a
-- silicon error in production versions of Artix 7. This
-- makes it neccessary to force a certain mode (16b internal
-- path) in order to achieve successful reset. The sequence
-- is described in Series 7 GTP User Guide v1.4, and on:
-- http://www.xilinx.com/support/answers/53561.html
--
--
-- (c) Copyright 2010-2012 Xilinx, Inc. All rights reserved.
--
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
--
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
--
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
--
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


ENTITY GTP_TILE_A7_GTRXRESET_SEQ IS
port (
  RST           : IN  std_logic; --Please add a synchroniser if it is not generated in DRPCLK domain.
  GTRXRESET_IN  : IN  std_logic; --Please add a synchroniser if it is not generated in DRPCLK domain.
  RXPMARESETDONE: IN  std_logic;
  GTRXRESET_OUT : OUT std_logic;

  DRPCLK        : IN  std_logic;
  DRPADDR       : OUT std_logic_vector(8 downto 0);
  DRPDO	  : IN  std_logic_vector(15 downto 0);
  DRPDI         : OUT std_logic_vector(15 downto 0);
  DRPRDY        : IN  std_logic;
  DRPEN	  : OUT std_logic;
  DRPWE         : OUT std_logic;
  DRP_OP_DONE   : OUT std_logic
);
END GTP_TILE_A7_GTRXRESET_SEQ;



ARCHITECTURE Behavioral of gtp_tile_a7_gtrxreset_seq is

-- ebjoersv has introduced several manual changes in this sequencer
-- to avoid a potential FSM lockup. The fix is described in and
-- fetched from: http://www.xilinx.com/support/answers/60489.html

component gtp_tile_a7_sync_block
 generic (
   INITIALISE : bit_vector(1 downto 0) := "00"
 );
 port  (
           clk           : in  std_logic;
           data_in       : in  std_logic;
           data_out      : out std_logic
        );
 end component;

constant DLY                : time := 1 ns;
type state_type is (idle,
                    drp_rd,
                    wait_rd_data,
                    wr_16,
                    wait_wr_done1,
                    wait_pmareset,
                    wr_20,
                    wait_wr_done2);

signal  state               : state_type := idle;
signal  next_state          : state_type := idle;
signal  gtrxreset_s         : std_logic;
signal  gtrxreset_ss        : std_logic;
signal  rxpmaresetdone_ss   : std_logic;
signal  rxpmaresetdone_sss  : std_logic;
signal  rd_data             : std_logic_vector(15 downto 0);
signal  next_rd_data        : std_logic_vector(15 downto 0);
signal  pmarstdone_fall_edge:std_logic;
signal  gtrxreset_i         :std_logic;
signal  gtrxreset_o         :std_logic;
signal  drpen_o             :std_logic;
signal  drpwe_o             :std_logic;
signal  drpaddr_o           :std_logic_vector(8 downto 0);
signal  drpdi_o             :std_logic_vector(15 downto 0);
signal  drp_op_done_o       :std_logic;

-- !! begin added by ebjoersv
signal flag : std_logic := '0'; -- Default value only for simulation purposes.
signal original_rd_data : std_logic_vector (15 downto 0);
-- !! end added by ebjoersv

BEGIN

sync0_RXPMARESETDONE : gtp_tile_a7_sync_block
port map
       (
          clk             =>  DRPCLK,
          data_in         =>  RXPMARESETDONE,
          data_out        =>  rxpmaresetdone_ss
       );

--output assignment
GTRXRESET_OUT <= gtrxreset_o;
DRPEN         <= drpen_o;
DRPWE         <= drpwe_o;
DRPADDR       <= drpaddr_o;
DRPDI         <= drpdi_o;
DRP_OP_DONE   <= drp_op_done_o;

-- !! begin added by ebjoersv
process (DRPCLK)  -- Reset is intentionally left out.
begin
if rising_edge(DRPCLK) then
  if state = wr_16 or state = wait_pmareset or state = wr_20 or state = wait_wr_done1 then
    flag <= '1';
  elsif (state = wait_wr_done2) then
    flag <= '0';
  end if;
end if;
end process;

process (DRPCLK) -- Reset is intentionally left out.
begin
if rising_edge(DRPCLK) then
  if state = wait_rd_data and DRPRDY = '1' and flag = '0' then
    original_rd_data <= DRPDO;
  end if;
end if;
end process;
-- !! end added by ebjoersv

PROCESS (DRPCLK,RST)
BEGIN
  IF (RST = '1') THEN
    state              <= idle              after DLY;
    gtrxreset_s        <= '0'               after DLY;
    gtrxreset_ss       <= '0'               after DLY;
    rxpmaresetdone_sss <= '0'               after DLY;
    rd_data            <= x"0000"           after DLY;
    gtrxreset_o        <= '0'               after DLY;
  ELSIF (DRPCLK'event and DRPCLK='1') THEN
    state              <= next_state        after DLY;
    gtrxreset_s        <= GTRXRESET_IN      after DLY;
    gtrxreset_ss       <= gtrxreset_s       after DLY;
    rxpmaresetdone_sss <= rxpmaresetdone_ss after DLY;
    rd_data            <= next_rd_data      after DLY;
    gtrxreset_o        <= gtrxreset_i       after DLY;
  END IF;
END PROCESS;

PROCESS (DRPCLK,GTRXRESET_IN)
BEGIN
  IF (GTRXRESET_IN = '1') THEN
     drp_op_done_o  <= '0'       after DLY;

  ELSIF (DRPCLK'event and DRPCLK='1') THEN
    IF (state = wait_wr_done2 and DRPRDY = '1') THEN
       drp_op_done_o  <= '1'       after DLY;
    ELSE
       drp_op_done_o  <= drp_op_done_o       after DLY;
    END IF;
  END IF;
END PROCESS;

pmarstdone_fall_edge <= (not rxpmaresetdone_ss) and (rxpmaresetdone_sss);

PROCESS (gtrxreset_ss,DRPRDY,state,pmarstdone_fall_edge)
BEGIN
  CASE state IS

    WHEN idle         =>
      IF (gtrxreset_ss='1') THEN
        next_state <= drp_rd;
      ELSE
        next_state <= idle;
END IF;

    WHEN drp_rd       =>
next_state<= wait_rd_data;

    WHEN wait_rd_data =>
      IF (DRPRDY='1')THEN
        next_state <= wr_16;
      ELSE
        next_state <= wait_rd_data;
      END IF;

    WHEN wr_16 =>
next_state <= wait_wr_done1;

    WHEN wait_wr_done1 =>
IF (DRPRDY='1') THEN
  next_state <= wait_pmareset;
      ELSE
  next_state <= wait_wr_done1;
      END IF;

    WHEN wait_pmareset =>
IF (pmarstdone_fall_edge='1') THEN
        next_state <= wr_20;
      ELSE
        next_state <= wait_pmareset;
      END IF;

    WHEN wr_20         =>
      next_state <= wait_wr_done2;

    WHEN wait_wr_done2 =>
IF (DRPRDY='1') THEN
        next_state <= idle;
      ELSE
        next_state <= wait_wr_done2;
END IF;

    WHEN others=>
        next_state <= idle;

  END CASE;
END PROCESS;

-- drives DRP interface and GTRXRESET_OUT
-- !! begin edited by ebjoersv
PROCESS(DRPRDY,state,rd_data,DRPDO,gtrxreset_ss, flag, original_rd_data)
-- !! end edited by ebjoersv
BEGIN
-- assert gtrxreset_out until wr to 16-bit is complete
-- RX_DATA_WIDTH is located at addr x"0011", [13 downto 11]
-- encoding is this : /16 = x "2", /20 = x"3", /32 = x"4", /40 = x"5"
  gtrxreset_i  <= '0';
  drpaddr_o    <= '0'& x"11"; -- 000010001
  drpen_o      <= '0';
  drpwe_o      <= '0';
  drpdi_o      <= x"0000";
  next_rd_data <= rd_data;

  CASE state IS

      --do nothing to DRP or reset
      WHEN idle   => null;

      --assert reset and issue rd
      WHEN drp_rd =>
          gtrxreset_i <= '1';
          drpen_o     <= '1';
          drpwe_o     <= '0';

      --assert reset and wait to load rd data
      WHEN wait_rd_data =>
          gtrxreset_i <= '1';
          -- !! begin edited by ebjoersv
          IF (DRPRDY='1' and flag='0') THEN
              next_rd_data <= DRPDO;
          elsif DRPRDY='1' and flag='1' then
              next_rd_data <= original_rd_data;
          else
              next_rd_data <= rd_data;
          END IF;
          -- !! end edited by ebjoersv

      --assert reset and write to 16-bit mode
      WHEN wr_16=>
          gtrxreset_i<= '1';
          drpen_o    <= '1';
          drpwe_o    <= '1';
          -- Addr "00001001" [11] = '0' puts width mode in /16 or /32
          drpdi_o <= rd_data(15 downto 12) & '0' & rd_data(10 downto 0);

      --keep asserting reset until write to 16-bit mode is complete
      WHEN wait_wr_done1=>
          gtrxreset_i <= '1';

      --deassert reset and no DRP access until 2nd pmareset
      WHEN wait_pmareset => null;
          IF (gtrxreset_ss='1') THEN
              gtrxreset_i <= '1';
          ELSE
              gtrxreset_i <= '0';
          END IF;

      --write to 20-bit mode
      WHEN wr_20 =>
          drpen_o <='1';
          drpwe_o <= '1';
          drpdi_o <= rd_data(15 downto 0); --restore user setting per prev read

      --wait to complete write to 20-bit mode
      WHEN wait_wr_done2 => null;

      WHEN others        => null;

   END CASE;
END PROCESS;

END Behavioral;












----------------------------------------------------
--
--  Library Name :  GTP
--  Unit    Name :  GTP_TILE_A7_GT
--  Unit    Type :  Text Unit
--
------------------------------------------------------
-------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version : 2.7
--  \   \         Application : 7 Series FPGAs Transceivers Wizard
--  /   /         Filename : gtp_tile_a7_gt.vhd
-- /___/   /\
-- \   \  /  \
--  \___\/\___\
--
--
-- Module gtp_tile_a7_GT (a GT Wrapper)
-- Generated by Xilinx 7 Series FPGAs Transceivers Wizard
--
--
-- (c) Copyright 2010-2012 Xilinx, Inc. All rights reserved.
--
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
--
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
--
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
--
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
library work;
use work.GTP_TILE_PKG.ALL;

--***************************** Entity Declaration ****************************

entity GTP_TILE_A7_GT is
generic
(
  -- Simulation attributes
  GT_SIM_GTRESET_SPEEDUP    : string  := "FALSE"; -- Set to "true" to speed up sim reset
  TXSYNC_OVRD_IN            : bit     := '0';
  TXSYNC_MULTILANE_IN       : bit     := '0';

  -- LINE RATE SELECT
  G_RATE : GTP_RATE_TYPE := GBPS_3_0
);
port
(
RST_IN         : in   std_logic;          -- Connect to System Reset
DRP_BUSY_OUT   : out  std_logic;          -- Indicates that the DRP bus is not accessible to the User
  ---------------------------- Channel - DRP Ports  --------------------------
  DRPADDR_IN                              : in   std_logic_vector(8 downto 0);
  DRPCLK_IN                               : in   std_logic;
  DRPDI_IN                                : in   std_logic_vector(15 downto 0);
  DRPDO_OUT                               : out  std_logic_vector(15 downto 0);
  DRPEN_IN                                : in   std_logic;
  DRPRDY_OUT                              : out  std_logic;
  DRPWE_IN                                : in   std_logic;
  ------------------------ GTPE2_CHANNEL Clocking Ports ----------------------
  PLL0CLK_IN                              : in   std_logic;
  PLL0REFCLK_IN                           : in   std_logic;
  PLL1CLK_IN                              : in   std_logic;
  PLL1REFCLK_IN                           : in   std_logic;
  --------------------- RX Initialization and Reset Ports --------------------
  RXUSERRDY_IN                            : in   std_logic;
  -------------------------- RX Margin Analysis Ports ------------------------
  EYESCANDATAERROR_OUT                    : out  std_logic;
  ------------------------- Receive Ports - CDR Ports ------------------------
  RXCDRHOLD_IN                            : in   std_logic;
  RXCDRLOCK_OUT                           : out  std_logic;
  ------------------- Receive Ports - Clock Correction Ports -----------------
  RXCLKCORCNT_OUT                         : out  std_logic_vector(1 downto 0);
  ------------------ Receive Ports - FPGA RX Interface Ports -----------------
  RXDATA_OUT                              : out  std_logic_vector(15 downto 0);
  RXUSRCLK_IN                             : in   std_logic;
  RXUSRCLK2_IN                            : in   std_logic;
  ------------------- Receive Ports - Pattern Checker Ports ------------------
  RXPRBSERR_OUT                           : out  std_logic;
  RXPRBSSEL_IN                            : in   std_logic_vector(2 downto 0);
  ------------------- Receive Ports - Pattern Checker ports ------------------
  RXPRBSCNTRESET_IN                       : in   std_logic;
  ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
  RXCHARISCOMMA_OUT                       : out  std_logic_vector(1 downto 0);
  RXCHARISK_OUT                           : out  std_logic_vector(1 downto 0);
  RXDISPERR_OUT                           : out  std_logic_vector(1 downto 0);
  RXNOTINTABLE_OUT                        : out  std_logic_vector(1 downto 0);
  ------------------------ Receive Ports - RX AFE Ports ----------------------
  GTPRXN_IN                               : in   std_logic;
  GTPRXP_IN                               : in   std_logic;
  ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
  RXBUFRESET_IN                           : in   std_logic;
  RXBUFSTATUS_OUT                         : out  std_logic_vector(2 downto 0);
  -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
  RXBYTEISALIGNED_OUT                     : out  std_logic;
  RXMCOMMAALIGNEN_IN                      : in   std_logic;
  RXPCOMMAALIGNEN_IN                      : in   std_logic;
  -------------------- Receive Ports - RX Equailizer Ports -------------------
  RXLPMHFHOLD_IN                          : in   std_logic;
  RXLPMLFHOLD_IN                          : in   std_logic;
  --------------- Receive Ports - RX Fabric Output Control Ports -------------
  RXOUTCLK_OUT                            : out  std_logic;
  ------------- Receive Ports - RX Initialization and Reset Ports ------------
  GTRXRESET_IN                            : in   std_logic;
  RXPCSRESET_IN                           : in   std_logic;
  RXPMARESET_IN                           : in   std_logic;
  -------------- Receive Ports -RX Initialization and Reset Ports ------------
  RXRESETDONE_OUT                         : out  std_logic;
  --------------------- TX Initialization and Reset Ports --------------------
  GTTXRESET_IN                            : in   std_logic;
  TXUSERRDY_IN                            : in   std_logic;
  ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
  TXDATA_IN                               : in   std_logic_vector(15 downto 0);
  TXUSRCLK_IN                             : in   std_logic;
  TXUSRCLK2_IN                            : in   std_logic;
  ------------------ Transmit Ports - Pattern Generator Ports ----------------
  TXPRBSFORCEERR_IN                       : in   std_logic;
  ------------------ Transmit Ports - TX 8B/10B Encoder Ports ----------------
  TX8B10BBYPASS_IN                        : in   std_logic_vector(1 downto 0);
  TXCHARDISPMODE_IN                       : in   std_logic_vector(1 downto 0);
  TXCHARDISPVAL_IN                        : in   std_logic_vector(1 downto 0);
  TXCHARISK_IN                            : in   std_logic_vector(1 downto 0);
  ---------------------- Transmit Ports - TX Buffer Ports --------------------
  TXBUFSTATUS_OUT                         : out  std_logic_vector(1 downto 0);
  --------------- Transmit Ports - TX Configurable Driver Ports --------------
  GTPTXN_OUT                              : out  std_logic;
  GTPTXP_OUT                              : out  std_logic;
  ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
  TXOUTCLK_OUT                            : out  std_logic;
  TXOUTCLKFABRIC_OUT                      : out  std_logic;
  TXOUTCLKPCS_OUT                         : out  std_logic;
  ------------- Transmit Ports - TX Initialization and Reset Ports -----------
  TXPCSRESET_IN                           : in   std_logic;
  TXPMARESET_IN                           : in   std_logic;
  TXRESETDONE_OUT                         : out  std_logic;
  ------------------ Transmit Ports - pattern Generator Ports ----------------
  TXPRBSSEL_IN                            : in   std_logic_vector(2 downto 0)


);


end GTP_TILE_A7_GT;



architecture RTL of GTP_TILE_A7_GT is

--*************************** Component Declarations **************************
component gtp_tile_a7_gtrxreset_seq
port (
  RST           : IN  std_logic;
  GTRXRESET_IN  : IN  std_logic;
  RXPMARESETDONE: IN  std_logic;
  GTRXRESET_OUT : OUT std_logic;

  DRPCLK        : IN  std_logic;
  DRPADDR       : OUT std_logic_vector(8 downto 0);
  DRPDO	  : IN  std_logic_vector(15 downto 0);
  DRPDI         : OUT std_logic_vector(15 downto 0);
  DRPRDY        : IN  std_logic;
  DRPEN	  : OUT std_logic;
  DRPWE         : OUT std_logic;
  DRP_OP_DONE   : OUT std_logic
);
end component;

component gtp_tile_a7_rxpmarst_seq
port (
  RST                : IN  std_logic;
  RXPMARESET_IN      : IN  std_logic;
  RXPMARESETDONE     : IN  std_logic;
  RXPMARESET_OUT     : OUT std_logic;

  DRPCLK             : IN  std_logic;
  DRPADDR            : OUT std_logic_vector(8 downto 0);
  DRPDO              : IN  std_logic_vector(15 downto 0);
  DRPDI              : OUT std_logic_vector(15 downto 0);
  DRPRDY             : IN  std_logic;
  DRPEN              : OUT std_logic;
  DRPWE              : OUT std_logic;
  DRP_BUSY_IN        : IN  std_logic;
  DRP_PMA_BUSY_OUT   : OUT std_logic
);
end component;


component gtp_tile_a7_rxrate_seq
port (
  RST                : IN  std_logic;
  RXRATE_IN          : IN  std_logic_vector(2 downto 0);
  RXPMARESETDONE     : IN  std_logic;
  RXRATE_OUT         : OUT std_logic_vector(2 downto 0);

  DRPCLK             : IN  std_logic;
  DRPADDR            : OUT std_logic_vector(8 downto 0);
  DRPDO              : IN  std_logic_vector(15 downto 0);
  DRPDI              : OUT std_logic_vector(15 downto 0);
  DRPRDY             : IN  std_logic;
  DRPEN              : OUT std_logic;
  DRPWE              : OUT std_logic;
  DRP_BUSY_IN        : IN  std_logic;
  DRP_RATE_BUSY_OUT  : OUT std_logic
);
end component;

--**************************** Signal Declarations ****************************

  -- ground and tied_to_vcc_i signals
  signal  tied_to_ground_i                :   std_logic;
  signal  tied_to_ground_vec_i            :   std_logic_vector(63 downto 0);
  signal  tied_to_vcc_i                   :   std_logic;


  signal    rxpmaresetdone_t                : std_logic;
  signal    gtrxreset_out                   : std_logic;
  signal    rxpmareset_out                  : std_logic;
  signal    rxrate_out                      : std_logic_vector(2 downto 0);
  signal    drp_op_done                     : std_logic;
  signal    drp_pma_busy                    : std_logic;
  signal    drp_rate_busy                   : std_logic;
  signal    drp_busy_i1                     : std_logic:= '0';
  signal    drp_busy_i2                     : std_logic:= '0';
  signal    drpen_rst_t                     : std_logic;
  signal    drpaddr_rst_t                   : std_logic_vector(8 downto 0);
  signal    drpwe_rst_t                     : std_logic;
  signal    drpdo_rst_t                     : std_logic_vector(15 downto 0);
  signal    drpdi_rst_t                     : std_logic_vector(15 downto 0);
  signal    drprdy_rst_t                    : std_logic;
  signal    drpen_pma_t                     : std_logic;
  signal    drpaddr_pma_t                   : std_logic_vector(8 downto 0);
  signal    drpwe_pma_t                     : std_logic;
  signal    drpdo_pma_t                     : std_logic_vector(15 downto 0);
  signal    drpdi_pma_t                     : std_logic_vector(15 downto 0);
  signal    drprdy_pma_t                    : std_logic;
  signal    drpen_rate_t                    : std_logic;
  signal    drpaddr_rate_t                  : std_logic_vector(8 downto 0);
  signal    drpwe_rate_t                    : std_logic;
  signal    drpdo_rate_t                    : std_logic_vector(15 downto 0);
  signal    drpdi_rate_t                    : std_logic_vector(15 downto 0);
  signal    drprdy_rate_t                   : std_logic;
  signal    drpen_i                         : std_logic;
  signal    drpaddr_i                       : std_logic_vector(8 downto 0);
  signal    drpwe_i                         : std_logic;
  signal    drpdo_i                         : std_logic_vector(15 downto 0);
  signal    drpdi_i                         : std_logic_vector(15 downto 0);
  signal    drprdy_i                        : std_logic;

  -- RX Datapath signals
  signal rxdata_i                         :   std_logic_vector(31 downto 0);
  signal rxchariscomma_float_i            :   std_logic_vector(1 downto 0);
  signal rxcharisk_float_i                :   std_logic_vector(1 downto 0);
  signal rxdisperr_float_i                :   std_logic_vector(1 downto 0);
  signal rxnotintable_float_i             :   std_logic_vector(1 downto 0);
  signal rxrundisp_float_i                :   std_logic_vector(1 downto 0);


  -- TX Datapath signals
  signal txdata_i                         :   std_logic_vector(31 downto 0);
  signal txkerr_float_i                   :   std_logic_vector(1 downto 0);
  signal txrundisp_float_i                :   std_logic_vector(1 downto 0);
  signal rxdatavalid_float_i              :   std_logic;
--******************************** Main Body of Code***************************

begin

  ---------------------------  Static signal Assignments ---------------------

  tied_to_ground_i                    <= '0';
  tied_to_ground_vec_i(63 downto 0)   <= (others => '0');
  tied_to_vcc_i                       <= '1';

  -------------------  GT Datapath byte mapping  -----------------

  RXDATA_OUT    <=   rxdata_i(15 downto 0);

  txdata_i    <=   (tied_to_ground_vec_i(15 downto 0) & TXDATA_IN);



 ----------------------------- GTPE2 Instance  --------------------------

  gtpe2_i : GTPE2_CHANNEL
  generic map
  (

  --_______________________ Simulation-Only Attributes ___________________

  SIM_RECEIVER_DETECT_PASS   =>      ("TRUE"),
  SIM_RESET_SPEEDUP          =>      (GT_SIM_GTRESET_SPEEDUP),
  SIM_TX_EIDLE_DRIVE_LEVEL   =>      ("X"),
  SIM_VERSION                =>      ("1.0"),


  ------------------RX Byte and Word Alignment Attributes---------------
  ALIGN_COMMA_DOUBLE                      =>     ("FALSE"),
  ALIGN_COMMA_ENABLE                      =>     ("1111111111"),
  ALIGN_COMMA_WORD                        =>     (2),
  ALIGN_MCOMMA_DET                        =>     ("TRUE"),
  ALIGN_MCOMMA_VALUE                      =>     ("1010000011"),
  ALIGN_PCOMMA_DET                        =>     ("TRUE"),
  ALIGN_PCOMMA_VALUE                      =>     ("0101111100"),
  SHOW_REALIGN_COMMA                      =>     ("TRUE"),
  RXSLIDE_AUTO_WAIT                       =>     (7),
  RXSLIDE_MODE                            =>     ("OFF"),
  RX_SIG_VALID_DLY                        =>     (10),

  ------------------RX 8B/10B Decoder Attributes---------------
  RX_DISPERR_SEQ_MATCH                    =>     ("TRUE"),
  DEC_MCOMMA_DETECT                       =>     ("TRUE"),
  DEC_PCOMMA_DETECT                       =>     ("TRUE"),
  DEC_VALID_COMMA_ONLY                    =>     ("TRUE"),

  ------------------------RX Clock Correction Attributes----------------------
  CBCC_DATA_SOURCE_SEL                    =>     ("DECODED"),
  CLK_COR_SEQ_2_USE                       =>     ("FALSE"),
  CLK_COR_KEEP_IDLE                       =>     ("FALSE"),
  CLK_COR_MAX_LAT                         =>     (15),
  CLK_COR_MIN_LAT                         =>     (12),
  CLK_COR_PRECEDENCE                      =>     ("TRUE"),
  CLK_COR_REPEAT_WAIT                     =>     (0),
  CLK_COR_SEQ_LEN                         =>     (2),
  CLK_COR_SEQ_1_ENABLE                    =>     ("0001"),
  CLK_COR_SEQ_1_1                         =>     ("0110111100"),
  CLK_COR_SEQ_1_2                         =>     ("0000000000"),
  CLK_COR_SEQ_1_3                         =>     ("0000000000"),
  CLK_COR_SEQ_1_4                         =>     ("0000000000"),
  CLK_CORRECT_USE                         =>     ("TRUE"),
  CLK_COR_SEQ_2_ENABLE                    =>     ("0000"),
  CLK_COR_SEQ_2_1                         =>     ("0110111100"),
  CLK_COR_SEQ_2_2                         =>     ("0000000000"),
  CLK_COR_SEQ_2_3                         =>     ("0000000000"),
  CLK_COR_SEQ_2_4                         =>     ("0000000000"),

  ------------------------RX Channel Bonding Attributes----------------------
  CHAN_BOND_KEEP_ALIGN                    =>     ("FALSE"),
  CHAN_BOND_MAX_SKEW                      =>     (1),
  CHAN_BOND_SEQ_LEN                       =>     (1),
  CHAN_BOND_SEQ_1_1                       =>     ("0000000000"),
  CHAN_BOND_SEQ_1_2                       =>     ("0000000000"),
  CHAN_BOND_SEQ_1_3                       =>     ("0000000000"),
  CHAN_BOND_SEQ_1_4                       =>     ("0000000000"),
  CHAN_BOND_SEQ_1_ENABLE                  =>     ("1111"),
  CHAN_BOND_SEQ_2_1                       =>     ("0000000000"),
  CHAN_BOND_SEQ_2_2                       =>     ("0000000000"),
  CHAN_BOND_SEQ_2_3                       =>     ("0000000000"),
  CHAN_BOND_SEQ_2_4                       =>     ("0000000000"),
  CHAN_BOND_SEQ_2_ENABLE                  =>     ("1111"),
  CHAN_BOND_SEQ_2_USE                     =>     ("FALSE"),
  FTS_DESKEW_SEQ_ENABLE                   =>     ("1111"),
  FTS_LANE_DESKEW_CFG                     =>     ("1111"),
  FTS_LANE_DESKEW_EN                      =>     ("FALSE"),

  ---------------------------RX Margin Analysis Attributes----------------------------
  ES_CONTROL                              =>     ("000000"),
  ES_ERRDET_EN                            =>     ("FALSE"),
  ES_EYE_SCAN_EN                          =>     ("FALSE"),
  ES_HORZ_OFFSET                          =>     (x"010"),
  ES_PMA_CFG                              =>     ("0000000000"),
  ES_PRESCALE                             =>     ("00000"),
  ES_QUALIFIER                            =>     (x"00000000000000000000"),
  ES_QUAL_MASK                            =>     (x"00000000000000000000"),
  ES_SDATA_MASK                           =>     (x"00000000000000000000"),
  ES_VERT_OFFSET                          =>     ("000000000"),

  -------------------------FPGA RX Interface Attributes-------------------------
  RX_DATA_WIDTH                           =>     (20),

  ---------------------------PMA Attributes----------------------------
  OUTREFCLK_SEL_INV                       =>     ("11"),
  PMA_RSV                                 =>     (x"00000333"),
  PMA_RSV2                                =>     (x"00002040"),
  PMA_RSV3                                =>     ("00"),
  PMA_RSV4                                =>     ("0000"),
  RX_BIAS_CFG                             =>     ("0000111100110011"),
  DMONITOR_CFG                            =>     (x"000A00"),
  RX_CM_SEL                               =>     ("00"),
  RX_CM_TRIM                              =>     ("0000"),
  RX_DEBUG_CFG                            =>     ("00000000000000"),
  RX_OS_CFG                               =>     ("0000010000000"),
  TERM_RCAL_CFG                           =>     ("100001000010000"),
  TERM_RCAL_OVRD                          =>     ("000"),
  TST_RSV                                 =>     (x"00000000"),
  RX_CLK25_DIV                            =>     (8),
  TX_CLK25_DIV                            =>     (8),
  UCODEER_CLR                             =>     ('0'),

  ---------------------------PCI Express Attributes----------------------------
  PCS_PCIE_EN                             =>     ("FALSE"),

  ---------------------------PCS Attributes----------------------------
  PCS_RSVD_ATTR                           =>     (x"000000000000"),

  -------------RX Buffer Attributes------------
  RXBUF_ADDR_MODE                         =>     ("FULL"),
  RXBUF_EIDLE_HI_CNT                      =>     ("1000"),
  RXBUF_EIDLE_LO_CNT                      =>     ("0000"),
  RXBUF_EN                                =>     ("TRUE"),
  RX_BUFFER_CFG                           =>     ("000000"),
  RXBUF_RESET_ON_CB_CHANGE                =>     ("FALSE"),
  RXBUF_RESET_ON_COMMAALIGN               =>     ("TRUE"),
  RXBUF_RESET_ON_EIDLE                    =>     ("FALSE"),
  RXBUF_RESET_ON_RATE_CHANGE              =>     ("FALSE"),
  RXBUFRESET_TIME                         =>     ("00001"),
  RXBUF_THRESH_OVFLW                      =>     (61),
  RXBUF_THRESH_OVRD                       =>     ("FALSE"),
  RXBUF_THRESH_UNDFLW                     =>     (4),
  RXDLY_CFG                               =>     (x"001F"),
  RXDLY_LCFG                              =>     (x"030"),
  RXDLY_TAP_CFG                           =>     (x"0000"),
  RXPH_CFG                                =>     (x"C00002"),
  RXPHDLY_CFG                             =>     (x"084020"),
  RXPH_MONITOR_SEL                        =>     ("00000"),
  RX_XCLK_SEL                             =>     ("RXREC"),
  RX_DDI_SEL                              =>     ("000000"),
  RX_DEFER_RESET_BUF_EN                   =>     ("TRUE"),

  -----------------------CDR Attributes-------------------------

  --For GTX only: Display Port, HBR/RBR- set RXCDR_CFG=72'h0380008bff40200008

  --For GTX only: Display Port, HBR2 -   set RXCDR_CFG=72'h038C008bff20200010
  RXCDR_CFG                               =>     (F_A7_RXCDR_CFG(G_RATE)),
  RXCDR_FR_RESET_ON_EIDLE                 =>     ('0'),
  RXCDR_HOLD_DURING_EIDLE                 =>     ('0'),
  RXCDR_PH_RESET_ON_EIDLE                 =>     ('0'),
  RXCDR_LOCK_CFG                          =>     ("001001"),

  -------------------RX Initialization and Reset Attributes-------------------
  RXCDRFREQRESET_TIME                     =>     ("00001"),
  RXCDRPHRESET_TIME                       =>     ("00001"),
  RXISCANRESET_TIME                       =>     ("00001"),
  RXPCSRESET_TIME                         =>     ("00001"),
  RXPMARESET_TIME                         =>     ("00011"),

  -------------------RX OOB Signaling Attributes-------------------
  RXOOB_CFG                               =>     ("0000110"),

  -------------------------RX Gearbox Attributes---------------------------
  RXGEARBOX_EN                            =>     ("FALSE"),
  GEARBOX_MODE                            =>     ("000"),

  -------------------------PRBS Detection Attribute-----------------------
  RXPRBS_ERR_LOOPBACK                     =>     ('0'),

  -------------Power-Down Attributes----------
  PD_TRANS_TIME_FROM_P2                   =>     (x"03c"),
  PD_TRANS_TIME_NONE_P2                   =>     (x"3c"),
  PD_TRANS_TIME_TO_P2                     =>     (x"64"),

  -------------RX OOB Signaling Attributes----------
  SAS_MAX_COM                             =>     (64),
  SAS_MIN_COM                             =>     (36),
  SATA_BURST_SEQ_LEN                      =>     ("1111"),
  SATA_BURST_VAL                          =>     ("100"),
  SATA_EIDLE_VAL                          =>     ("100"),
  SATA_MAX_BURST                          =>     (8),
  SATA_MAX_INIT                           =>     (21),
  SATA_MAX_WAKE                           =>     (7),
  SATA_MIN_BURST                          =>     (4),
  SATA_MIN_INIT                           =>     (12),
  SATA_MIN_WAKE                           =>     (4),

  -------------RX Fabric Clock Output Control Attributes----------
  TRANS_TIME_RATE                         =>     (x"0E"),

  --------------TX Buffer Attributes----------------
  TXBUF_EN                                =>     ("TRUE"),
  TXBUF_RESET_ON_RATE_CHANGE              =>     ("TRUE"),
  TXDLY_CFG                               =>     (x"001F"),
  TXDLY_LCFG                              =>     (x"030"),
  TXDLY_TAP_CFG                           =>     (x"0000"),
  TXPH_CFG                                =>     (x"0780"),
  TXPHDLY_CFG                             =>     (x"084020"),
  TXPH_MONITOR_SEL                        =>     ("00000"),
  TX_XCLK_SEL                             =>     ("TXOUT"),

  -------------------------FPGA TX Interface Attributes-------------------------
  TX_DATA_WIDTH                           =>     (20),

  -------------------------TX Configurable Driver Attributes-------------------------
  TX_DEEMPH0                              =>     ("000000"),
  TX_DEEMPH1                              =>     ("000000"),
  TX_EIDLE_ASSERT_DELAY                   =>     ("110"),
  TX_EIDLE_DEASSERT_DELAY                 =>     ("100"),
  TX_LOOPBACK_DRIVE_HIZ                   =>     ("FALSE"),
  TX_MAINCURSOR_SEL                       =>     ('0'),
  TX_DRIVE_MODE                           =>     ("DIRECT"),
  TX_MARGIN_FULL_0                        =>     ("1001110"),
  TX_MARGIN_FULL_1                        =>     ("1001001"),
  TX_MARGIN_FULL_2                        =>     ("1000101"),
  TX_MARGIN_FULL_3                        =>     ("1000010"),
  TX_MARGIN_FULL_4                        =>     ("1000000"),
  TX_MARGIN_LOW_0                         =>     ("1000110"),
  TX_MARGIN_LOW_1                         =>     ("1000100"),
  TX_MARGIN_LOW_2                         =>     ("1000010"),
  TX_MARGIN_LOW_3                         =>     ("1000000"),
  TX_MARGIN_LOW_4                         =>     ("1000000"),

  -------------------------TX Gearbox Attributes--------------------------
  TXGEARBOX_EN                            =>     ("FALSE"),

  -------------------------TX Initialization and Reset Attributes--------------------------
  TXPCSRESET_TIME                         =>     ("00001"),
  TXPMARESET_TIME                         =>     ("00001"),

  -------------------------TX Receiver Detection Attributes--------------------------
  TX_RXDETECT_CFG                         =>     (x"1832"),
  TX_RXDETECT_REF                         =>     ("100"),

  ------------------ JTAG Attributes ---------------
  ACJTAG_DEBUG_MODE                       =>     ('0'),
  ACJTAG_MODE                             =>     ('0'),
  ACJTAG_RESET                            =>     ('0'),

  ------------------ CDR Attributes ---------------
  CFOK_CFG                                =>     (x"49000040E80"),
  CFOK_CFG2                               =>     ("0100000"),
  CFOK_CFG3                               =>     ("0100000"),
  CFOK_CFG4                               =>     ('0'),
  CFOK_CFG5                               =>     (x"0"),
  CFOK_CFG6                               =>     ("0000"),
  RXOSCALRESET_TIME                       =>     ("00011"),
  RXOSCALRESET_TIMEOUT                    =>     ("00000"),

  ------------------ PMA Attributes ---------------
  CLK_COMMON_SWING                        =>     ('0'),
  RX_CLKMUX_EN                            =>     ('1'),
  TX_CLKMUX_EN                            =>     ('1'),
  ES_CLK_PHASE_SEL                        =>     ('0'),
  USE_PCS_CLK_PHASE_SEL                   =>     ('0'),
  PMA_RSV6                                =>     ('0'),
  PMA_RSV7                                =>     ('0'),

  ------------------ TX Configuration Driver Attributes ---------------
  TX_PREDRIVER_MODE                       =>     ('0'),
  PMA_RSV5                                =>     ('0'),
  SATA_PLL_CFG                            =>     ("VCO_3000MHZ"),

  ------------------ RX Fabric Clock Output Control Attributes ---------------
  RXOUT_DIV                               =>     (F_A7_TRXOUT_DIV(G_RATE)),

  ------------------ TX Fabric Clock Output Control Attributes ---------------
  TXOUT_DIV                               =>     (F_A7_TRXOUT_DIV(G_RATE)),

  ------------------ RX Phase Interpolator Attributes---------------
  RXPI_CFG0                               =>     ("000"),
  RXPI_CFG1                               =>     ('1'),
  RXPI_CFG2                               =>     ('1'),

  --------------RX Equalizer Attributes-------------
  ADAPT_CFG0                              =>     (x"00000"),
  RXLPMRESET_TIME                         =>     ("0001111"),
  RXLPM_BIAS_STARTUP_DISABLE              =>     ('0'),
  RXLPM_CFG                               =>     ("0110"),
  RXLPM_CFG1                              =>     ('0'),
  RXLPM_CM_CFG                            =>     ('0'),
  RXLPM_GC_CFG                            =>     ("111100010"),
  RXLPM_GC_CFG2                           =>     ("001"),
  RXLPM_HF_CFG                            =>     ("00001111110000"),
  RXLPM_HF_CFG2                           =>     ("01010"),
  RXLPM_HF_CFG3                           =>     ("0000"),
  RXLPM_HOLD_DURING_EIDLE                 =>     ('0'),
  RXLPM_INCM_CFG                          =>     ('1'),
  RXLPM_IPCM_CFG                          =>     ('0'),
  RXLPM_LF_CFG                            =>     ("000000001111110000"),
  RXLPM_LF_CFG2                           =>     ("01010"),
  RXLPM_OSINT_CFG                         =>     ("100"),

  ------------------ TX Phase Interpolator PPM Controller Attributes---------------
  TXPI_CFG0                               =>     ("00"),
  TXPI_CFG1                               =>     ("00"),
  TXPI_CFG2                               =>     ("00"),
  TXPI_CFG3                               =>     ('0'),
  TXPI_CFG4                               =>     ('0'),
  TXPI_CFG5                               =>     ("000"),
  TXPI_GREY_SEL                           =>     ('0'),
  TXPI_INVSTROBE_SEL                      =>     ('0'),
  TXPI_PPMCLK_SEL                         =>     ("TXUSRCLK2"),
  TXPI_PPM_CFG                            =>     (x"00"),
  TXPI_SYNFREQ_PPM                        =>     ("000"),

  ------------------ LOOPBACK Attributes---------------
  LOOPBACK_CFG                            =>     ('0'),
  PMA_LOOPBACK_CFG                        =>     ('0'),

  ------------------RX OOB Signalling Attributes---------------
  RXOOB_CLK_CFG                           =>     ("PMA"),

  ------------------TX OOB Signalling Attributes---------------
  TXOOB_CFG                               =>     ('0'),

  ------------------RX Buffer Attributes---------------
  RXSYNC_MULTILANE                        =>     ('0'),
  RXSYNC_OVRD                             =>     ('0'),
  RXSYNC_SKIP_DA                          =>     ('0'),

  ------------------TX Buffer Attributes---------------
  TXSYNC_MULTILANE                        =>     (TXSYNC_MULTILANE_IN),
  TXSYNC_OVRD                             =>     (TXSYNC_OVRD_IN),
  TXSYNC_SKIP_DA                          =>     ('0')

  )
  port map
  (
      --------------------------------- CPLL Ports -------------------------------
      GTRSVD                          =>      "0000000000000000",
      PCSRSVDIN                       =>      "0000000000000000",
      TSTIN                           =>      "11111111111111111111",
      ---------------------------- Channel - DRP Ports  --------------------------
      DRPADDR                         =>      drpaddr_i,
      DRPCLK                          =>      DRPCLK_IN,
      DRPDI                           =>      drpdi_i,
      DRPDO                           =>      drpdo_i,
      DRPEN                           =>      drpen_i,
      DRPRDY                          =>      drprdy_i,
      DRPWE                           =>      drpwe_i,
      ------------------------------- Clocking Ports -----------------------------
      RXSYSCLKSEL                     =>      "00",
      TXSYSCLKSEL                     =>      "00",
      ----------------- FPGA TX Interface Datapath Configuration  ----------------
      TX8B10BEN                       =>      tied_to_vcc_i,
      ------------------------ GTPE2_CHANNEL Clocking Ports ----------------------
      PLL0CLK                         =>      PLL0CLK_IN,
      PLL0REFCLK                      =>      PLL0REFCLK_IN,
      PLL1CLK                         =>      PLL1CLK_IN,
      PLL1REFCLK                      =>      PLL1REFCLK_IN,
      ------------------------------- Loopback Ports -----------------------------
      LOOPBACK                        =>      tied_to_ground_vec_i(2 downto 0),
      ----------------------------- PCI Express Ports ----------------------------
      PHYSTATUS                       =>      open,
      RXRATE                          =>      tied_to_ground_vec_i(2 downto 0),
      RXVALID                         =>      open,
      ----------------------------- PMA Reserved Ports ---------------------------
      PMARSVDIN3                      =>      '0',
      PMARSVDIN4                      =>      '0',
      ------------------------------ Power-Down Ports ----------------------------
      RXPD                            =>      "00",
      TXPD                            =>      "00",
      -------------------------- RX 8B/10B Decoder Ports -------------------------
      SETERRSTATUS                    =>      tied_to_ground_i,
      --------------------- RX Initialization and Reset Ports --------------------
      EYESCANRESET                    =>      tied_to_ground_i,
      RXUSERRDY                       =>      RXUSERRDY_IN,
      -------------------------- RX Margin Analysis Ports ------------------------
      EYESCANDATAERROR                =>      EYESCANDATAERROR_OUT,
      EYESCANMODE                     =>      tied_to_ground_i,
      EYESCANTRIGGER                  =>      tied_to_ground_i,
      ------------------------------- Receive Ports ------------------------------
      CLKRSVD0                        =>      tied_to_ground_i,
      CLKRSVD1                        =>      tied_to_ground_i,
      DMONFIFORESET                   =>      tied_to_ground_i,
      DMONITORCLK                     =>      tied_to_ground_i,
      RXPMARESETDONE                  =>      rxpmaresetdone_t,
      SIGVALIDCLK                     =>      tied_to_ground_i,
      ------------------------- Receive Ports - CDR Ports ------------------------
      RXCDRFREQRESET                  =>      tied_to_ground_i,
      RXCDRHOLD                       =>      RXCDRHOLD_IN,
      RXCDRLOCK                       =>      RXCDRLOCK_OUT,
      RXCDROVRDEN                     =>      tied_to_ground_i,
      RXCDRRESET                      =>      tied_to_ground_i,
      RXCDRRESETRSV                   =>      tied_to_ground_i,
      RXOSCALRESET                    =>      tied_to_ground_i,
      RXOSINTCFG                      =>      "0010",
      RXOSINTDONE                     =>      open,
      RXOSINTHOLD                     =>      tied_to_ground_i,
      RXOSINTOVRDEN                   =>      tied_to_ground_i,
      RXOSINTPD                       =>      tied_to_ground_i,
      RXOSINTSTARTED                  =>      open,
      RXOSINTSTROBE                   =>      tied_to_ground_i,
      RXOSINTSTROBESTARTED            =>      open,
      RXOSINTTESTOVRDEN               =>      tied_to_ground_i,
      ------------------- Receive Ports - Clock Correction Ports -----------------
      RXCLKCORCNT                     =>      RXCLKCORCNT_OUT,
      ---------- Receive Ports - FPGA RX Interface Datapath Configuration --------
      RX8B10BEN                       =>      tied_to_vcc_i,
      ------------------ Receive Ports - FPGA RX Interface Ports -----------------
      RXDATA                          =>      rxdata_i,
      RXUSRCLK                        =>      RXUSRCLK_IN,
      RXUSRCLK2                       =>      RXUSRCLK2_IN,
      ------------------- Receive Ports - Pattern Checker Ports ------------------
      RXPRBSERR                       =>      RXPRBSERR_OUT,
      RXPRBSSEL                       =>      RXPRBSSEL_IN,
      ------------------- Receive Ports - Pattern Checker ports ------------------
      RXPRBSCNTRESET                  =>      RXPRBSCNTRESET_IN,
      ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
      RXCHARISCOMMA(3 downto 2)       =>      rxchariscomma_float_i,
      RXCHARISCOMMA(1 downto 0)       =>      RXCHARISCOMMA_OUT,
      RXCHARISK(3 downto 2)           =>      rxcharisk_float_i,
      RXCHARISK(1 downto 0)           =>      RXCHARISK_OUT,
      RXDISPERR(3 downto 2)           =>      rxdisperr_float_i,
      RXDISPERR(1 downto 0)           =>      RXDISPERR_OUT,
      RXNOTINTABLE(3 downto 2)        =>      rxnotintable_float_i,
      RXNOTINTABLE(1 downto 0)        =>      RXNOTINTABLE_OUT,
      ------------------------ Receive Ports - RX AFE Ports ----------------------
      GTPRXN                          =>      GTPRXN_IN,
      GTPRXP                          =>      GTPRXP_IN,
      PMARSVDIN2                      =>      '0',
      PMARSVDOUT0                     =>      open,
      PMARSVDOUT1                     =>      open,
      ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
      RXBUFRESET                      =>      RXBUFRESET_IN,
      RXBUFSTATUS                     =>      RXBUFSTATUS_OUT,
      RXDDIEN                         =>      tied_to_ground_i,
      RXDLYBYPASS                     =>      tied_to_vcc_i,
      RXDLYEN                         =>      tied_to_ground_i,
      RXDLYOVRDEN                     =>      tied_to_ground_i,
      RXDLYSRESET                     =>      tied_to_ground_i,
      RXDLYSRESETDONE                 =>      open,
      RXPHALIGN                       =>      tied_to_ground_i,
      RXPHALIGNDONE                   =>      open,
      RXPHALIGNEN                     =>      tied_to_ground_i,
      RXPHDLYPD                       =>      tied_to_ground_i,
      RXPHDLYRESET                    =>      tied_to_ground_i,
      RXPHMONITOR                     =>      open,
      RXPHOVRDEN                      =>      tied_to_ground_i,
      RXPHSLIPMONITOR                 =>      open,
      RXSTATUS                        =>      open,
      RXSYNCALLIN                     =>      tied_to_ground_i,
      RXSYNCDONE                      =>      open,
      RXSYNCIN                        =>      tied_to_ground_i,
      RXSYNCMODE                      =>      tied_to_ground_i,
      RXSYNCOUT                       =>      open,
      -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
      RXBYTEISALIGNED                 =>      RXBYTEISALIGNED_OUT,
      RXBYTEREALIGN                   =>      open,
      RXCOMMADET                      =>      open,
      RXCOMMADETEN                    =>      tied_to_vcc_i,
      RXMCOMMAALIGNEN                 =>      RXMCOMMAALIGNEN_IN,
      RXPCOMMAALIGNEN                 =>      RXPCOMMAALIGNEN_IN,
      RXSLIDE                         =>      tied_to_ground_i,
      ------------------ Receive Ports - RX Channel Bonding Ports ----------------
      RXCHANBONDSEQ                   =>      open,
      RXCHBONDEN                      =>      tied_to_ground_i,
      RXCHBONDI                       =>      "0000",
      RXCHBONDLEVEL                   =>      tied_to_ground_vec_i(2 downto 0),
      RXCHBONDMASTER                  =>      tied_to_ground_i,
      RXCHBONDO                       =>      open,
      RXCHBONDSLAVE                   =>      tied_to_ground_i,
      ----------------- Receive Ports - RX Channel Bonding Ports  ----------------
      RXCHANISALIGNED                 =>      open,
      RXCHANREALIGN                   =>      open,
      ------------ Receive Ports - RX Decision Feedback Equalizer(DFE) -----------
      DMONITOROUT                     =>      open,
      RXADAPTSELTEST                  =>      tied_to_ground_vec_i(13 downto 0),
      RXDFEXYDEN                      =>      tied_to_ground_i,
      RXOSINTEN                       =>      '1',
      RXOSINTID0                      =>      tied_to_ground_vec_i(3 downto 0),
      RXOSINTNTRLEN                   =>      tied_to_ground_i,
      RXOSINTSTROBEDONE               =>      open,
      ------- Receive Ports - RX Driver,OOB signalling,Coupling and Eq.,CDR ------
      RXLPMLFOVRDEN                   =>      tied_to_ground_i,
      RXLPMOSINTNTRLEN                =>      tied_to_ground_i,
      -------------------- Receive Ports - RX Equailizer Ports -------------------
      RXLPMHFHOLD                     =>      RXLPMHFHOLD_IN,
      RXLPMHFOVRDEN                   =>      tied_to_ground_i,
      RXLPMLFHOLD                     =>      RXLPMLFHOLD_IN,
      --------------------- Receive Ports - RX Equalizer Ports -------------------
      RXOSHOLD                        =>      tied_to_ground_i,
      RXOSOVRDEN                      =>      tied_to_ground_i,
      ------------ Receive Ports - RX Fabric ClocK Output Control Ports ----------
      RXRATEDONE                      =>      open,
      ----------- Receive Ports - RX Fabric Clock Output Control Ports  ----------
      RXRATEMODE                      =>      '0',
      --------------- Receive Ports - RX Fabric Output Control Ports -------------
      RXOUTCLK                        =>      RXOUTCLK_OUT,
      RXOUTCLKFABRIC                  =>      open,
      RXOUTCLKPCS                     =>      open,
      RXOUTCLKSEL                     =>      "010",
      ---------------------- Receive Ports - RX Gearbox Ports --------------------
      RXDATAVALID                     =>      open,
      RXHEADER                        =>      open,
      RXHEADERVALID                   =>      open,
      RXSTARTOFSEQ                    =>      open,
      --------------------- Receive Ports - RX Gearbox Ports  --------------------
      RXGEARBOXSLIP                   =>      tied_to_ground_i,
      ------------- Receive Ports - RX Initialization and Reset Ports ------------
      GTRXRESET                       =>      gtrxreset_out,
      RXLPMRESET                      =>      tied_to_ground_i,
      RXOOBRESET                      =>      tied_to_ground_i,
      RXPCSRESET                      =>      RXPCSRESET_IN,
      RXPMARESET                      =>      rxpmareset_out,
      ------------------- Receive Ports - RX OOB Signaling ports -----------------
      RXCOMSASDET                     =>      open,
      RXCOMWAKEDET                    =>      open,
      ------------------ Receive Ports - RX OOB Signaling ports  -----------------
      RXCOMINITDET                    =>      open,
      ------------------ Receive Ports - RX OOB signalling Ports -----------------
      RXELECIDLE                      =>      open,
      RXELECIDLEMODE                  =>      "11",
      ----------------- Receive Ports - RX Polarity Control Ports ----------------
      RXPOLARITY                      =>      tied_to_ground_i,
      -------------- Receive Ports -RX Initialization and Reset Ports ------------
      RXRESETDONE                     =>      RXRESETDONE_OUT,
      --------------------------- TX Buffer Bypass Ports -------------------------
      TXPHDLYTSTCLK                   =>      tied_to_ground_i,
      ------------------------ TX Configurable Driver Ports ----------------------
      TXPOSTCURSOR                    =>      "00000",
      TXPOSTCURSORINV                 =>      tied_to_ground_i,
      TXPRECURSOR                     =>      tied_to_ground_vec_i(4 downto 0),
      TXPRECURSORINV                  =>      tied_to_ground_i,
      -------------------- TX Fabric Clock Output Control Ports ------------------
      TXRATEMODE                      =>      tied_to_ground_i,
      --------------------- TX Initialization and Reset Ports --------------------
      CFGRESET                        =>      tied_to_ground_i,
      GTTXRESET                       =>      GTTXRESET_IN,
      PCSRSVDOUT                      =>      open,
      TXUSERRDY                       =>      TXUSERRDY_IN,
      ----------------- TX Phase Interpolator PPM Controller Ports ---------------
      TXPIPPMEN                       =>      tied_to_ground_i,
      TXPIPPMOVRDEN                   =>      tied_to_ground_i,
      TXPIPPMPD                       =>      tied_to_ground_i,
      TXPIPPMSEL                      =>      tied_to_ground_i,
      TXPIPPMSTEPSIZE                 =>      tied_to_ground_vec_i(4 downto 0),
      ---------------------- Transceiver Reset Mode Operation --------------------
      GTRESETSEL                      =>      tied_to_ground_i,
      RESETOVRD                       =>      tied_to_ground_i,
      ------------------------------- Transmit Ports -----------------------------
      TXPMARESETDONE                  =>      open,
      ----------------- Transmit Ports - Configurable Driver Ports ---------------
      PMARSVDIN0                      =>      '0',
      PMARSVDIN1                      =>      '0',
      ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
      TXDATA                          =>      txdata_i,
      TXUSRCLK                        =>      TXUSRCLK_IN,
      TXUSRCLK2                       =>      TXUSRCLK2_IN,
      --------------------- Transmit Ports - PCI Express Ports -------------------
      TXELECIDLE                      =>      tied_to_ground_i,
      TXMARGIN                        =>      tied_to_ground_vec_i(2 downto 0),
      TXRATE                          =>      tied_to_ground_vec_i(2 downto 0),
      TXSWING                         =>      tied_to_ground_i,
      ------------------ Transmit Ports - Pattern Generator Ports ----------------
      TXPRBSFORCEERR                  =>      TXPRBSFORCEERR_IN,
      ------------------ Transmit Ports - TX 8B/10B Encoder Ports ----------------
      TX8B10BBYPASS(3 downto 2)       =>      tied_to_ground_vec_i(1 downto 0),
      TX8B10BBYPASS(1 downto 0)       =>      TX8B10BBYPASS_IN,
      TXCHARDISPMODE(3 downto 2)      =>      tied_to_ground_vec_i(1 downto 0),
      TXCHARDISPMODE(1 downto 0)      =>      TXCHARDISPMODE_IN,
      TXCHARDISPVAL(3 downto 2)       =>      tied_to_ground_vec_i(1 downto 0),
      TXCHARDISPVAL(1 downto 0)       =>      TXCHARDISPVAL_IN,
      TXCHARISK(3 downto 2)           =>      tied_to_ground_vec_i(1 downto 0),
      TXCHARISK(1 downto 0)           =>      TXCHARISK_IN,
      ------------------ Transmit Ports - TX Buffer Bypass Ports -----------------
      TXDLYBYPASS                     =>      tied_to_vcc_i,
      TXDLYEN                         =>      tied_to_ground_i,
      TXDLYHOLD                       =>      tied_to_ground_i,
      TXDLYOVRDEN                     =>      tied_to_ground_i,
      TXDLYSRESET                     =>      tied_to_ground_i,
      TXDLYSRESETDONE                 =>      open,
      TXDLYUPDOWN                     =>      tied_to_ground_i,
      TXPHALIGN                       =>      tied_to_ground_i,
      TXPHALIGNDONE                   =>      open,
      TXPHALIGNEN                     =>      tied_to_ground_i,
      TXPHDLYPD                       =>      tied_to_ground_i,
      TXPHDLYRESET                    =>      tied_to_ground_i,
      TXPHINIT                        =>      tied_to_ground_i,
      TXPHINITDONE                    =>      open,
      TXPHOVRDEN                      =>      tied_to_ground_i,
      ---------------------- Transmit Ports - TX Buffer Ports --------------------
      TXBUFSTATUS                     =>      TXBUFSTATUS_OUT,
      ------------ Transmit Ports - TX Buffer and Phase Alignment Ports ----------
      TXSYNCALLIN                     =>      tied_to_ground_i,
      TXSYNCDONE                      =>      open,
      TXSYNCIN                        =>      tied_to_ground_i,
      TXSYNCMODE                      =>      tied_to_ground_i,
      TXSYNCOUT                       =>      open,
      --------------- Transmit Ports - TX Configurable Driver Ports --------------
      GTPTXN                          =>      GTPTXN_OUT,
      GTPTXP                          =>      GTPTXP_OUT,
      TXBUFDIFFCTRL                   =>      "100",
      TXDEEMPH                        =>      tied_to_ground_i,
      TXDIFFCTRL                      =>      "1000",
      TXDIFFPD                        =>      tied_to_ground_i,
      TXINHIBIT                       =>      tied_to_ground_i,
      TXMAINCURSOR                    =>      "0000000",
      TXPISOPD                        =>      tied_to_ground_i,
      ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
      TXOUTCLK                        =>      TXOUTCLK_OUT,
      TXOUTCLKFABRIC                  =>      TXOUTCLKFABRIC_OUT,
      TXOUTCLKPCS                     =>      TXOUTCLKPCS_OUT,
      TXOUTCLKSEL                     =>      "010",
      TXRATEDONE                      =>      open,
      --------------------- Transmit Ports - TX Gearbox Ports --------------------
      TXGEARBOXREADY                  =>      open,
      TXHEADER                        =>      tied_to_ground_vec_i(2 downto 0),
      TXSEQUENCE                      =>      tied_to_ground_vec_i(6 downto 0),
      TXSTARTSEQ                      =>      tied_to_ground_i,
      ------------- Transmit Ports - TX Initialization and Reset Ports -----------
      TXPCSRESET                      =>      TXPCSRESET_IN,
      TXPMARESET                      =>      TXPMARESET_IN,
      TXRESETDONE                     =>      TXRESETDONE_OUT,
      ------------------ Transmit Ports - TX OOB signalling Ports ----------------
      TXCOMFINISH                     =>      open,
      TXCOMINIT                       =>      tied_to_ground_i,
      TXCOMSAS                        =>      tied_to_ground_i,
      TXCOMWAKE                       =>      tied_to_ground_i,
      TXPDELECIDLEMODE                =>      tied_to_ground_i,
      ----------------- Transmit Ports - TX Polarity Control Ports ---------------
      TXPOLARITY                      =>      tied_to_ground_i,
      --------------- Transmit Ports - TX Receiver Detection Ports  --------------
      TXDETECTRX                      =>      tied_to_ground_i,
      ------------------ Transmit Ports - pattern Generator Ports ----------------
      TXPRBSSEL                       =>      TXPRBSSEL_IN

  );

   ------------------------- Soft Fix for Production Silicon----------------------
     gtrxreset_seq_i  : gtp_tile_a7_gtrxreset_seq
     port map
          (
      RST                             =>      RST_IN,
      GTRXRESET_IN                    =>      GTRXRESET_IN,
      RXPMARESETDONE                  =>      rxpmaresetdone_t,
      GTRXRESET_OUT                   =>      gtrxreset_out,
      DRP_OP_DONE                     =>      drp_op_done,
      DRPCLK                          =>      DRPCLK_IN,
      DRPEN                           =>      drpen_rst_t,
      DRPADDR                         =>      drpaddr_rst_t,
      DRPWE                           =>      drpwe_rst_t,
      DRPDO                           =>      drpdo_rst_t,
      DRPDI                           =>      drpdi_rst_t,
      DRPRDY                          =>      drprdy_rst_t

          );


    drpen_i       <= drpen_rst_t     when drp_op_done ='0'   else
                     drpen_pma_t     when drp_pma_busy = '1' else
                     drpen_rate_t    when drp_rate_busy ='1' else DRPEN_IN;


    drpaddr_i     <= drpaddr_rst_t   when drp_op_done ='0'   else
                     drpaddr_pma_t   when drp_pma_busy = '1' else
                     drpaddr_rate_t  when drp_rate_busy ='1' else DRPADDR_IN;


    drpwe_i       <= drpwe_rst_t     when drp_op_done ='0'   else
                     drpwe_pma_t     when drp_pma_busy = '1' else
                     drpwe_rate_t    when drp_rate_busy ='1' else DRPWE_IN;



    DRPDO_OUT      <=  drpdo_i when (drp_op_done='1' or drp_pma_busy='0' or drp_rate_busy='0') else x"0000";

    drpdo_rst_t    <=  drpdo_i;

    drpdo_pma_t    <=  drpdo_i;

    drpdo_rate_t   <=  drpdo_i;


    drpdi_i        <=  drpdi_rst_t     when drp_op_done ='0'   else
                       drpdi_pma_t     when drp_pma_busy = '1' else
                       drpdi_rate_t    when drp_rate_busy ='1' else DRPDI_IN;


    DRPRDY_OUT     <=  drprdy_i when (drp_op_done='1' or drp_pma_busy='0' or drp_rate_busy='0') else '0';

    drprdy_rst_t   <=  drprdy_i;

    drprdy_pma_t   <=  drprdy_i;

    drprdy_rate_t  <=  drprdy_i;


 drp_rate_busy <= '0';

--  ila_3 : work.ila_3
--  port map (
-- 	.clk(clk), -- input wire clk
-- 	.probe0(rxdata_i), -- input wire [0:0]  probe0
-- 	.probe1(RXCHARISK_OUT), -- input wire [1:0]  probe1
-- 	.probe2(rxcharisk_float_i) -- input wire [1:0]  probe2
-- );


process (DRPCLK_IN)
begin
   if(rising_edge(DRPCLK_IN)) then
      if(drp_op_done = '0' or drp_rate_busy='1') then
         drp_busy_i1 <= '1';
      else
         drp_busy_i1 <= '0';
      end if;
   end if;
end process;

process (DRPCLK_IN)
begin
   if(rising_edge(DRPCLK_IN)) then
      if(drp_op_done = '0' or drp_pma_busy='1') then
         drp_busy_i2 <= '1';
      else
         drp_busy_i2 <= '0';
      end if;
   end if;
end process;

  DRP_BUSY_OUT <= drp_busy_i1 or drp_busy_i2;

------------------------- Soft Fix for Production Silicon----------------------
     rxpmarst_seq_i :  gtp_tile_a7_rxpmarst_seq
     port map
          (
      RST                             =>      RST_IN,
      RXPMARESET_IN                   =>      RXPMARESET_IN,
      RXPMARESETDONE                  =>      rxpmaresetdone_t,
      RXPMARESET_OUT                  =>      rxpmareset_out,
      DRP_PMA_BUSY_OUT                =>      drp_pma_busy,
      DRP_BUSY_IN                     =>      drp_busy_i1,
      DRPCLK                          =>      DRPCLK_IN,
      DRPEN                           =>      drpen_pma_t,
      DRPADDR                         =>      drpaddr_pma_t,
      DRPWE                           =>      drpwe_pma_t,
      DRPDO                           =>      drpdo_pma_t,
      DRPDI                           =>      drpdi_pma_t,
      DRPRDY                          =>      drprdy_pma_t

          );





end RTL;





----------------------------------------------------
--
--  Library Name :  GTP
--  Unit    Name :  GTP_TILE_A7
--  Unit    Type :  Text Unit
--
------------------------------------------------------
-------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version : 2.7
--  \   \         Application : 7 Series FPGAs Transceivers Wizard
--  /   /         Filename : gtp_tile_a7.vhd
-- /___/   /\
-- \   \  /  \
--  \___\/\___\
--
--
-- Module gtp_tile_a7 (a GT Wrapper)
-- Generated by Xilinx 7 Series FPGAs Transceivers Wizard
--
--
-- (c) Copyright 2010-2012 Xilinx, Inc. All rights reserved.
--
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
--
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
--
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
--
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
library work;
use work.GTP_TILE_PKG.ALL;


--***************************** Entity Declaration ****************************

entity GTP_TILE_A7 is
generic
(
  -- LINE RATE SELECT
  G_RATE : GTP_RATE_TYPE := GBPS_3_0
);
port
(
  --_________________________________________________________________________
  --_________________________________________________________________________
  --GT0  (X0Y0)
  --____________________________CHANNEL PORTS________________________________
  GT0_DRP_BUSY_OUT                        : out  std_logic;
  ---------------------------- Channel - DRP Ports  --------------------------
  GT0_DRPADDR_IN                          : in   std_logic_vector(8 downto 0);
  GT0_DRPCLK_IN                           : in   std_logic;
  GT0_DRPDI_IN                            : in   std_logic_vector(15 downto 0);
  GT0_DRPDO_OUT                           : out  std_logic_vector(15 downto 0);
  GT0_DRPEN_IN                            : in   std_logic;
  GT0_DRPRDY_OUT                          : out  std_logic;
  GT0_DRPWE_IN                            : in   std_logic;
  --------------------- RX Initialization and Reset Ports --------------------
  GT0_RXUSERRDY_IN                        : in   std_logic;
  -------------------------- RX Margin Analysis Ports ------------------------
  GT0_EYESCANDATAERROR_OUT                : out  std_logic;
  ------------------------- Receive Ports - CDR Ports ------------------------
  GT0_RXCDRHOLD_IN                        : in   std_logic;
  GT0_RXCDRLOCK_OUT                       : out  std_logic;
  ------------------- Receive Ports - Clock Correction Ports -----------------
  GT0_RXCLKCORCNT_OUT                     : out  std_logic_vector(1 downto 0);
  ------------------ Receive Ports - FPGA RX Interface Ports -----------------
  GT0_RXDATA_OUT                          : out  std_logic_vector(15 downto 0);
  GT0_RXUSRCLK_IN                         : in   std_logic;
  GT0_RXUSRCLK2_IN                        : in   std_logic;
  ------------------- Receive Ports - Pattern Checker Ports ------------------
  GT0_RXPRBSERR_OUT                       : out  std_logic;
  GT0_RXPRBSSEL_IN                        : in   std_logic_vector(2 downto 0);
  ------------------- Receive Ports - Pattern Checker ports ------------------
  GT0_RXPRBSCNTRESET_IN                   : in   std_logic;
  ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
  GT0_RXCHARISCOMMA_OUT                   : out  std_logic_vector(1 downto 0);
  GT0_RXCHARISK_OUT                       : out  std_logic_vector(1 downto 0);
  GT0_RXDISPERR_OUT                       : out  std_logic_vector(1 downto 0);
  GT0_RXNOTINTABLE_OUT                    : out  std_logic_vector(1 downto 0);
  ------------------------ Receive Ports - RX AFE Ports ----------------------
  GT0_GTPRXN_IN                           : in   std_logic;
  GT0_GTPRXP_IN                           : in   std_logic;
  ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
  GT0_RXBUFRESET_IN                       : in   std_logic;
  GT0_RXBUFSTATUS_OUT                     : out  std_logic_vector(2 downto 0);
  -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
  GT0_RXBYTEISALIGNED_OUT                 : out  std_logic;
  GT0_RXMCOMMAALIGNEN_IN                  : in   std_logic;
  GT0_RXPCOMMAALIGNEN_IN                  : in   std_logic;
  -------------------- Receive Ports - RX Equailizer Ports -------------------
  GT0_RXLPMHFHOLD_IN                      : in   std_logic;
  GT0_RXLPMLFHOLD_IN                      : in   std_logic;
  --------------- Receive Ports - RX Fabric Output Control Ports -------------
  GT0_RXOUTCLK_OUT                        : out  std_logic;
  ------------- Receive Ports - RX Initialization and Reset Ports ------------
  GT0_GTRXRESET_IN                        : in   std_logic;
  GT0_RXPCSRESET_IN                       : in   std_logic;
  GT0_RXPMARESET_IN                       : in   std_logic;
  -------------- Receive Ports -RX Initialization and Reset Ports ------------
  GT0_RXRESETDONE_OUT                     : out  std_logic;
  --------------------- TX Initialization and Reset Ports --------------------
  GT0_GTTXRESET_IN                        : in   std_logic;
  GT0_TXUSERRDY_IN                        : in   std_logic;
  ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
  GT0_TXDATA_IN                           : in   std_logic_vector(15 downto 0);
  GT0_TXUSRCLK_IN                         : in   std_logic;
  GT0_TXUSRCLK2_IN                        : in   std_logic;
  ------------------ Transmit Ports - Pattern Generator Ports ----------------
  GT0_TXPRBSFORCEERR_IN                   : in   std_logic;
  ------------------ Transmit Ports - TX 8B/10B Encoder Ports ----------------
  GT0_TX8B10BBYPASS_IN                    : in   std_logic_vector(1 downto 0);
  GT0_TXCHARDISPMODE_IN                   : in   std_logic_vector(1 downto 0);
  GT0_TXCHARDISPVAL_IN                    : in   std_logic_vector(1 downto 0);
  GT0_TXCHARISK_IN                        : in   std_logic_vector(1 downto 0);
  ---------------------- Transmit Ports - TX Buffer Ports --------------------
  GT0_TXBUFSTATUS_OUT                     : out  std_logic_vector(1 downto 0);
  --------------- Transmit Ports - TX Configurable Driver Ports --------------
  GT0_GTPTXN_OUT                          : out  std_logic;
  GT0_GTPTXP_OUT                          : out  std_logic;
  ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
  GT0_TXOUTCLK_OUT                        : out  std_logic;
  GT0_TXOUTCLKFABRIC_OUT                  : out  std_logic;
  GT0_TXOUTCLKPCS_OUT                     : out  std_logic;
  ------------- Transmit Ports - TX Initialization and Reset Ports -----------
  GT0_TXPCSRESET_IN                       : in   std_logic;
  GT0_TXPMARESET_IN                       : in   std_logic;
  GT0_TXRESETDONE_OUT                     : out  std_logic;
  ------------------ Transmit Ports - pattern Generator Ports ----------------
  GT0_TXPRBSSEL_IN                        : in   std_logic_vector(2 downto 0);

  --GT1  (X0Y1)
  --____________________________CHANNEL PORTS________________________________
  GT1_DRP_BUSY_OUT                        : out  std_logic;
  ---------------------------- Channel - DRP Ports  --------------------------
  GT1_DRPADDR_IN                          : in   std_logic_vector(8 downto 0);
  GT1_DRPCLK_IN                           : in   std_logic;
  GT1_DRPDI_IN                            : in   std_logic_vector(15 downto 0);
  GT1_DRPDO_OUT                           : out  std_logic_vector(15 downto 0);
  GT1_DRPEN_IN                            : in   std_logic;
  GT1_DRPRDY_OUT                          : out  std_logic;
  GT1_DRPWE_IN                            : in   std_logic;
  --------------------- RX Initialization and Reset Ports --------------------
  GT1_RXUSERRDY_IN                        : in   std_logic;
  -------------------------- RX Margin Analysis Ports ------------------------
  GT1_EYESCANDATAERROR_OUT                : out  std_logic;
  ------------------------- Receive Ports - CDR Ports ------------------------
  GT1_RXCDRHOLD_IN                        : in   std_logic;
  GT1_RXCDRLOCK_OUT                       : out  std_logic;
  ------------------- Receive Ports - Clock Correction Ports -----------------
  GT1_RXCLKCORCNT_OUT                     : out  std_logic_vector(1 downto 0);
  ------------------ Receive Ports - FPGA RX Interface Ports -----------------
  GT1_RXDATA_OUT                          : out  std_logic_vector(15 downto 0);
  GT1_RXUSRCLK_IN                         : in   std_logic;
  GT1_RXUSRCLK2_IN                        : in   std_logic;
  ------------------- Receive Ports - Pattern Checker Ports ------------------
  GT1_RXPRBSERR_OUT                       : out  std_logic;
  GT1_RXPRBSSEL_IN                        : in   std_logic_vector(2 downto 0);
  ------------------- Receive Ports - Pattern Checker ports ------------------
  GT1_RXPRBSCNTRESET_IN                   : in   std_logic;
  ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
  GT1_RXCHARISCOMMA_OUT                   : out  std_logic_vector(1 downto 0);
  GT1_RXCHARISK_OUT                       : out  std_logic_vector(1 downto 0);
  GT1_RXDISPERR_OUT                       : out  std_logic_vector(1 downto 0);
  GT1_RXNOTINTABLE_OUT                    : out  std_logic_vector(1 downto 0);
  ------------------------ Receive Ports - RX AFE Ports ----------------------
  GT1_GTPRXN_IN                           : in   std_logic;
  GT1_GTPRXP_IN                           : in   std_logic;
  ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
  GT1_RXBUFRESET_IN                       : in   std_logic;
  GT1_RXBUFSTATUS_OUT                     : out  std_logic_vector(2 downto 0);
  -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
  GT1_RXBYTEISALIGNED_OUT                 : out  std_logic;
  GT1_RXMCOMMAALIGNEN_IN                  : in   std_logic;
  GT1_RXPCOMMAALIGNEN_IN                  : in   std_logic;
  -------------------- Receive Ports - RX Equailizer Ports -------------------
  GT1_RXLPMHFHOLD_IN                      : in   std_logic;
  GT1_RXLPMLFHOLD_IN                      : in   std_logic;
  --------------- Receive Ports - RX Fabric Output Control Ports -------------
  GT1_RXOUTCLK_OUT                        : out  std_logic;
  ------------- Receive Ports - RX Initialization and Reset Ports ------------
  GT1_GTRXRESET_IN                        : in   std_logic;
  GT1_RXPCSRESET_IN                       : in   std_logic;
  GT1_RXPMARESET_IN                       : in   std_logic;
  -------------- Receive Ports -RX Initialization and Reset Ports ------------
  GT1_RXRESETDONE_OUT                     : out  std_logic;
  --------------------- TX Initialization and Reset Ports --------------------
  GT1_GTTXRESET_IN                        : in   std_logic;
  GT1_TXUSERRDY_IN                        : in   std_logic;
  ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
  GT1_TXDATA_IN                           : in   std_logic_vector(15 downto 0);
  GT1_TXUSRCLK_IN                         : in   std_logic;
  GT1_TXUSRCLK2_IN                        : in   std_logic;
  ------------------ Transmit Ports - Pattern Generator Ports ----------------
  GT1_TXPRBSFORCEERR_IN                   : in   std_logic;
  ------------------ Transmit Ports - TX 8B/10B Encoder Ports ----------------
  GT1_TX8B10BBYPASS_IN                    : in   std_logic_vector(1 downto 0);
  GT1_TXCHARDISPMODE_IN                   : in   std_logic_vector(1 downto 0);
  GT1_TXCHARDISPVAL_IN                    : in   std_logic_vector(1 downto 0);
  GT1_TXCHARISK_IN                        : in   std_logic_vector(1 downto 0);
  ---------------------- Transmit Ports - TX Buffer Ports --------------------
  GT1_TXBUFSTATUS_OUT                     : out  std_logic_vector(1 downto 0);
  --------------- Transmit Ports - TX Configurable Driver Ports --------------
  GT1_GTPTXN_OUT                          : out  std_logic;
  GT1_GTPTXP_OUT                          : out  std_logic;
  ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
  GT1_TXOUTCLK_OUT                        : out  std_logic;
  GT1_TXOUTCLKFABRIC_OUT                  : out  std_logic;
  GT1_TXOUTCLKPCS_OUT                     : out  std_logic;
  ------------- Transmit Ports - TX Initialization and Reset Ports -----------
  GT1_TXPCSRESET_IN                       : in   std_logic;
  GT1_TXPMARESET_IN                       : in   std_logic;
  GT1_TXRESETDONE_OUT                     : out  std_logic;
  ------------------ Transmit Ports - pattern Generator Ports ----------------
  GT1_TXPRBSSEL_IN                        : in   std_logic_vector(2 downto 0);

  --GT2  (X0Y2)
  --____________________________CHANNEL PORTS________________________________
  GT2_DRP_BUSY_OUT                        : out  std_logic;
  ---------------------------- Channel - DRP Ports  --------------------------
  GT2_DRPADDR_IN                          : in   std_logic_vector(8 downto 0);
  GT2_DRPCLK_IN                           : in   std_logic;
  GT2_DRPDI_IN                            : in   std_logic_vector(15 downto 0);
  GT2_DRPDO_OUT                           : out  std_logic_vector(15 downto 0);
  GT2_DRPEN_IN                            : in   std_logic;
  GT2_DRPRDY_OUT                          : out  std_logic;
  GT2_DRPWE_IN                            : in   std_logic;
  --------------------- RX Initialization and Reset Ports --------------------
  GT2_RXUSERRDY_IN                        : in   std_logic;
  -------------------------- RX Margin Analysis Ports ------------------------
  GT2_EYESCANDATAERROR_OUT                : out  std_logic;
  ------------------------- Receive Ports - CDR Ports ------------------------
  GT2_RXCDRHOLD_IN                        : in   std_logic;
  GT2_RXCDRLOCK_OUT                       : out  std_logic;
  ------------------- Receive Ports - Clock Correction Ports -----------------
  GT2_RXCLKCORCNT_OUT                     : out  std_logic_vector(1 downto 0);
  ------------------ Receive Ports - FPGA RX Interface Ports -----------------
  GT2_RXDATA_OUT                          : out  std_logic_vector(15 downto 0);
  GT2_RXUSRCLK_IN                         : in   std_logic;
  GT2_RXUSRCLK2_IN                        : in   std_logic;
  ------------------- Receive Ports - Pattern Checker Ports ------------------
  GT2_RXPRBSERR_OUT                       : out  std_logic;
  GT2_RXPRBSSEL_IN                        : in   std_logic_vector(2 downto 0);
  ------------------- Receive Ports - Pattern Checker ports ------------------
  GT2_RXPRBSCNTRESET_IN                   : in   std_logic;
  ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
  GT2_RXCHARISCOMMA_OUT                   : out  std_logic_vector(1 downto 0);
  GT2_RXCHARISK_OUT                       : out  std_logic_vector(1 downto 0);
  GT2_RXDISPERR_OUT                       : out  std_logic_vector(1 downto 0);
  GT2_RXNOTINTABLE_OUT                    : out  std_logic_vector(1 downto 0);
  ------------------------ Receive Ports - RX AFE Ports ----------------------
  GT2_GTPRXN_IN                           : in   std_logic;
  GT2_GTPRXP_IN                           : in   std_logic;
  ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
  GT2_RXBUFRESET_IN                       : in   std_logic;
  GT2_RXBUFSTATUS_OUT                     : out  std_logic_vector(2 downto 0);
  -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
  GT2_RXBYTEISALIGNED_OUT                 : out  std_logic;
  GT2_RXMCOMMAALIGNEN_IN                  : in   std_logic;
  GT2_RXPCOMMAALIGNEN_IN                  : in   std_logic;
  -------------------- Receive Ports - RX Equailizer Ports -------------------
  GT2_RXLPMHFHOLD_IN                      : in   std_logic;
  GT2_RXLPMLFHOLD_IN                      : in   std_logic;
  --------------- Receive Ports - RX Fabric Output Control Ports -------------
  GT2_RXOUTCLK_OUT                        : out  std_logic;
  ------------- Receive Ports - RX Initialization and Reset Ports ------------
  GT2_GTRXRESET_IN                        : in   std_logic;
  GT2_RXPCSRESET_IN                       : in   std_logic;
  GT2_RXPMARESET_IN                       : in   std_logic;
  -------------- Receive Ports -RX Initialization and Reset Ports ------------
  GT2_RXRESETDONE_OUT                     : out  std_logic;
  --------------------- TX Initialization and Reset Ports --------------------
  GT2_GTTXRESET_IN                        : in   std_logic;
  GT2_TXUSERRDY_IN                        : in   std_logic;
  ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
  GT2_TXDATA_IN                           : in   std_logic_vector(15 downto 0);
  GT2_TXUSRCLK_IN                         : in   std_logic;
  GT2_TXUSRCLK2_IN                        : in   std_logic;
  ------------------ Transmit Ports - Pattern Generator Ports ----------------
  GT2_TXPRBSFORCEERR_IN                   : in   std_logic;
  ------------------ Transmit Ports - TX 8B/10B Encoder Ports ----------------
  GT2_TX8B10BBYPASS_IN                    : in   std_logic_vector(1 downto 0);
  GT2_TXCHARDISPMODE_IN                   : in   std_logic_vector(1 downto 0);
  GT2_TXCHARDISPVAL_IN                    : in   std_logic_vector(1 downto 0);
  GT2_TXCHARISK_IN                        : in   std_logic_vector(1 downto 0);
  ---------------------- Transmit Ports - TX Buffer Ports --------------------
  GT2_TXBUFSTATUS_OUT                     : out  std_logic_vector(1 downto 0);
  --------------- Transmit Ports - TX Configurable Driver Ports --------------
  GT2_GTPTXN_OUT                          : out  std_logic;
  GT2_GTPTXP_OUT                          : out  std_logic;
  ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
  GT2_TXOUTCLK_OUT                        : out  std_logic;
  GT2_TXOUTCLKFABRIC_OUT                  : out  std_logic;
  GT2_TXOUTCLKPCS_OUT                     : out  std_logic;
  ------------- Transmit Ports - TX Initialization and Reset Ports -----------
  GT2_TXPCSRESET_IN                       : in   std_logic;
  GT2_TXPMARESET_IN                       : in   std_logic;
  GT2_TXRESETDONE_OUT                     : out  std_logic;
  ------------------ Transmit Ports - pattern Generator Ports ----------------
  GT2_TXPRBSSEL_IN                        : in   std_logic_vector(2 downto 0);

  --GT3  (X0Y3)
  --____________________________CHANNEL PORTS________________________________
  GT3_DRP_BUSY_OUT                        : out  std_logic;
  ---------------------------- Channel - DRP Ports  --------------------------
  GT3_DRPADDR_IN                          : in   std_logic_vector(8 downto 0);
  GT3_DRPCLK_IN                           : in   std_logic;
  GT3_DRPDI_IN                            : in   std_logic_vector(15 downto 0);
  GT3_DRPDO_OUT                           : out  std_logic_vector(15 downto 0);
  GT3_DRPEN_IN                            : in   std_logic;
  GT3_DRPRDY_OUT                          : out  std_logic;
  GT3_DRPWE_IN                            : in   std_logic;
  --------------------- RX Initialization and Reset Ports --------------------
  GT3_RXUSERRDY_IN                        : in   std_logic;
  -------------------------- RX Margin Analysis Ports ------------------------
  GT3_EYESCANDATAERROR_OUT                : out  std_logic;
  ------------------------- Receive Ports - CDR Ports ------------------------
  GT3_RXCDRHOLD_IN                        : in   std_logic;
  GT3_RXCDRLOCK_OUT                       : out  std_logic;
  ------------------- Receive Ports - Clock Correction Ports -----------------
  GT3_RXCLKCORCNT_OUT                     : out  std_logic_vector(1 downto 0);
  ------------------ Receive Ports - FPGA RX Interface Ports -----------------
  GT3_RXDATA_OUT                          : out  std_logic_vector(15 downto 0);
  GT3_RXUSRCLK_IN                         : in   std_logic;
  GT3_RXUSRCLK2_IN                        : in   std_logic;
  ------------------- Receive Ports - Pattern Checker Ports ------------------
  GT3_RXPRBSERR_OUT                       : out  std_logic;
  GT3_RXPRBSSEL_IN                        : in   std_logic_vector(2 downto 0);
  ------------------- Receive Ports - Pattern Checker ports ------------------
  GT3_RXPRBSCNTRESET_IN                   : in   std_logic;
  ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
  GT3_RXCHARISCOMMA_OUT                   : out  std_logic_vector(1 downto 0);
  GT3_RXCHARISK_OUT                       : out  std_logic_vector(1 downto 0);
  GT3_RXDISPERR_OUT                       : out  std_logic_vector(1 downto 0);
  GT3_RXNOTINTABLE_OUT                    : out  std_logic_vector(1 downto 0);
  ------------------------ Receive Ports - RX AFE Ports ----------------------
  GT3_GTPRXN_IN                           : in   std_logic;
  GT3_GTPRXP_IN                           : in   std_logic;
  ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
  GT3_RXBUFRESET_IN                       : in   std_logic;
  GT3_RXBUFSTATUS_OUT                     : out  std_logic_vector(2 downto 0);
  -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
  GT3_RXBYTEISALIGNED_OUT                 : out  std_logic;
  GT3_RXMCOMMAALIGNEN_IN                  : in   std_logic;
  GT3_RXPCOMMAALIGNEN_IN                  : in   std_logic;
  -------------------- Receive Ports - RX Equailizer Ports -------------------
  GT3_RXLPMHFHOLD_IN                      : in   std_logic;
  GT3_RXLPMLFHOLD_IN                      : in   std_logic;
  --------------- Receive Ports - RX Fabric Output Control Ports -------------
  GT3_RXOUTCLK_OUT                        : out  std_logic;
  ------------- Receive Ports - RX Initialization and Reset Ports ------------
  GT3_GTRXRESET_IN                        : in   std_logic;
  GT3_RXPCSRESET_IN                       : in   std_logic;
  GT3_RXPMARESET_IN                       : in   std_logic;
  -------------- Receive Ports -RX Initialization and Reset Ports ------------
  GT3_RXRESETDONE_OUT                     : out  std_logic;
  --------------------- TX Initialization and Reset Ports --------------------
  GT3_GTTXRESET_IN                        : in   std_logic;
  GT3_TXUSERRDY_IN                        : in   std_logic;
  ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
  GT3_TXDATA_IN                           : in   std_logic_vector(15 downto 0);
  GT3_TXUSRCLK_IN                         : in   std_logic;
  GT3_TXUSRCLK2_IN                        : in   std_logic;
  ------------------ Transmit Ports - Pattern Generator Ports ----------------
  GT3_TXPRBSFORCEERR_IN                   : in   std_logic;
  ------------------ Transmit Ports - TX 8B/10B Encoder Ports ----------------
  GT3_TX8B10BBYPASS_IN                    : in   std_logic_vector(1 downto 0);
  GT3_TXCHARDISPMODE_IN                   : in   std_logic_vector(1 downto 0);
  GT3_TXCHARDISPVAL_IN                    : in   std_logic_vector(1 downto 0);
  GT3_TXCHARISK_IN                        : in   std_logic_vector(1 downto 0);
  ---------------------- Transmit Ports - TX Buffer Ports --------------------
  GT3_TXBUFSTATUS_OUT                     : out  std_logic_vector(1 downto 0);
  --------------- Transmit Ports - TX Configurable Driver Ports --------------
  GT3_GTPTXN_OUT                          : out  std_logic;
  GT3_GTPTXP_OUT                          : out  std_logic;
  ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
  GT3_TXOUTCLK_OUT                        : out  std_logic;
  GT3_TXOUTCLKFABRIC_OUT                  : out  std_logic;
  GT3_TXOUTCLKPCS_OUT                     : out  std_logic;
  ------------- Transmit Ports - TX Initialization and Reset Ports -----------
  GT3_TXPCSRESET_IN                       : in   std_logic;
  GT3_TXPMARESET_IN                       : in   std_logic;
  GT3_TXRESETDONE_OUT                     : out  std_logic;
  ------------------ Transmit Ports - pattern Generator Ports ----------------
  GT3_TXPRBSSEL_IN                        : in   std_logic_vector(2 downto 0);


  --____________________________COMMON PORTS________________________________
  ----------------- Common Block - GTPE2_COMMON Clocking Ports ---------------
  GT0_GTREFCLK0_IN                        : in   std_logic;
  -------------------------- Common Block - PLL Ports ------------------------
  GT0_PLL0LOCK_OUT                        : out  std_logic;
  GT0_PLL0LOCKDETCLK_IN                   : in   std_logic;
  GT0_PLL0REFCLKLOST_OUT                  : out  std_logic;
  GT0_PLL0RESET_IN                        : in   std_logic


);


end GTP_TILE_A7;



architecture RTL of gtp_tile_a7 is

  attribute CORE_GENERATION_INFO : string;
  attribute CORE_GENERATION_INFO of RTL : architecture is "gtp_tile_a7,gtwizard_v2_7,{protocol_file=Start_from_scratch}";


--***********************************Parameter Declarations********************

  constant DLY : time := 1 ns;

--***************************** Signal Declarations *****************************

  -- ground and tied_to_vcc_i signals
  signal  tied_to_ground_i                :   std_logic;
  signal  tied_to_ground_vec_i            :   std_logic_vector(63 downto 0);
  signal  tied_to_vcc_i                   :   std_logic;
  signal   gt0_pll0outclk_i       :   std_logic;
  signal   gt0_pll0outrefclk_i    :   std_logic;
  signal   gt0_pll1outclk_i       :   std_logic;
  signal   gt0_pll1outrefclk_i    :   std_logic;


  signal  gt0_mgtrefclktx_i           :   std_logic_vector(1 downto 0);
  signal  gt0_mgtrefclkrx_i           :   std_logic_vector(1 downto 0);

  signal  gt1_mgtrefclktx_i           :   std_logic_vector(1 downto 0);
  signal  gt1_mgtrefclkrx_i           :   std_logic_vector(1 downto 0);

  signal  gt2_mgtrefclktx_i           :   std_logic_vector(1 downto 0);
  signal  gt2_mgtrefclkrx_i           :   std_logic_vector(1 downto 0);

  signal  gt3_mgtrefclktx_i           :   std_logic_vector(1 downto 0);
  signal  gt3_mgtrefclkrx_i           :   std_logic_vector(1 downto 0);


  signal   gt0_pll0clk_i       :   std_logic;
  signal   gt0_pll0refclk_i    :   std_logic;
  signal   gt0_pll1clk_i       :   std_logic;
  signal   gt0_pll1refclk_i    :   std_logic;
  signal    gt0_rst_i                       : std_logic;


  signal   gt1_pll0clk_i       :   std_logic;
  signal   gt1_pll0refclk_i    :   std_logic;
  signal   gt1_pll1clk_i       :   std_logic;
  signal   gt1_pll1refclk_i    :   std_logic;
  signal    gt1_rst_i                       : std_logic;


  signal   gt2_pll0clk_i       :   std_logic;
  signal   gt2_pll0refclk_i    :   std_logic;
  signal   gt2_pll1clk_i       :   std_logic;
  signal   gt2_pll1refclk_i    :   std_logic;
  signal    gt2_rst_i                       : std_logic;


  signal   gt3_pll0clk_i       :   std_logic;
  signal   gt3_pll0refclk_i    :   std_logic;
  signal   gt3_pll1clk_i       :   std_logic;
  signal   gt3_pll1refclk_i    :   std_logic;
  signal    gt3_rst_i                       : std_logic;




--*************************** Component Declarations **************************
component gtp_tile_a7_GT
generic
(
  -- Simulation attributes
  GT_SIM_GTRESET_SPEEDUP    : string  := "FALSE";
  TXSYNC_OVRD_IN            : bit     := '0';
  TXSYNC_MULTILANE_IN       : bit     := '0';
  -- LINE RATE SELECT
  G_RATE                    : GTP_RATE_TYPE := GBPS_3_0
);
port
(
  RST_IN                                  : in   std_logic;
  DRP_BUSY_OUT                            : out  std_logic;

  ---------------------------- Channel - DRP Ports  --------------------------
  DRPADDR_IN                              : in   std_logic_vector(8 downto 0);
  DRPCLK_IN                               : in   std_logic;
  DRPDI_IN                                : in   std_logic_vector(15 downto 0);
  DRPDO_OUT                               : out  std_logic_vector(15 downto 0);
  DRPEN_IN                                : in   std_logic;
  DRPRDY_OUT                              : out  std_logic;
  DRPWE_IN                                : in   std_logic;
  ------------------------ GTPE2_CHANNEL Clocking Ports ----------------------
  PLL0CLK_IN                              : in   std_logic;
  PLL0REFCLK_IN                           : in   std_logic;
  PLL1CLK_IN                              : in   std_logic;
  PLL1REFCLK_IN                           : in   std_logic;
  --------------------- RX Initialization and Reset Ports --------------------
  RXUSERRDY_IN                            : in   std_logic;
  -------------------------- RX Margin Analysis Ports ------------------------
  EYESCANDATAERROR_OUT                    : out  std_logic;
  ------------------------- Receive Ports - CDR Ports ------------------------
  RXCDRHOLD_IN                            : in   std_logic;
  RXCDRLOCK_OUT                           : out  std_logic;
  ------------------- Receive Ports - Clock Correction Ports -----------------
  RXCLKCORCNT_OUT                         : out  std_logic_vector(1 downto 0);
  ------------------ Receive Ports - FPGA RX Interface Ports -----------------
  RXDATA_OUT                              : out  std_logic_vector(15 downto 0);
  RXUSRCLK_IN                             : in   std_logic;
  RXUSRCLK2_IN                            : in   std_logic;
  ------------------- Receive Ports - Pattern Checker Ports ------------------
  RXPRBSERR_OUT                           : out  std_logic;
  RXPRBSSEL_IN                            : in   std_logic_vector(2 downto 0);
  ------------------- Receive Ports - Pattern Checker ports ------------------
  RXPRBSCNTRESET_IN                       : in   std_logic;
  ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
  RXCHARISCOMMA_OUT                       : out  std_logic_vector(1 downto 0);
  RXCHARISK_OUT                           : out  std_logic_vector(1 downto 0);
  RXDISPERR_OUT                           : out  std_logic_vector(1 downto 0);
  RXNOTINTABLE_OUT                        : out  std_logic_vector(1 downto 0);
  ------------------------ Receive Ports - RX AFE Ports ----------------------
  GTPRXN_IN                               : in   std_logic;
  GTPRXP_IN                               : in   std_logic;
  ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
  RXBUFRESET_IN                           : in   std_logic;
  RXBUFSTATUS_OUT                         : out  std_logic_vector(2 downto 0);
  -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
  RXBYTEISALIGNED_OUT                     : out  std_logic;
  RXMCOMMAALIGNEN_IN                      : in   std_logic;
  RXPCOMMAALIGNEN_IN                      : in   std_logic;
  -------------------- Receive Ports - RX Equailizer Ports -------------------
  RXLPMHFHOLD_IN                          : in   std_logic;
  RXLPMLFHOLD_IN                          : in   std_logic;
  --------------- Receive Ports - RX Fabric Output Control Ports -------------
  RXOUTCLK_OUT                            : out  std_logic;
  ------------- Receive Ports - RX Initialization and Reset Ports ------------
  GTRXRESET_IN                            : in   std_logic;
  RXPCSRESET_IN                           : in   std_logic;
  RXPMARESET_IN                           : in   std_logic;
  -------------- Receive Ports -RX Initialization and Reset Ports ------------
  RXRESETDONE_OUT                         : out  std_logic;
  --------------------- TX Initialization and Reset Ports --------------------
  GTTXRESET_IN                            : in   std_logic;
  TXUSERRDY_IN                            : in   std_logic;
  ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
  TXDATA_IN                               : in   std_logic_vector(15 downto 0);
  TXUSRCLK_IN                             : in   std_logic;
  TXUSRCLK2_IN                            : in   std_logic;
  ------------------ Transmit Ports - Pattern Generator Ports ----------------
  TXPRBSFORCEERR_IN                       : in   std_logic;
  ------------------ Transmit Ports - TX 8B/10B Encoder Ports ----------------
  TX8B10BBYPASS_IN                        : in   std_logic_vector(1 downto 0);
  TXCHARDISPMODE_IN                       : in   std_logic_vector(1 downto 0);
  TXCHARDISPVAL_IN                        : in   std_logic_vector(1 downto 0);
  TXCHARISK_IN                            : in   std_logic_vector(1 downto 0);
  ---------------------- Transmit Ports - TX Buffer Ports --------------------
  TXBUFSTATUS_OUT                         : out  std_logic_vector(1 downto 0);
  --------------- Transmit Ports - TX Configurable Driver Ports --------------
  GTPTXN_OUT                              : out  std_logic;
  GTPTXP_OUT                              : out  std_logic;
  ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
  TXOUTCLK_OUT                            : out  std_logic;
  TXOUTCLKFABRIC_OUT                      : out  std_logic;
  TXOUTCLKPCS_OUT                         : out  std_logic;
  ------------- Transmit Ports - TX Initialization and Reset Ports -----------
  TXPCSRESET_IN                           : in   std_logic;
  TXPMARESET_IN                           : in   std_logic;
  TXRESETDONE_OUT                         : out  std_logic;
  ------------------ Transmit Ports - pattern Generator Ports ----------------
  TXPRBSSEL_IN                            : in   std_logic_vector(2 downto 0)


);
end component;


  constant PLL0_FBDIV_IN      :   integer := F_A7_PLL_FBDIV_IN(G_RATE);
  constant PLL1_FBDIV_IN      :   integer := F_A7_PLL_FBDIV_IN(G_RATE);
  constant PLL0_FBDIV_45_IN   :   integer := 5;
  constant PLL1_FBDIV_45_IN   :   integer := 5;
  constant PLL0_REFCLK_DIV_IN :   integer := 1;
  constant PLL1_REFCLK_DIV_IN :   integer := 1;

--********************************* Main Body of Code**************************

begin

  tied_to_ground_i                    <= '0';
  tied_to_ground_vec_i(63 downto 0)   <= (others => '0');
  tied_to_vcc_i                       <= '1';
  gt0_pll0clk_i    <= gt0_pll0outclk_i;
  gt0_pll0refclk_i <= gt0_pll0outrefclk_i;
  gt0_pll1clk_i    <= gt0_pll1outclk_i;
  gt0_pll1refclk_i <= gt0_pll1outrefclk_i;
  gt0_rst_i        <= GT0_PLL0RESET_IN;



  gt1_pll0clk_i    <= gt0_pll0outclk_i;
  gt1_pll0refclk_i <= gt0_pll0outrefclk_i;
  gt1_pll1clk_i    <= gt0_pll1outclk_i;
  gt1_pll1refclk_i <= gt0_pll1outrefclk_i;
  gt1_rst_i        <= GT0_PLL0RESET_IN;



  gt2_pll0clk_i    <= gt0_pll0outclk_i;
  gt2_pll0refclk_i <= gt0_pll0outrefclk_i;
  gt2_pll1clk_i    <= gt0_pll1outclk_i;
  gt2_pll1refclk_i <= gt0_pll1outrefclk_i;
  gt2_rst_i        <= GT0_PLL0RESET_IN;



  gt3_pll0clk_i    <= gt0_pll0outclk_i;
  gt3_pll0refclk_i <= gt0_pll0outrefclk_i;
  gt3_pll1clk_i    <= gt0_pll1outclk_i;
  gt3_pll1refclk_i <= gt0_pll1outrefclk_i;
  gt3_rst_i        <= GT0_PLL0RESET_IN;



  --------------------------- GT Instances  -------------------------------
  --_________________________________________________________________________
  --_________________________________________________________________________
  --GT0  (X0Y0)
  gt0_gtp_tile_a7_i : gtp_tile_a7_GT
  generic map
  (
      -- Simulation attributes
      GT_SIM_GTRESET_SPEEDUP => "TRUE",
      TXSYNC_OVRD_IN         => ('0'),
      TXSYNC_MULTILANE_IN    => ('0'),
      -- LINE RATE SELECT
      G_RATE                 => G_RATE
  )
  port map
  (
      RST_IN                          =>      gt0_rst_i,
      DRP_BUSY_OUT                    =>      GT0_DRP_BUSY_OUT,

      ---------------------------- Channel - DRP Ports  --------------------------
      DRPADDR_IN                      =>      GT0_DRPADDR_IN,
      DRPCLK_IN                       =>      GT0_DRPCLK_IN,
      DRPDI_IN                        =>      GT0_DRPDI_IN,
      DRPDO_OUT                       =>      GT0_DRPDO_OUT,
      DRPEN_IN                        =>      GT0_DRPEN_IN,
      DRPRDY_OUT                      =>      GT0_DRPRDY_OUT,
      DRPWE_IN                        =>      GT0_DRPWE_IN,
      ------------------------ GTPE2_CHANNEL Clocking Ports ----------------------
      PLL0CLK_IN                      =>      gt0_pll0clk_i,
      PLL0REFCLK_IN                   =>      gt0_pll0refclk_i,
      PLL1CLK_IN                      =>      gt0_pll1clk_i,
      PLL1REFCLK_IN                   =>      gt0_pll1refclk_i,
      --------------------- RX Initialization and Reset Ports --------------------
      RXUSERRDY_IN                    =>      GT0_RXUSERRDY_IN,
      -------------------------- RX Margin Analysis Ports ------------------------
      EYESCANDATAERROR_OUT            =>      GT0_EYESCANDATAERROR_OUT,
      ------------------------- Receive Ports - CDR Ports ------------------------
      RXCDRHOLD_IN                    =>      GT0_RXCDRHOLD_IN,
      RXCDRLOCK_OUT                   =>      GT0_RXCDRLOCK_OUT,
      ------------------- Receive Ports - Clock Correction Ports -----------------
      RXCLKCORCNT_OUT                 =>      GT0_RXCLKCORCNT_OUT,
      ------------------ Receive Ports - FPGA RX Interface Ports -----------------
      RXDATA_OUT                      =>      GT0_RXDATA_OUT,
      RXUSRCLK_IN                     =>      GT0_RXUSRCLK_IN,
      RXUSRCLK2_IN                    =>      GT0_RXUSRCLK2_IN,
      ------------------- Receive Ports - Pattern Checker Ports ------------------
      RXPRBSERR_OUT                   =>      GT0_RXPRBSERR_OUT,
      RXPRBSSEL_IN                    =>      GT0_RXPRBSSEL_IN,
      ------------------- Receive Ports - Pattern Checker ports ------------------
      RXPRBSCNTRESET_IN               =>      GT0_RXPRBSCNTRESET_IN,
      ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
      RXCHARISCOMMA_OUT               =>      GT0_RXCHARISCOMMA_OUT,
      RXCHARISK_OUT                   =>      GT0_RXCHARISK_OUT,
      RXDISPERR_OUT                   =>      GT0_RXDISPERR_OUT,
      RXNOTINTABLE_OUT                =>      GT0_RXNOTINTABLE_OUT,
      ------------------------ Receive Ports - RX AFE Ports ----------------------
      GTPRXN_IN                       =>      GT0_GTPRXN_IN,
      GTPRXP_IN                       =>      GT0_GTPRXP_IN,
      ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
      RXBUFRESET_IN                   =>      GT0_RXBUFRESET_IN,
      RXBUFSTATUS_OUT                 =>      GT0_RXBUFSTATUS_OUT,
      -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
      RXBYTEISALIGNED_OUT             =>      GT0_RXBYTEISALIGNED_OUT,
      RXMCOMMAALIGNEN_IN              =>      GT0_RXMCOMMAALIGNEN_IN,
      RXPCOMMAALIGNEN_IN              =>      GT0_RXPCOMMAALIGNEN_IN,
      -------------------- Receive Ports - RX Equailizer Ports -------------------
      RXLPMHFHOLD_IN                  =>      GT0_RXLPMHFHOLD_IN,
      RXLPMLFHOLD_IN                  =>      GT0_RXLPMLFHOLD_IN,
      --------------- Receive Ports - RX Fabric Output Control Ports -------------
      RXOUTCLK_OUT                    =>      GT0_RXOUTCLK_OUT,
      ------------- Receive Ports - RX Initialization and Reset Ports ------------
      GTRXRESET_IN                    =>      GT0_GTRXRESET_IN,
      RXPCSRESET_IN                   =>      GT0_RXPCSRESET_IN,
      RXPMARESET_IN                   =>      GT0_RXPMARESET_IN,
      -------------- Receive Ports -RX Initialization and Reset Ports ------------
      RXRESETDONE_OUT                 =>      GT0_RXRESETDONE_OUT,
      --------------------- TX Initialization and Reset Ports --------------------
      GTTXRESET_IN                    =>      GT0_GTTXRESET_IN,
      TXUSERRDY_IN                    =>      GT0_TXUSERRDY_IN,
      ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
      TXDATA_IN                       =>      GT0_TXDATA_IN,
      TXUSRCLK_IN                     =>      GT0_TXUSRCLK_IN,
      TXUSRCLK2_IN                    =>      GT0_TXUSRCLK2_IN,
      ------------------ Transmit Ports - Pattern Generator Ports ----------------
      TXPRBSFORCEERR_IN               =>      GT0_TXPRBSFORCEERR_IN,
      ------------------ Transmit Ports - TX 8B/10B Encoder Ports ----------------
      TX8B10BBYPASS_IN                =>      GT0_TX8B10BBYPASS_IN,
      TXCHARDISPMODE_IN               =>      GT0_TXCHARDISPMODE_IN,
      TXCHARDISPVAL_IN                =>      GT0_TXCHARDISPVAL_IN,
      TXCHARISK_IN                    =>      GT0_TXCHARISK_IN,
      ---------------------- Transmit Ports - TX Buffer Ports --------------------
      TXBUFSTATUS_OUT                 =>      GT0_TXBUFSTATUS_OUT,
      --------------- Transmit Ports - TX Configurable Driver Ports --------------
      GTPTXN_OUT                      =>      GT0_GTPTXN_OUT,
      GTPTXP_OUT                      =>      GT0_GTPTXP_OUT,
      ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
      TXOUTCLK_OUT                    =>      GT0_TXOUTCLK_OUT,
      TXOUTCLKFABRIC_OUT              =>      GT0_TXOUTCLKFABRIC_OUT,
      TXOUTCLKPCS_OUT                 =>      GT0_TXOUTCLKPCS_OUT,
      ------------- Transmit Ports - TX Initialization and Reset Ports -----------
      TXPCSRESET_IN                   =>      GT0_TXPCSRESET_IN,
      TXPMARESET_IN                   =>      GT0_TXPMARESET_IN,
      TXRESETDONE_OUT                 =>      GT0_TXRESETDONE_OUT,
      ------------------ Transmit Ports - pattern Generator Ports ----------------
      TXPRBSSEL_IN                    =>      GT0_TXPRBSSEL_IN

  );

  --_________________________________________________________________________
  --_________________________________________________________________________
  --GT1  (X0Y1)
  gt1_gtp_tile_a7_i : gtp_tile_a7_GT
  generic map
  (
      -- Simulation attributes
      GT_SIM_GTRESET_SPEEDUP => "FALSE",
      TXSYNC_OVRD_IN         => ('0'),
      TXSYNC_MULTILANE_IN    => ('0'),
      -- LINE RATE SELECT
      G_RATE                 => G_RATE
  )
  port map
  (
      RST_IN                          =>      gt1_rst_i,
      DRP_BUSY_OUT                    =>      GT1_DRP_BUSY_OUT,

      ---------------------------- Channel - DRP Ports  --------------------------
      DRPADDR_IN                      =>      GT1_DRPADDR_IN,
      DRPCLK_IN                       =>      GT1_DRPCLK_IN,
      DRPDI_IN                        =>      GT1_DRPDI_IN,
      DRPDO_OUT                       =>      GT1_DRPDO_OUT,
      DRPEN_IN                        =>      GT1_DRPEN_IN,
      DRPRDY_OUT                      =>      GT1_DRPRDY_OUT,
      DRPWE_IN                        =>      GT1_DRPWE_IN,
      ------------------------ GTPE2_CHANNEL Clocking Ports ----------------------
      PLL0CLK_IN                      =>      gt1_pll0clk_i,
      PLL0REFCLK_IN                   =>      gt1_pll0refclk_i,
      PLL1CLK_IN                      =>      gt1_pll1clk_i,
      PLL1REFCLK_IN                   =>      gt1_pll1refclk_i,
      --------------------- RX Initialization and Reset Ports --------------------
      RXUSERRDY_IN                    =>      GT1_RXUSERRDY_IN,
      -------------------------- RX Margin Analysis Ports ------------------------
      EYESCANDATAERROR_OUT            =>      GT1_EYESCANDATAERROR_OUT,
      ------------------------- Receive Ports - CDR Ports ------------------------
      RXCDRHOLD_IN                    =>      GT1_RXCDRHOLD_IN,
      RXCDRLOCK_OUT                   =>      GT1_RXCDRLOCK_OUT,
      ------------------- Receive Ports - Clock Correction Ports -----------------
      RXCLKCORCNT_OUT                 =>      GT1_RXCLKCORCNT_OUT,
      ------------------ Receive Ports - FPGA RX Interface Ports -----------------
      RXDATA_OUT                      =>      GT1_RXDATA_OUT,
      RXUSRCLK_IN                     =>      GT1_RXUSRCLK_IN,
      RXUSRCLK2_IN                    =>      GT1_RXUSRCLK2_IN,
      ------------------- Receive Ports - Pattern Checker Ports ------------------
      RXPRBSERR_OUT                   =>      GT1_RXPRBSERR_OUT,
      RXPRBSSEL_IN                    =>      GT1_RXPRBSSEL_IN,
      ------------------- Receive Ports - Pattern Checker ports ------------------
      RXPRBSCNTRESET_IN               =>      GT1_RXPRBSCNTRESET_IN,
      ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
      RXCHARISCOMMA_OUT               =>      GT1_RXCHARISCOMMA_OUT,
      RXCHARISK_OUT                   =>      GT1_RXCHARISK_OUT,
      RXDISPERR_OUT                   =>      GT1_RXDISPERR_OUT,
      RXNOTINTABLE_OUT                =>      GT1_RXNOTINTABLE_OUT,
      ------------------------ Receive Ports - RX AFE Ports ----------------------
      GTPRXN_IN                       =>      GT1_GTPRXN_IN,
      GTPRXP_IN                       =>      GT1_GTPRXP_IN,
      ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
      RXBUFRESET_IN                   =>      GT1_RXBUFRESET_IN,
      RXBUFSTATUS_OUT                 =>      GT1_RXBUFSTATUS_OUT,
      -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
      RXBYTEISALIGNED_OUT             =>      GT1_RXBYTEISALIGNED_OUT,
      RXMCOMMAALIGNEN_IN              =>      GT1_RXMCOMMAALIGNEN_IN,
      RXPCOMMAALIGNEN_IN              =>      GT1_RXPCOMMAALIGNEN_IN,
      -------------------- Receive Ports - RX Equailizer Ports -------------------
      RXLPMHFHOLD_IN                  =>      GT1_RXLPMHFHOLD_IN,
      RXLPMLFHOLD_IN                  =>      GT1_RXLPMLFHOLD_IN,
      --------------- Receive Ports - RX Fabric Output Control Ports -------------
      RXOUTCLK_OUT                    =>      GT1_RXOUTCLK_OUT,
      ------------- Receive Ports - RX Initialization and Reset Ports ------------
      GTRXRESET_IN                    =>      GT1_GTRXRESET_IN,
      RXPCSRESET_IN                   =>      GT1_RXPCSRESET_IN,
      RXPMARESET_IN                   =>      GT1_RXPMARESET_IN,
      -------------- Receive Ports -RX Initialization and Reset Ports ------------
      RXRESETDONE_OUT                 =>      GT1_RXRESETDONE_OUT,
      --------------------- TX Initialization and Reset Ports --------------------
      GTTXRESET_IN                    =>      GT1_GTTXRESET_IN,
      TXUSERRDY_IN                    =>      GT1_TXUSERRDY_IN,
      ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
      TXDATA_IN                       =>      GT1_TXDATA_IN,
      TXUSRCLK_IN                     =>      GT1_TXUSRCLK_IN,
      TXUSRCLK2_IN                    =>      GT1_TXUSRCLK2_IN,
      ------------------ Transmit Ports - Pattern Generator Ports ----------------
      TXPRBSFORCEERR_IN               =>      GT1_TXPRBSFORCEERR_IN,
      ------------------ Transmit Ports - TX 8B/10B Encoder Ports ----------------
      TX8B10BBYPASS_IN                =>      GT1_TX8B10BBYPASS_IN,
      TXCHARDISPMODE_IN               =>      GT1_TXCHARDISPMODE_IN,
      TXCHARDISPVAL_IN                =>      GT1_TXCHARDISPVAL_IN,
      TXCHARISK_IN                    =>      GT1_TXCHARISK_IN,
      ---------------------- Transmit Ports - TX Buffer Ports --------------------
      TXBUFSTATUS_OUT                 =>      GT1_TXBUFSTATUS_OUT,
      --------------- Transmit Ports - TX Configurable Driver Ports --------------
      GTPTXN_OUT                      =>      GT1_GTPTXN_OUT,
      GTPTXP_OUT                      =>      GT1_GTPTXP_OUT,
      ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
      TXOUTCLK_OUT                    =>      GT1_TXOUTCLK_OUT,
      TXOUTCLKFABRIC_OUT              =>      GT1_TXOUTCLKFABRIC_OUT,
      TXOUTCLKPCS_OUT                 =>      GT1_TXOUTCLKPCS_OUT,
      ------------- Transmit Ports - TX Initialization and Reset Ports -----------
      TXPCSRESET_IN                   =>      GT1_TXPCSRESET_IN,
      TXPMARESET_IN                   =>      GT1_TXPMARESET_IN,
      TXRESETDONE_OUT                 =>      GT1_TXRESETDONE_OUT,
      ------------------ Transmit Ports - pattern Generator Ports ----------------
      TXPRBSSEL_IN                    =>      GT1_TXPRBSSEL_IN

  );

  --_________________________________________________________________________
  --_________________________________________________________________________
  --GT2  (X0Y2)
  gt2_gtp_tile_a7_i : gtp_tile_a7_GT
  generic map
  (
      -- Simulation attributes
      GT_SIM_GTRESET_SPEEDUP => "FALSE",
      TXSYNC_OVRD_IN         => ('0'),
      TXSYNC_MULTILANE_IN    => ('0'),
      -- LINE RATE SELECT
      G_RATE                 => G_RATE
  )
  port map
  (
      RST_IN                          =>      gt2_rst_i,
      DRP_BUSY_OUT                    =>      GT2_DRP_BUSY_OUT,

      ---------------------------- Channel - DRP Ports  --------------------------
      DRPADDR_IN                      =>      GT2_DRPADDR_IN,
      DRPCLK_IN                       =>      GT2_DRPCLK_IN,
      DRPDI_IN                        =>      GT2_DRPDI_IN,
      DRPDO_OUT                       =>      GT2_DRPDO_OUT,
      DRPEN_IN                        =>      GT2_DRPEN_IN,
      DRPRDY_OUT                      =>      GT2_DRPRDY_OUT,
      DRPWE_IN                        =>      GT2_DRPWE_IN,
      ------------------------ GTPE2_CHANNEL Clocking Ports ----------------------
      PLL0CLK_IN                      =>      gt2_pll0clk_i,
      PLL0REFCLK_IN                   =>      gt2_pll0refclk_i,
      PLL1CLK_IN                      =>      gt2_pll1clk_i,
      PLL1REFCLK_IN                   =>      gt2_pll1refclk_i,
      --------------------- RX Initialization and Reset Ports --------------------
      RXUSERRDY_IN                    =>      GT2_RXUSERRDY_IN,
      -------------------------- RX Margin Analysis Ports ------------------------
      EYESCANDATAERROR_OUT            =>      GT2_EYESCANDATAERROR_OUT,
      ------------------------- Receive Ports - CDR Ports ------------------------
      RXCDRHOLD_IN                    =>      GT2_RXCDRHOLD_IN,
      RXCDRLOCK_OUT                   =>      GT2_RXCDRLOCK_OUT,
      ------------------- Receive Ports - Clock Correction Ports -----------------
      RXCLKCORCNT_OUT                 =>      GT2_RXCLKCORCNT_OUT,
      ------------------ Receive Ports - FPGA RX Interface Ports -----------------
      RXDATA_OUT                      =>      GT2_RXDATA_OUT,
      RXUSRCLK_IN                     =>      GT2_RXUSRCLK_IN,
      RXUSRCLK2_IN                    =>      GT2_RXUSRCLK2_IN,
      ------------------- Receive Ports - Pattern Checker Ports ------------------
      RXPRBSERR_OUT                   =>      GT2_RXPRBSERR_OUT,
      RXPRBSSEL_IN                    =>      GT2_RXPRBSSEL_IN,
      ------------------- Receive Ports - Pattern Checker ports ------------------
      RXPRBSCNTRESET_IN               =>      GT2_RXPRBSCNTRESET_IN,
      ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
      RXCHARISCOMMA_OUT               =>      GT2_RXCHARISCOMMA_OUT,
      RXCHARISK_OUT                   =>      GT2_RXCHARISK_OUT,
      RXDISPERR_OUT                   =>      GT2_RXDISPERR_OUT,
      RXNOTINTABLE_OUT                =>      GT2_RXNOTINTABLE_OUT,
      ------------------------ Receive Ports - RX AFE Ports ----------------------
      GTPRXN_IN                       =>      GT2_GTPRXN_IN,
      GTPRXP_IN                       =>      GT2_GTPRXP_IN,
      ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
      RXBUFRESET_IN                   =>      GT2_RXBUFRESET_IN,
      RXBUFSTATUS_OUT                 =>      GT2_RXBUFSTATUS_OUT,
      -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
      RXBYTEISALIGNED_OUT             =>      GT2_RXBYTEISALIGNED_OUT,
      RXMCOMMAALIGNEN_IN              =>      GT2_RXMCOMMAALIGNEN_IN,
      RXPCOMMAALIGNEN_IN              =>      GT2_RXPCOMMAALIGNEN_IN,
      -------------------- Receive Ports - RX Equailizer Ports -------------------
      RXLPMHFHOLD_IN                  =>      GT2_RXLPMHFHOLD_IN,
      RXLPMLFHOLD_IN                  =>      GT2_RXLPMLFHOLD_IN,
      --------------- Receive Ports - RX Fabric Output Control Ports -------------
      RXOUTCLK_OUT                    =>      GT2_RXOUTCLK_OUT,
      ------------- Receive Ports - RX Initialization and Reset Ports ------------
      GTRXRESET_IN                    =>      GT2_GTRXRESET_IN,
      RXPCSRESET_IN                   =>      GT2_RXPCSRESET_IN,
      RXPMARESET_IN                   =>      GT2_RXPMARESET_IN,
      -------------- Receive Ports -RX Initialization and Reset Ports ------------
      RXRESETDONE_OUT                 =>      GT2_RXRESETDONE_OUT,
      --------------------- TX Initialization and Reset Ports --------------------
      GTTXRESET_IN                    =>      GT2_GTTXRESET_IN,
      TXUSERRDY_IN                    =>      GT2_TXUSERRDY_IN,
      ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
      TXDATA_IN                       =>      GT2_TXDATA_IN,
      TXUSRCLK_IN                     =>      GT2_TXUSRCLK_IN,
      TXUSRCLK2_IN                    =>      GT2_TXUSRCLK2_IN,
      ------------------ Transmit Ports - Pattern Generator Ports ----------------
      TXPRBSFORCEERR_IN               =>      GT2_TXPRBSFORCEERR_IN,
      ------------------ Transmit Ports - TX 8B/10B Encoder Ports ----------------
      TX8B10BBYPASS_IN                =>      GT2_TX8B10BBYPASS_IN,
      TXCHARDISPMODE_IN               =>      GT2_TXCHARDISPMODE_IN,
      TXCHARDISPVAL_IN                =>      GT2_TXCHARDISPVAL_IN,
      TXCHARISK_IN                    =>      GT2_TXCHARISK_IN,
      ---------------------- Transmit Ports - TX Buffer Ports --------------------
      TXBUFSTATUS_OUT                 =>      GT2_TXBUFSTATUS_OUT,
      --------------- Transmit Ports - TX Configurable Driver Ports --------------
      GTPTXN_OUT                      =>      GT2_GTPTXN_OUT,
      GTPTXP_OUT                      =>      GT2_GTPTXP_OUT,
      ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
      TXOUTCLK_OUT                    =>      GT2_TXOUTCLK_OUT,
      TXOUTCLKFABRIC_OUT              =>      GT2_TXOUTCLKFABRIC_OUT,
      TXOUTCLKPCS_OUT                 =>      GT2_TXOUTCLKPCS_OUT,
      ------------- Transmit Ports - TX Initialization and Reset Ports -----------
      TXPCSRESET_IN                   =>      GT2_TXPCSRESET_IN,
      TXPMARESET_IN                   =>      GT2_TXPMARESET_IN,
      TXRESETDONE_OUT                 =>      GT2_TXRESETDONE_OUT,
      ------------------ Transmit Ports - pattern Generator Ports ----------------
      TXPRBSSEL_IN                    =>      GT2_TXPRBSSEL_IN

  );

  --_________________________________________________________________________
  --_________________________________________________________________________
  --GT3  (X0Y3)
  gt3_gtp_tile_a7_i : gtp_tile_a7_GT
  generic map
  (
      -- Simulation attributes
      GT_SIM_GTRESET_SPEEDUP => "FALSE",
      TXSYNC_OVRD_IN         => ('0'),
      TXSYNC_MULTILANE_IN    => ('0'),
      -- LINE RATE SELECT
      G_RATE                 => G_RATE
  )
  port map
  (
      RST_IN                          =>      gt3_rst_i,
      DRP_BUSY_OUT                    =>      GT3_DRP_BUSY_OUT,

      ---------------------------- Channel - DRP Ports  --------------------------
      DRPADDR_IN                      =>      GT3_DRPADDR_IN,
      DRPCLK_IN                       =>      GT3_DRPCLK_IN,
      DRPDI_IN                        =>      GT3_DRPDI_IN,
      DRPDO_OUT                       =>      GT3_DRPDO_OUT,
      DRPEN_IN                        =>      GT3_DRPEN_IN,
      DRPRDY_OUT                      =>      GT3_DRPRDY_OUT,
      DRPWE_IN                        =>      GT3_DRPWE_IN,
      ------------------------ GTPE2_CHANNEL Clocking Ports ----------------------
      PLL0CLK_IN                      =>      gt3_pll0clk_i,
      PLL0REFCLK_IN                   =>      gt3_pll0refclk_i,
      PLL1CLK_IN                      =>      gt3_pll1clk_i,
      PLL1REFCLK_IN                   =>      gt3_pll1refclk_i,
      --------------------- RX Initialization and Reset Ports --------------------
      RXUSERRDY_IN                    =>      GT3_RXUSERRDY_IN,
      -------------------------- RX Margin Analysis Ports ------------------------
      EYESCANDATAERROR_OUT            =>      GT3_EYESCANDATAERROR_OUT,
      ------------------------- Receive Ports - CDR Ports ------------------------
      RXCDRHOLD_IN                    =>      GT3_RXCDRHOLD_IN,
      RXCDRLOCK_OUT                   =>      GT3_RXCDRLOCK_OUT,
      ------------------- Receive Ports - Clock Correction Ports -----------------
      RXCLKCORCNT_OUT                 =>      GT3_RXCLKCORCNT_OUT,
      ------------------ Receive Ports - FPGA RX Interface Ports -----------------
      RXDATA_OUT                      =>      GT3_RXDATA_OUT,
      RXUSRCLK_IN                     =>      GT3_RXUSRCLK_IN,
      RXUSRCLK2_IN                    =>      GT3_RXUSRCLK2_IN,
      ------------------- Receive Ports - Pattern Checker Ports ------------------
      RXPRBSERR_OUT                   =>      GT3_RXPRBSERR_OUT,
      RXPRBSSEL_IN                    =>      GT3_RXPRBSSEL_IN,
      ------------------- Receive Ports - Pattern Checker ports ------------------
      RXPRBSCNTRESET_IN               =>      GT3_RXPRBSCNTRESET_IN,
      ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
      RXCHARISCOMMA_OUT               =>      GT3_RXCHARISCOMMA_OUT,
      RXCHARISK_OUT                   =>      GT3_RXCHARISK_OUT,
      RXDISPERR_OUT                   =>      GT3_RXDISPERR_OUT,
      RXNOTINTABLE_OUT                =>      GT3_RXNOTINTABLE_OUT,
      ------------------------ Receive Ports - RX AFE Ports ----------------------
      GTPRXN_IN                       =>      GT3_GTPRXN_IN,
      GTPRXP_IN                       =>      GT3_GTPRXP_IN,
      ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
      RXBUFRESET_IN                   =>      GT3_RXBUFRESET_IN,
      RXBUFSTATUS_OUT                 =>      GT3_RXBUFSTATUS_OUT,
      -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
      RXBYTEISALIGNED_OUT             =>      GT3_RXBYTEISALIGNED_OUT,
      RXMCOMMAALIGNEN_IN              =>      GT3_RXMCOMMAALIGNEN_IN,
      RXPCOMMAALIGNEN_IN              =>      GT3_RXPCOMMAALIGNEN_IN,
      -------------------- Receive Ports - RX Equailizer Ports -------------------
      RXLPMHFHOLD_IN                  =>      GT3_RXLPMHFHOLD_IN,
      RXLPMLFHOLD_IN                  =>      GT3_RXLPMLFHOLD_IN,
      --------------- Receive Ports - RX Fabric Output Control Ports -------------
      RXOUTCLK_OUT                    =>      GT3_RXOUTCLK_OUT,
      ------------- Receive Ports - RX Initialization and Reset Ports ------------
      GTRXRESET_IN                    =>      GT3_GTRXRESET_IN,
      RXPCSRESET_IN                   =>      GT3_RXPCSRESET_IN,
      RXPMARESET_IN                   =>      GT3_RXPMARESET_IN,
      -------------- Receive Ports -RX Initialization and Reset Ports ------------
      RXRESETDONE_OUT                 =>      GT3_RXRESETDONE_OUT,
      --------------------- TX Initialization and Reset Ports --------------------
      GTTXRESET_IN                    =>      GT3_GTTXRESET_IN,
      TXUSERRDY_IN                    =>      GT3_TXUSERRDY_IN,
      ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
      TXDATA_IN                       =>      GT3_TXDATA_IN,
      TXUSRCLK_IN                     =>      GT3_TXUSRCLK_IN,
      TXUSRCLK2_IN                    =>      GT3_TXUSRCLK2_IN,
      ------------------ Transmit Ports - Pattern Generator Ports ----------------
      TXPRBSFORCEERR_IN               =>      GT3_TXPRBSFORCEERR_IN,
      ------------------ Transmit Ports - TX 8B/10B Encoder Ports ----------------
      TX8B10BBYPASS_IN                =>      GT3_TX8B10BBYPASS_IN,
      TXCHARDISPMODE_IN               =>      GT3_TXCHARDISPMODE_IN,
      TXCHARDISPVAL_IN                =>      GT3_TXCHARDISPVAL_IN,
      TXCHARISK_IN                    =>      GT3_TXCHARISK_IN,
      ---------------------- Transmit Ports - TX Buffer Ports --------------------
      TXBUFSTATUS_OUT                 =>      GT3_TXBUFSTATUS_OUT,
      --------------- Transmit Ports - TX Configurable Driver Ports --------------
      GTPTXN_OUT                      =>      GT3_GTPTXN_OUT,
      GTPTXP_OUT                      =>      GT3_GTPTXP_OUT,
      ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
      TXOUTCLK_OUT                    =>      GT3_TXOUTCLK_OUT,
      TXOUTCLKFABRIC_OUT              =>      GT3_TXOUTCLKFABRIC_OUT,
      TXOUTCLKPCS_OUT                 =>      GT3_TXOUTCLKPCS_OUT,
      ------------- Transmit Ports - TX Initialization and Reset Ports -----------
      TXPCSRESET_IN                   =>      GT3_TXPCSRESET_IN,
      TXPMARESET_IN                   =>      GT3_TXPMARESET_IN,
      TXRESETDONE_OUT                 =>      GT3_TXRESETDONE_OUT,
      ------------------ Transmit Ports - pattern Generator Ports ----------------
      TXPRBSSEL_IN                    =>      GT3_TXPRBSSEL_IN

  );


  --_________________________________________________________________________
  --_________________________________________________________________________
  --_________________________GTPE2_COMMON____________________________________

  gtpe2_common_0_i : GTPE2_COMMON
  generic map
  (
          -- Simulation attributes
          SIM_RESET_SPEEDUP    => "FALSE",
          SIM_PLL0REFCLK_SEL   => ("001"),
          SIM_PLL1REFCLK_SEL   => ("001"),
          SIM_VERSION          => ("1.0"),

    PLL0_FBDIV           => PLL0_FBDIV_IN     ,
    PLL0_FBDIV_45        => PLL0_FBDIV_45_IN  ,
    PLL0_REFCLK_DIV      => PLL0_REFCLK_DIV_IN,
    PLL1_FBDIV           => PLL1_FBDIV_IN     ,
    PLL1_FBDIV_45        => PLL1_FBDIV_45_IN  ,
    PLL1_REFCLK_DIV      => PLL1_REFCLK_DIV_IN,


     ------------------COMMON BLOCK Attributes---------------
      BIAS_CFG                                =>     (x"0000000000050001"),
      COMMON_CFG                              =>     (x"00000000"),

     ----------------------------PLL Attributes----------------------------
      PLL0_CFG                                =>     (x"01F03DC"),
      PLL0_DMON_CFG                           =>     ('0'),
      PLL0_INIT_CFG                           =>     (x"00001E"),
      PLL0_LOCK_CFG                           =>     (x"1E8"),
      PLL1_CFG                                =>     (x"01F03DC"),
      PLL1_DMON_CFG                           =>     ('0'),
      PLL1_INIT_CFG                           =>     (x"00001E"),
      PLL1_LOCK_CFG                           =>     (x"1E8"),
      PLL_CLKOUT_CFG                          =>     (x"00"),

     ----------------------------Reserved Attributes----------------------------
      RSVD_ATTR0                              =>     (x"0000"),
      RSVD_ATTR1                              =>     (x"0000")


  )
  port map
  (
     DMONITOROUT             => open,
      ------------- Common Block  - Dynamic Reconfiguration Port (DRP) -----------
      DRPADDR                         =>      tied_to_ground_vec_i(7 downto 0),
      DRPCLK                          =>      tied_to_ground_i,
      DRPDI                           =>      tied_to_ground_vec_i(15 downto 0),
      DRPDO                           =>      open,
      DRPEN                           =>      tied_to_ground_i,
      DRPRDY                          =>      open,
      DRPWE                           =>      tied_to_ground_i,
      ----------------- Common Block - GTPE2_COMMON Clocking Ports ---------------
      GTEASTREFCLK0                   =>      tied_to_ground_i,
      GTEASTREFCLK1                   =>      tied_to_ground_i,
      GTGREFCLK1                      =>      tied_to_ground_i,
      GTREFCLK0                       =>      GT0_GTREFCLK0_IN,
      GTREFCLK1                       =>      tied_to_ground_i,
      GTWESTREFCLK0                   =>      tied_to_ground_i,
      GTWESTREFCLK1                   =>      tied_to_ground_i,
      PLL0OUTCLK                      =>      gt0_pll0outclk_i,
      PLL0OUTREFCLK                   =>      gt0_pll0outrefclk_i,
      PLL1OUTCLK                      =>      gt0_pll1outclk_i,
      PLL1OUTREFCLK                   =>      gt0_pll1outrefclk_i,
      -------------------------- Common Block - PLL Ports ------------------------
      PLL0FBCLKLOST                   =>      open,
      PLL0LOCK                        =>      GT0_PLL0LOCK_OUT,
      PLL0LOCKDETCLK                  =>      GT0_PLL0LOCKDETCLK_IN,
      PLL0LOCKEN                      =>      tied_to_vcc_i,
      PLL0PD                          =>      tied_to_ground_i,
      PLL0REFCLKLOST                  =>      GT0_PLL0REFCLKLOST_OUT,
      PLL0REFCLKSEL                   =>      "001",
      PLL0RESET                       =>      GT0_PLL0RESET_IN,
      PLL1FBCLKLOST                   =>      open,
      PLL1LOCK                        =>      open,
      PLL1LOCKDETCLK                  =>      tied_to_ground_i,
      PLL1LOCKEN                      =>      tied_to_vcc_i,
      PLL1PD                          =>      '1',
      PLL1REFCLKLOST                  =>      open,
      PLL1REFCLKSEL                   =>      "001",
      PLL1RESET                       =>      tied_to_ground_i,
      ---------------------------- Common Block - Ports --------------------------
      BGRCALOVRDENB                   =>      tied_to_vcc_i,
      GTGREFCLK0                      =>      tied_to_ground_i,
      PLLRSVD1                        =>      "0000000000000000",
      PLLRSVD2                        =>      "00000",
      REFCLKOUTMONITOR0               =>      open,
      REFCLKOUTMONITOR1               =>      open,
      ------------------------ Common Block - RX AFE Ports -----------------------
      PMARSVDOUT                      =>      open,
      --------------------------------- QPLL Ports -------------------------------
      BGBYPASSB                       =>      tied_to_vcc_i,
      BGMONITORENB                    =>      tied_to_vcc_i,
      BGPDB                           =>      tied_to_vcc_i,
      BGRCALOVRD                      =>      "00000",
      PMARSVD                         =>      "00000000",
      RCALENB                         =>      tied_to_vcc_i

  );


end RTL;



library ieee;
use ieee.STD_LOGIC_1164.all;
use ieee.NUMERIC_STD.all;
library work;
use work.GTP_TILE_PKG.all;
entity GTP_A7 is
generic (
         G_RATE : GTP_RATE_TYPE := GBPS_3_0
         );
port (
      PRBSTxEna0 : in std_logic_vector(2 downto 0 );
      PRBSTxEna1 : in std_logic_vector(2 downto 0 );
      PRBSTxEna2 : in std_logic_vector(2 downto 0 );
      PRBSTxEna3 : in std_logic_vector(2 downto 0 );
      PRBSErrCnt0 : out std_logic_vector(15 downto 0 );
      PRBSErrCnt1 : out std_logic_vector(15 downto 0 );
      PRBSErrCnt2 : out std_logic_vector(15 downto 0 );
      PRBSErrCnt3 : out std_logic_vector(15 downto 0 );
      RxPMARst0 : in std_logic;
      RxPMARst1 : in std_logic;
      RxPMARst2 : in std_logic;
      RxPMARst3 : in std_logic;
      Rx0_N : in std_logic;
      Rx0_P : in std_logic;
      PLLRefClkLost : out std_logic;
      PRBSCntReset0 : in std_logic;
      PRBSCntReset1 : in std_logic;
      PRBSCntReset2 : in std_logic;
      PRBSCntReset3 : in std_logic;
      Valid0 : in std_logic;
      Valid1 : in std_logic;
      Valid2 : in std_logic;
      Valid3 : in std_logic;
      TxBufStatus0 : out std_logic_vector(1 downto 0 );
      TxBufStatus1 : out std_logic_vector(1 downto 0 );
      TxBufStatus2 : out std_logic_vector(1 downto 0 );
      TxBufStatus3 : out std_logic_vector(1 downto 0 );
      RxPCSRst0 : in std_logic;
      RxPCSRst1 : in std_logic;
      RxPCSRst2 : in std_logic;
      RxPCSRst3 : in std_logic;
      TxRst0 : in std_logic;
      TxRst1 : in std_logic;
      TxRst2 : in std_logic;
      TxRst3 : in std_logic;
      PRBSTxForce0 : in std_logic;
      PRBSTxForce1 : in std_logic;
      Tx0_N : out std_logic;
      PRBSTxForce2 : in std_logic;
      PRBSTxForce3 : in std_logic;
      Tx0_P : out std_logic;
      TxStr0 : in std_logic;
      TxStr1 : in std_logic;
      TxStr2 : in std_logic;
      TxStr3 : in std_logic;
      AlignRx0 : in std_logic;
      AlignRx1 : in std_logic;
      TxUserReady0 : in std_logic;
      AlignRx2 : in std_logic;
      TxUserReady1 : in std_logic;
      AlignRx3 : in std_logic;
      TxUserReady2 : in std_logic;
      TxUserReady3 : in std_logic;
      PRBSRxEna0 : in std_logic_vector(2 downto 0 );
      PRBSRxEna1 : in std_logic_vector(2 downto 0 );
      PRBSRxEna2 : in std_logic_vector(2 downto 0 );
      PRBSRxEna3 : in std_logic_vector(2 downto 0 );
      Rx2_N : in std_logic;
      IsAligned0 : out std_logic;
      IsAligned1 : out std_logic;
      Rx2_P : in std_logic;
      IsAligned2 : out std_logic;
      IsAligned3 : out std_logic;
      RxClkCorCnt0 : out std_logic_vector(1 downto 0 );
      RxClkCorCnt1 : out std_logic_vector(1 downto 0 );
      RxClkCorCnt2 : out std_logic_vector(1 downto 0 );
      RxClkCorCnt3 : out std_logic_vector(1 downto 0 );
      RxRstDone0 : out std_logic;
      Tx2_N : out std_logic;
      RxRstDone1 : out std_logic;
      RxRstDone2 : out std_logic;
      Tx2_P : out std_logic;
      RxRstDone3 : out std_logic;
      TxRstDone0 : out std_logic;
      TxRstDone1 : out std_logic;
      TxRstDone2 : out std_logic;
      TxRstDone3 : out std_logic;
      RxRst0 : in std_logic;
      RxRst1 : in std_logic;
      RxRst2 : in std_logic;
      RxRst3 : in std_logic;
      RxStr0 : out std_logic;
      RxStr1 : out std_logic;
      RxStr2 : out std_logic;
      RxStr3 : out std_logic;
      RxData0 : out std_logic_vector(15 downto 0 );
      RxData1 : out std_logic_vector(15 downto 0 );
      RxData2 : out std_logic_vector(15 downto 0 );
      RxData3 : out std_logic_vector(15 downto 0 );
      RxUserReady0 : in std_logic;
      RxUserReady1 : in std_logic;
      RxUserReady2 : in std_logic;
      Faults_DispError0 : out std_logic;
      RxUserReady3 : in std_logic;
      Faults_DispError1 : out std_logic;
      Faults_DispError2 : out std_logic;
      Faults_DispError3 : out std_logic;
      PLLRst : in std_logic;
      RxCommaMisalign0 : out std_logic;
      RxCommaMisalign1 : out std_logic;
      RxCommaMisalign2 : out std_logic;
      RxCommaMisalign3 : out std_logic;
      RxDoubleComma0 : out std_logic;
      RxDoubleComma1 : out std_logic;
      RxDoubleComma2 : out std_logic;
      RxDoubleComma3 : out std_logic;
      Faults_NotInTable0 : out std_logic;
      Faults_NotInTable1 : out std_logic;
      Faults_NotInTable2 : out std_logic;
      Faults_NotInTable3 : out std_logic;
      RxDataTest0 : out std_logic_vector(15 downto 0 );
      RxDataTest1 : out std_logic_vector(15 downto 0 );
      PLLLock : out std_logic;
      RxDataTest2 : out std_logic_vector(15 downto 0 );
      Rx1_N : in std_logic;
      RxDataTest3 : out std_logic_vector(15 downto 0 );
      Rx1_P : in std_logic;
      GTPClk : in std_logic;
      RxComma0 : out std_logic;
      RxComma1 : out std_logic;
      RxComma2 : out std_logic;
      RxComma3 : out std_logic;
      Tx1_N : out std_logic;
      Tx1_P : out std_logic;
      RxIsKTest0 : out std_logic_vector(1 downto 0 );
      RxIsKTest1 : out std_logic_vector(1 downto 0 );
      TxMkr0 : in std_logic;
      RxIsKTest2 : out std_logic_vector(1 downto 0 );
      TxMkr1 : in std_logic;
      RxIsKTest3 : out std_logic_vector(1 downto 0 );
      TxMkr2 : in std_logic;
      TxMkr3 : in std_logic;
      Rx3_N : in std_logic;
      Rx3_P : in std_logic;
      TxPMARst0 : in std_logic;
      TxPMARst1 : in std_logic;
      TxPMARst2 : in std_logic;
      TxPMARst3 : in std_logic;
      RxBufStatus0 : out std_logic_vector(2 downto 0 );
      RxBufStatus1 : out std_logic_vector(2 downto 0 );
      RxBufStatus2 : out std_logic_vector(2 downto 0 );
      RxBufStatus3 : out std_logic_vector(2 downto 0 );
      TxData0 : in std_logic_vector(15 downto 0 );
      TxData1 : in std_logic_vector(15 downto 0 );
      TxData2 : in std_logic_vector(15 downto 0 );
      TxData3 : in std_logic_vector(15 downto 0 );
      UserClk : in std_logic;
      TxPCSRst0 : in std_logic;
      TxPCSRst1 : in std_logic;
      TxPCSRst2 : in std_logic;
      TxPCSRst3 : in std_logic;
      Tx3_N : out std_logic;
      Tx3_P : out std_logic;
      RxBufRst0 : in std_logic;
      RxBufRst1 : in std_logic;
      RxBufRst2 : in std_logic;
      RxBufRst3 : in std_logic
      );


end GTP_A7;


use work.all;
architecture GTP_A7 of GTP_A7 is

signal PRBSErr3 : std_logic;
signal Rx3ByteNotAligned : std_logic;
signal PLLLock_loc : std_logic;
signal CEN0 : std_logic;
signal CEN1 : std_logic;
signal CEN2 : std_logic;
signal CEN3 : std_logic;
signal GT2_RxRst : std_logic;
signal PRBSErr1_1 : std_logic;
signal AlignRx0Q : std_logic;
signal Rx0InvalidSignals : std_logic_vector(3 downto 0 );
signal Faults_NotInTable3_loc : std_logic;
signal Sfp3NotOk : std_logic;
signal Rx3Invalid : std_logic;
signal GT0_TXDATA_IN : std_logic_vector(15 downto 0 );
signal GT2_RxPCSRst : std_logic;
signal GT0_RXDISPERR : std_logic_vector(1 downto 0 );
signal Rx2Invalid : std_logic;
signal GT1_TXCHARDISPMODE_IN : std_logic_vector(1 downto 0 );
signal GT3_RXBYTEISALIGNED : std_logic;
signal AlignRx3Q : std_logic;
signal GT0_RxPCSRst : std_logic;
signal GT1_TXDATA_IN : std_logic_vector(15 downto 0 );
signal Faults_DispError3_loc : std_logic;
signal Rx1Invalid : std_logic;
signal Rx1InvalidSignals : std_logic_vector(3 downto 0 );
signal PRBSRx0 : std_logic;
signal PRBSRx1 : std_logic;
signal PRBSRx2 : std_logic;
signal PRBSRx3 : std_logic;
signal GT1_RXDISPERR : std_logic_vector(1 downto 0 );
signal GT2_RXBYTEISALIGNED : std_logic;
signal Sfp2NotOk : std_logic;
signal Rx0Invalid : std_logic;
signal GT3_RXENCOMMAALIGN : std_logic;
signal AlignRx1Q : std_logic;
signal GT1_RXENCOMMAALIGN : std_logic;
signal Loc_PRBSErrCnt0 : std_logic_vector(15 downto 0 );
signal Loc_PRBSErrCnt1 : std_logic_vector(15 downto 0 );
signal Loc_PRBSErrCnt2 : std_logic_vector(15 downto 0 );
signal GT3_RxRst : std_logic;
signal AlignRx2Q : std_logic;
signal Loc_PRBSErrCnt3 : std_logic_vector(15 downto 0 );
signal AlignedEdge0 : std_logic;
signal AlignedEdge1 : std_logic;
signal GT3_RXCHARISK_OUT : std_logic_vector(1 downto 0 );
signal AlignedEdge2 : std_logic;
signal AlignedEdge3 : std_logic;
signal Sfp1NotOk : std_logic;
signal Faults_DispError0_loc : std_logic;
signal GT2_RXNOTINTABLE : std_logic_vector(1 downto 0 );
signal TXDATA2_IN : std_logic_vector(19 downto 0 );
signal TXDATA0_IN : std_logic_vector(19 downto 0 );
signal Rx2ByteNotAligned : std_logic;
signal Rx3InvalidSignals : std_logic_vector(3 downto 0 );
signal GT3_RXNOTINTABLE : std_logic_vector(1 downto 0 );
signal GT0_TXCHARDISPMODE_IN : std_logic_vector(1 downto 0 );
signal Rx1ByteNotAligned : std_logic;
signal Sfp0NotOk : std_logic;
signal PRBSErr0_1 : std_logic;
signal RxRstDone1_loc : std_logic;
signal Faults_DispError2_loc : std_logic;
signal RxRst1Disable : std_logic;
signal GT1_RXCHARISK_OUT : std_logic_vector(1 downto 0 );
signal KMode0 : std_logic_vector(1 downto 0 );
signal KMode1 : std_logic_vector(1 downto 0 );
signal KMode2 : std_logic_vector(1 downto 0 );
signal KMode3 : std_logic_vector(1 downto 0 );
signal GT3_TXCHARDISPVAL_IN : std_logic_vector(1 downto 0 );
signal RxRst0Disable : std_logic;
signal RxRstDone2_loc : std_logic;
signal GT0_RxRst : std_logic;
signal Faults_DispError1_loc : std_logic;
signal GT2_TXDATA_IN : std_logic_vector(15 downto 0 );
signal Rx3Valid : std_logic;
signal GT1_RXDATA_OUT : std_logic_vector(15 downto 0 );
signal GT2_RXDISPERR : std_logic_vector(1 downto 0 );
signal One2b : std_logic_vector(1 downto 0 );
signal GT1_TXCHARDISPVAL_IN : std_logic_vector(1 downto 0 );
signal GT3_TXCHARDISPMODE_IN : std_logic_vector(1 downto 0 );
signal GT1_RXBYTEISALIGNED : std_logic;
signal GT3_TXDATA_IN : std_logic_vector(15 downto 0 );
signal GT2_TXCHARDISPVAL_IN : std_logic_vector(1 downto 0 );
signal RxRstDone0_loc : std_logic;
signal GT3_RxPCSRst : std_logic;
signal PRBSErr2_1 : std_logic;
signal GT3_RXDISPERR : std_logic_vector(1 downto 0 );
signal TERCNT0 : std_logic;
signal TERCNT1 : std_logic;
signal TERCNT2 : std_logic;
signal GT0_RXBYTEISALIGNED : std_logic;
signal TERCNT3 : std_logic;
signal Faults_NotInTable1_loc : std_logic;
signal GT0_RXENCOMMAALIGN : std_logic;
signal GT1_RxPCSRst : std_logic;
signal Rx2Valid : std_logic;
signal GT3_RXDATA_OUT : std_logic_vector(15 downto 0 );
signal Zero16b : std_logic_vector(15 downto 0 );
signal Zero2b : std_logic_vector(1 downto 0 );
signal GT0_RXDATA_OUT : std_logic_vector(15 downto 0 );
signal GT0_TXCHARDISPVAL_IN : std_logic_vector(1 downto 0 );
signal GT2_RXCHARISK_OUT : std_logic_vector(1 downto 0 );
signal Faults_NotInTable2_loc : std_logic;
signal GT2_RXENCOMMAALIGN : std_logic;
signal PRBSErr3_1 : std_logic;
signal GT1_RxRst : std_logic;
signal Rx2InvalidSignals : std_logic_vector(3 downto 0 );
signal Rx1Valid : std_logic;
signal GT2_RXDATA_OUT : std_logic_vector(15 downto 0 );
signal Rx0ByteNotAligned : std_logic;
signal Faults_NotInTable0_loc : std_logic;
signal TXDATA3_IN : std_logic_vector(19 downto 0 );
signal RxRst3Disable : std_logic;
signal GT2_TXCHARDISPMODE_IN : std_logic_vector(1 downto 0 );
signal Zero : std_logic;
signal TXDATA1_IN : std_logic_vector(19 downto 0 );
signal GT0_RXNOTINTABLE : std_logic_vector(1 downto 0 );
signal RxRst2Disable : std_logic;
signal Rx0Valid : std_logic;
signal GT0_RXCHARISK_OUT : std_logic_vector(1 downto 0 );
signal RxRstDone3_loc : std_logic;
signal GT1_RXNOTINTABLE : std_logic_vector(1 downto 0 );
signal Zero9b : std_logic_vector(8 downto 0 );
signal PRBSErr0 : std_logic;
signal PRBSErr1 : std_logic;
signal PRBSErr2 : std_logic;
component GTP_TILE_A7
    generic (
             G_RATE : GTP_RATE_TYPE := GBPS_3_0
             );
    port (
          GT0_DRP_BUSY_OUT : out std_logic;
          GT0_DRPADDR_IN : in std_logic_vector(8 downto 0 );
          GT0_DRPCLK_IN : in std_logic;
          GT0_DRPDI_IN : in std_logic_vector(15 downto 0 );
          GT0_DRPDO_OUT : out std_logic_vector(15 downto 0 );
          GT0_DRPEN_IN : in std_logic;
          GT0_DRPRDY_OUT : out std_logic;
          GT0_DRPWE_IN : in std_logic;
          GT0_RXUSERRDY_IN : in std_logic;
          GT0_EYESCANDATAERROR_OUT : out std_logic;
          GT0_RXCDRHOLD_IN : in std_logic;
          GT0_RXCDRLOCK_OUT : out std_logic;
          GT0_RXCLKCORCNT_OUT : out std_logic_vector(1 downto 0 );
          GT0_RXDATA_OUT : out std_logic_vector(15 downto 0 );
          GT0_RXUSRCLK_IN : in std_logic;
          GT0_RXUSRCLK2_IN : in std_logic;
          GT0_RXPRBSERR_OUT : out std_logic;
          GT0_RXPRBSSEL_IN : in std_logic_vector(2 downto 0 );
          GT0_RXPRBSCNTRESET_IN : in std_logic;
          GT0_RXCHARISCOMMA_OUT : out std_logic_vector(1 downto 0 );
          GT0_RXCHARISK_OUT : out std_logic_vector(1 downto 0 );
          GT0_RXDISPERR_OUT : out std_logic_vector(1 downto 0 );
          GT0_RXNOTINTABLE_OUT : out std_logic_vector(1 downto 0 );
          GT0_GTPRXN_IN : in std_logic;
          GT0_GTPRXP_IN : in std_logic;
          GT0_RXBUFRESET_IN : in std_logic;
          GT0_RXBUFSTATUS_OUT : out std_logic_vector(2 downto 0 );
          GT0_RXBYTEISALIGNED_OUT : out std_logic;
          GT0_RXMCOMMAALIGNEN_IN : in std_logic;
          GT0_RXPCOMMAALIGNEN_IN : in std_logic;
          GT0_RXLPMHFHOLD_IN : in std_logic;
          GT0_RXLPMLFHOLD_IN : in std_logic;
          GT0_RXOUTCLK_OUT : out std_logic;
          GT0_GTRXRESET_IN : in std_logic;
          GT0_RXPCSRESET_IN : in std_logic;
          GT0_RXPMARESET_IN : in std_logic;
          GT0_RXRESETDONE_OUT : out std_logic;
          GT0_GTTXRESET_IN : in std_logic;
          GT0_TXUSERRDY_IN : in std_logic;
          GT0_TXDATA_IN : in std_logic_vector(15 downto 0 );
          GT0_TXUSRCLK_IN : in std_logic;
          GT0_TXUSRCLK2_IN : in std_logic;
          GT0_TXPRBSFORCEERR_IN : in std_logic;
          GT0_TX8B10BBYPASS_IN : in std_logic_vector(1 downto 0 );
          GT0_TXCHARDISPMODE_IN : in std_logic_vector(1 downto 0 );
          GT0_TXCHARDISPVAL_IN : in std_logic_vector(1 downto 0 );
          GT0_TXCHARISK_IN : in std_logic_vector(1 downto 0 );
          GT0_TXBUFSTATUS_OUT : out std_logic_vector(1 downto 0 );
          GT0_GTPTXN_OUT : out std_logic;
          GT0_GTPTXP_OUT : out std_logic;
          GT0_TXOUTCLK_OUT : out std_logic;
          GT0_TXOUTCLKFABRIC_OUT : out std_logic;
          GT0_TXOUTCLKPCS_OUT : out std_logic;
          GT0_TXPCSRESET_IN : in std_logic;
          GT0_TXPMARESET_IN : in std_logic;
          GT0_TXRESETDONE_OUT : out std_logic;
          GT0_TXPRBSSEL_IN : in std_logic_vector(2 downto 0 );
          GT1_DRP_BUSY_OUT : out std_logic;
          GT1_DRPADDR_IN : in std_logic_vector(8 downto 0 );
          GT1_DRPCLK_IN : in std_logic;
          GT1_DRPDI_IN : in std_logic_vector(15 downto 0 );
          GT1_DRPDO_OUT : out std_logic_vector(15 downto 0 );
          GT1_DRPEN_IN : in std_logic;
          GT1_DRPRDY_OUT : out std_logic;
          GT1_DRPWE_IN : in std_logic;
          GT1_RXUSERRDY_IN : in std_logic;
          GT1_EYESCANDATAERROR_OUT : out std_logic;
          GT1_RXCDRHOLD_IN : in std_logic;
          GT1_RXCDRLOCK_OUT : out std_logic;
          GT1_RXCLKCORCNT_OUT : out std_logic_vector(1 downto 0 );
          GT1_RXDATA_OUT : out std_logic_vector(15 downto 0 );
          GT1_RXUSRCLK_IN : in std_logic;
          GT1_RXUSRCLK2_IN : in std_logic;
          GT1_RXPRBSERR_OUT : out std_logic;
          GT1_RXPRBSSEL_IN : in std_logic_vector(2 downto 0 );
          GT1_RXPRBSCNTRESET_IN : in std_logic;
          GT1_RXCHARISCOMMA_OUT : out std_logic_vector(1 downto 0 );
          GT1_RXCHARISK_OUT : out std_logic_vector(1 downto 0 );
          GT1_RXDISPERR_OUT : out std_logic_vector(1 downto 0 );
          GT1_RXNOTINTABLE_OUT : out std_logic_vector(1 downto 0 );
          GT1_GTPRXN_IN : in std_logic;
          GT1_GTPRXP_IN : in std_logic;
          GT1_RXBUFRESET_IN : in std_logic;
          GT1_RXBUFSTATUS_OUT : out std_logic_vector(2 downto 0 );
          GT1_RXBYTEISALIGNED_OUT : out std_logic;
          GT1_RXMCOMMAALIGNEN_IN : in std_logic;
          GT1_RXPCOMMAALIGNEN_IN : in std_logic;
          GT1_RXLPMHFHOLD_IN : in std_logic;
          GT1_RXLPMLFHOLD_IN : in std_logic;
          GT1_RXOUTCLK_OUT : out std_logic;
          GT1_GTRXRESET_IN : in std_logic;
          GT1_RXPCSRESET_IN : in std_logic;
          GT1_RXPMARESET_IN : in std_logic;
          GT1_RXRESETDONE_OUT : out std_logic;
          GT1_GTTXRESET_IN : in std_logic;
          GT1_TXUSERRDY_IN : in std_logic;
          GT1_TXDATA_IN : in std_logic_vector(15 downto 0 );
          GT1_TXUSRCLK_IN : in std_logic;
          GT1_TXUSRCLK2_IN : in std_logic;
          GT1_TXPRBSFORCEERR_IN : in std_logic;
          GT1_TX8B10BBYPASS_IN : in std_logic_vector(1 downto 0 );
          GT1_TXCHARDISPMODE_IN : in std_logic_vector(1 downto 0 );
          GT1_TXCHARDISPVAL_IN : in std_logic_vector(1 downto 0 );
          GT1_TXCHARISK_IN : in std_logic_vector(1 downto 0 );
          GT1_TXBUFSTATUS_OUT : out std_logic_vector(1 downto 0 );
          GT1_GTPTXN_OUT : out std_logic;
          GT1_GTPTXP_OUT : out std_logic;
          GT1_TXOUTCLK_OUT : out std_logic;
          GT1_TXOUTCLKFABRIC_OUT : out std_logic;
          GT1_TXOUTCLKPCS_OUT : out std_logic;
          GT1_TXPCSRESET_IN : in std_logic;
          GT1_TXPMARESET_IN : in std_logic;
          GT1_TXRESETDONE_OUT : out std_logic;
          GT1_TXPRBSSEL_IN : in std_logic_vector(2 downto 0 );
          GT2_DRP_BUSY_OUT : out std_logic;
          GT2_DRPADDR_IN : in std_logic_vector(8 downto 0 );
          GT2_DRPCLK_IN : in std_logic;
          GT2_DRPDI_IN : in std_logic_vector(15 downto 0 );
          GT2_DRPDO_OUT : out std_logic_vector(15 downto 0 );
          GT2_DRPEN_IN : in std_logic;
          GT2_DRPRDY_OUT : out std_logic;
          GT2_DRPWE_IN : in std_logic;
          GT2_RXUSERRDY_IN : in std_logic;
          GT2_EYESCANDATAERROR_OUT : out std_logic;
          GT2_RXCDRHOLD_IN : in std_logic;
          GT2_RXCDRLOCK_OUT : out std_logic;
          GT2_RXCLKCORCNT_OUT : out std_logic_vector(1 downto 0 );
          GT2_RXDATA_OUT : out std_logic_vector(15 downto 0 );
          GT2_RXUSRCLK_IN : in std_logic;
          GT2_RXUSRCLK2_IN : in std_logic;
          GT2_RXPRBSERR_OUT : out std_logic;
          GT2_RXPRBSSEL_IN : in std_logic_vector(2 downto 0 );
          GT2_RXPRBSCNTRESET_IN : in std_logic;
          GT2_RXCHARISCOMMA_OUT : out std_logic_vector(1 downto 0 );
          GT2_RXCHARISK_OUT : out std_logic_vector(1 downto 0 );
          GT2_RXDISPERR_OUT : out std_logic_vector(1 downto 0 );
          GT2_RXNOTINTABLE_OUT : out std_logic_vector(1 downto 0 );
          GT2_GTPRXN_IN : in std_logic;
          GT2_GTPRXP_IN : in std_logic;
          GT2_RXBUFRESET_IN : in std_logic;
          GT2_RXBUFSTATUS_OUT : out std_logic_vector(2 downto 0 );
          GT2_RXBYTEISALIGNED_OUT : out std_logic;
          GT2_RXMCOMMAALIGNEN_IN : in std_logic;
          GT2_RXPCOMMAALIGNEN_IN : in std_logic;
          GT2_RXLPMHFHOLD_IN : in std_logic;
          GT2_RXLPMLFHOLD_IN : in std_logic;
          GT2_RXOUTCLK_OUT : out std_logic;
          GT2_GTRXRESET_IN : in std_logic;
          GT2_RXPCSRESET_IN : in std_logic;
          GT2_RXPMARESET_IN : in std_logic;
          GT2_RXRESETDONE_OUT : out std_logic;
          GT2_GTTXRESET_IN : in std_logic;
          GT2_TXUSERRDY_IN : in std_logic;
          GT2_TXDATA_IN : in std_logic_vector(15 downto 0 );
          GT2_TXUSRCLK_IN : in std_logic;
          GT2_TXUSRCLK2_IN : in std_logic;
          GT2_TXPRBSFORCEERR_IN : in std_logic;
          GT2_TX8B10BBYPASS_IN : in std_logic_vector(1 downto 0 );
          GT2_TXCHARDISPMODE_IN : in std_logic_vector(1 downto 0 );
          GT2_TXCHARDISPVAL_IN : in std_logic_vector(1 downto 0 );
          GT2_TXCHARISK_IN : in std_logic_vector(1 downto 0 );
          GT2_TXBUFSTATUS_OUT : out std_logic_vector(1 downto 0 );
          GT2_GTPTXN_OUT : out std_logic;
          GT2_GTPTXP_OUT : out std_logic;
          GT2_TXOUTCLK_OUT : out std_logic;
          GT2_TXOUTCLKFABRIC_OUT : out std_logic;
          GT2_TXOUTCLKPCS_OUT : out std_logic;
          GT2_TXPCSRESET_IN : in std_logic;
          GT2_TXPMARESET_IN : in std_logic;
          GT2_TXRESETDONE_OUT : out std_logic;
          GT2_TXPRBSSEL_IN : in std_logic_vector(2 downto 0 );
          GT3_DRP_BUSY_OUT : out std_logic;
          GT3_DRPADDR_IN : in std_logic_vector(8 downto 0 );
          GT3_DRPCLK_IN : in std_logic;
          GT3_DRPDI_IN : in std_logic_vector(15 downto 0 );
          GT3_DRPDO_OUT : out std_logic_vector(15 downto 0 );
          GT3_DRPEN_IN : in std_logic;
          GT3_DRPRDY_OUT : out std_logic;
          GT3_DRPWE_IN : in std_logic;
          GT3_RXUSERRDY_IN : in std_logic;
          GT3_EYESCANDATAERROR_OUT : out std_logic;
          GT3_RXCDRHOLD_IN : in std_logic;
          GT3_RXCDRLOCK_OUT : out std_logic;
          GT3_RXCLKCORCNT_OUT : out std_logic_vector(1 downto 0 );
          GT3_RXDATA_OUT : out std_logic_vector(15 downto 0 );
          GT3_RXUSRCLK_IN : in std_logic;
          GT3_RXUSRCLK2_IN : in std_logic;
          GT3_RXPRBSERR_OUT : out std_logic;
          GT3_RXPRBSSEL_IN : in std_logic_vector(2 downto 0 );
          GT3_RXPRBSCNTRESET_IN : in std_logic;
          GT3_RXCHARISCOMMA_OUT : out std_logic_vector(1 downto 0 );
          GT3_RXCHARISK_OUT : out std_logic_vector(1 downto 0 );
          GT3_RXDISPERR_OUT : out std_logic_vector(1 downto 0 );
          GT3_RXNOTINTABLE_OUT : out std_logic_vector(1 downto 0 );
          GT3_GTPRXN_IN : in std_logic;
          GT3_GTPRXP_IN : in std_logic;
          GT3_RXBUFRESET_IN : in std_logic;
          GT3_RXBUFSTATUS_OUT : out std_logic_vector(2 downto 0 );
          GT3_RXBYTEISALIGNED_OUT : out std_logic;
          GT3_RXMCOMMAALIGNEN_IN : in std_logic;
          GT3_RXPCOMMAALIGNEN_IN : in std_logic;
          GT3_RXLPMHFHOLD_IN : in std_logic;
          GT3_RXLPMLFHOLD_IN : in std_logic;
          GT3_RXOUTCLK_OUT : out std_logic;
          GT3_GTRXRESET_IN : in std_logic;
          GT3_RXPCSRESET_IN : in std_logic;
          GT3_RXPMARESET_IN : in std_logic;
          GT3_RXRESETDONE_OUT : out std_logic;
          GT3_GTTXRESET_IN : in std_logic;
          GT3_TXUSERRDY_IN : in std_logic;
          GT3_TXDATA_IN : in std_logic_vector(15 downto 0 );
          GT3_TXUSRCLK_IN : in std_logic;
          GT3_TXUSRCLK2_IN : in std_logic;
          GT3_TXPRBSFORCEERR_IN : in std_logic;
          GT3_TX8B10BBYPASS_IN : in std_logic_vector(1 downto 0 );
          GT3_TXCHARDISPMODE_IN : in std_logic_vector(1 downto 0 );
          GT3_TXCHARDISPVAL_IN : in std_logic_vector(1 downto 0 );
          GT3_TXCHARISK_IN : in std_logic_vector(1 downto 0 );
          GT3_TXBUFSTATUS_OUT : out std_logic_vector(1 downto 0 );
          GT3_GTPTXN_OUT : out std_logic;
          GT3_GTPTXP_OUT : out std_logic;
          GT3_TXOUTCLK_OUT : out std_logic;
          GT3_TXOUTCLKFABRIC_OUT : out std_logic;
          GT3_TXOUTCLKPCS_OUT : out std_logic;
          GT3_TXPCSRESET_IN : in std_logic;
          GT3_TXPMARESET_IN : in std_logic;
          GT3_TXRESETDONE_OUT : out std_logic;
          GT3_TXPRBSSEL_IN : in std_logic_vector(2 downto 0 );
          GT0_GTREFCLK0_IN : in std_logic;
          GT0_PLL0LOCK_OUT : out std_logic;
          GT0_PLL0LOCKDETCLK_IN : in std_logic;
          GT0_PLL0REFCLKLOST_OUT : out std_logic;
          GT0_PLL0RESET_IN : in std_logic
          );
end component;
component GTP_16b20bEnc
    port (
          In_16b : in std_logic_vector(15 downto 0 );
          Clk : in std_logic;
          Out_20b : out std_logic_vector(19 downto 0 );
          Rst : in std_logic;
          KMode : in std_logic_vector(1 downto 0 );
          Str : in std_logic
          );
end component;
component GTP_RxLogic
    port (
          GTPData : in std_logic_vector(15 downto 0 );
          RxData : out std_logic_vector(15 downto 0 );
          GTPIsK : in std_logic_vector(1 downto 0 );
          UsrClk : in std_logic;
          RxStr : out std_logic;
          RxCommaMisalign : out std_logic;
          RxDoubleComma : out std_logic;
          RxComma : out std_logic;
          Valid : in std_logic
          );
end component;
component RSFF
    port (
          Clk : in std_logic;
          Set : in std_logic;
          Clr : in std_logic;
          Rst : in std_logic := '0';
          Q : out std_logic
          );
end component;
component PosEdge
    port (
          Clk : in std_logic;
          SigIn : in std_logic;
          PEdg : out std_logic
          );
end component;
component GTP_RstSeq
    port (
          Clk : in std_logic;
          Rst : in std_logic;
          Invalid : in std_logic;
          RstRxCDR_in : in std_logic := '0';
          RstRx_in : in std_logic := '0';
          RstRxCDR_out : out std_logic;
          RstRx_out : out std_logic;
          RstDone : in std_logic;
          Sta : out std_logic_vector(2 downto 0 );
          Disable : in std_logic
          );
end component;

  component ila_1
  port (
    clk    : in std_logic;
    probe0 : in std_logic_vector(15 downto 0 );
    probe1 : in std_logic;
    probe2 : in std_logic;
    probe3 : in std_logic;
    probe4 : in std_logic_vector(4 downto 0 );
    probe5 : in std_logic_vector(4 downto 0 );
    probe6 : in std_logic;
    probe7 : in std_logic;
    probe8 : in std_logic
  );
  end component;

signal visual_C67_cur_state : std_logic_vector(16 - 1 downto 0 );
signal visual_C67_next_state : std_logic_vector(16 - 1 downto 0 );
signal visual_C67_next_count : std_logic_vector(16 - 1 downto 0 );
signal visual_C67_en_state : std_logic_vector(16 - 1 downto 0 );
signal visual_C68_cur_state : std_logic_vector(16 - 1 downto 0 );
signal visual_C68_next_state : std_logic_vector(16 - 1 downto 0 );
signal visual_C68_next_count : std_logic_vector(16 - 1 downto 0 );
signal visual_C68_en_state : std_logic_vector(16 - 1 downto 0 );
signal visual_C72_cur_state : std_logic_vector(16 - 1 downto 0 );
signal visual_C72_next_state : std_logic_vector(16 - 1 downto 0 );
signal visual_C72_next_count : std_logic_vector(16 - 1 downto 0 );
signal visual_C72_en_state : std_logic_vector(16 - 1 downto 0 );
signal visual_C74_cur_state : std_logic_vector(16 - 1 downto 0 );
signal visual_C74_next_state : std_logic_vector(16 - 1 downto 0 );
signal visual_C74_next_count : std_logic_vector(16 - 1 downto 0 );
signal visual_C74_en_state : std_logic_vector(16 - 1 downto 0 );

-- Start Configuration Specification
-- ++ for all : GTP_TILE_A7 use entity work.GTP_TILE_A7(RTL);
-- ++ for all : GTP_16b20bEnc use entity work.GTP_16b20bEnc(GTP_16b20bEnc);
-- ++ for all : GTP_RxLogic use entity work.GTP_RxLogic(GTP_RxLogic);
-- ++ for all : RSFF use entity work.RSFF(V1);
-- ++ for all : PosEdge use entity work.PosEdge(V1);
-- ++ for all : GTP_RstSeq use entity work.GTP_RstSeq(GTP_RstSeq);
-- End Configuration Specification

begin

B_GTP_Quad: GTP_TILE_A7
  generic map (G_RATE => G_RATE
               )
  port map (
            GT0_DRP_BUSY_OUT => open,
            GT0_DRPADDR_IN => Zero9b(8 downto 0),
            GT0_DRPCLK_IN => UserClk,
            GT0_DRPDI_IN => Zero16b(15 downto 0),
            GT0_DRPDO_OUT => open,
            GT0_DRPEN_IN => Zero,
            GT0_DRPRDY_OUT => open,
            GT0_DRPWE_IN => Zero,
            GT0_RXUSERRDY_IN => RxUserReady0,
            GT0_EYESCANDATAERROR_OUT => open,
            GT0_RXCDRHOLD_IN => Zero,
            GT0_RXCDRLOCK_OUT => open,
            GT0_RXCLKCORCNT_OUT => RxClkCorCnt0(1 downto 0),
            GT0_RXDATA_OUT => GT0_RXDATA_OUT(15 downto 0),
            GT0_RXUSRCLK_IN => UserClk,
            GT0_RXUSRCLK2_IN => UserClk,
            GT0_RXPRBSERR_OUT => PRBSErr0,
            GT0_RXPRBSSEL_IN => PRBSRxEna0(2 downto 0),
            GT0_RXPRBSCNTRESET_IN => PRBSErr0,
            GT0_RXCHARISCOMMA_OUT => open,
            GT0_RXCHARISK_OUT => GT0_RXCHARISK_OUT(1 downto 0),
            GT0_RXDISPERR_OUT => GT0_RXDISPERR(1 downto 0),
            GT0_RXNOTINTABLE_OUT => GT0_RXNOTINTABLE(1 downto 0),
            GT0_GTPRXN_IN => Rx0_N,
            GT0_GTPRXP_IN => Rx0_P,
            GT0_RXBUFRESET_IN => RxBufRst0,
            GT0_RXBUFSTATUS_OUT => RxBufStatus0(2 downto 0),
            GT0_RXBYTEISALIGNED_OUT => GT0_RXBYTEISALIGNED,
            GT0_RXMCOMMAALIGNEN_IN => GT0_RXENCOMMAALIGN,
            GT0_RXPCOMMAALIGNEN_IN => GT0_RXENCOMMAALIGN,
            GT0_RXLPMHFHOLD_IN => Zero,
            GT0_RXLPMLFHOLD_IN => Zero,
            GT0_RXOUTCLK_OUT => open,
            GT0_GTRXRESET_IN => GT0_RxRst,
            GT0_RXPCSRESET_IN => GT0_RxPCSRst,
            GT0_RXPMARESET_IN => RxPMARst0,
            GT0_RXRESETDONE_OUT => RxRstDone0_loc,
            GT0_GTTXRESET_IN => TxRst0,
            GT0_TXUSERRDY_IN => TxUserReady0,
            GT0_TXDATA_IN => GT0_TXDATA_IN(15 downto 0),
            GT0_TXUSRCLK_IN => UserClk,
            GT0_TXUSRCLK2_IN => UserClk,
            GT0_TXPRBSFORCEERR_IN => PRBSTxForce0,
            GT0_TX8B10BBYPASS_IN => One2b(1 downto 0),
            GT0_TXCHARDISPMODE_IN => GT0_TXCHARDISPMODE_IN(1 downto 0),
            GT0_TXCHARDISPVAL_IN => GT0_TXCHARDISPVAL_IN(1 downto 0),
            GT0_TXCHARISK_IN => Zero2b(1 downto 0),
            GT0_TXBUFSTATUS_OUT => TxBufStatus0(1 downto 0),
            GT0_GTPTXN_OUT => Tx0_N,
            GT0_GTPTXP_OUT => Tx0_P,
            GT0_TXOUTCLK_OUT => open,
            GT0_TXOUTCLKFABRIC_OUT => open,
            GT0_TXOUTCLKPCS_OUT => open,
            GT0_TXPCSRESET_IN => TxPCSRst0,
            GT0_TXPMARESET_IN => TxPMARst0,
            GT0_TXRESETDONE_OUT => TxRstDone0,
            GT0_TXPRBSSEL_IN => PRBSTxEna0(2 downto 0),
            GT1_DRP_BUSY_OUT => open,
            GT1_DRPADDR_IN => Zero9b(8 downto 0),
            GT1_DRPCLK_IN => UserClk,
            GT1_DRPDI_IN => Zero16b(15 downto 0),
            GT1_DRPDO_OUT => open,
            GT1_DRPEN_IN => Zero,
            GT1_DRPRDY_OUT => open,
            GT1_DRPWE_IN => Zero,
            GT1_RXUSERRDY_IN => RxUserReady1,
            GT1_EYESCANDATAERROR_OUT => open,
            GT1_RXCDRHOLD_IN => Zero,
            GT1_RXCDRLOCK_OUT => open,
            GT1_RXCLKCORCNT_OUT => RxClkCorCnt1(1 downto 0),
            GT1_RXDATA_OUT => GT1_RXDATA_OUT(15 downto 0),
            GT1_RXUSRCLK_IN => UserClk,
            GT1_RXUSRCLK2_IN => UserClk,
            GT1_RXPRBSERR_OUT => PRBSErr1,
            GT1_RXPRBSSEL_IN => PRBSRxEna1(2 downto 0),
            GT1_RXPRBSCNTRESET_IN => PRBSErr1,
            GT1_RXCHARISCOMMA_OUT => open,
            GT1_RXCHARISK_OUT => GT1_RXCHARISK_OUT(1 downto 0),
            GT1_RXDISPERR_OUT => GT1_RXDISPERR(1 downto 0),
            GT1_RXNOTINTABLE_OUT => GT1_RXNOTINTABLE(1 downto 0),
            GT1_GTPRXN_IN => Rx1_N,
            GT1_GTPRXP_IN => Rx1_P,
            GT1_RXBUFRESET_IN => RxBufRst1,
            GT1_RXBUFSTATUS_OUT => RxBufStatus1(2 downto 0),
            GT1_RXBYTEISALIGNED_OUT => GT1_RXBYTEISALIGNED,
            GT1_RXMCOMMAALIGNEN_IN => GT1_RXENCOMMAALIGN,
            GT1_RXPCOMMAALIGNEN_IN => GT1_RXENCOMMAALIGN,
            GT1_RXLPMHFHOLD_IN => Zero,
            GT1_RXLPMLFHOLD_IN => Zero,
            GT1_RXOUTCLK_OUT => open,
            GT1_GTRXRESET_IN => GT1_RxRst,
            GT1_RXPCSRESET_IN => GT1_RxPCSRst,
            GT1_RXPMARESET_IN => RxPMARst1,
            GT1_RXRESETDONE_OUT => RxRstDone1_loc,
            GT1_GTTXRESET_IN => TxRst1,
            GT1_TXUSERRDY_IN => TxUserReady1,
            GT1_TXDATA_IN => GT1_TXDATA_IN(15 downto 0),
            GT1_TXUSRCLK_IN => UserClk,
            GT1_TXUSRCLK2_IN => UserClk,
            GT1_TXPRBSFORCEERR_IN => PRBSTxForce1,
            GT1_TX8B10BBYPASS_IN => One2b(1 downto 0),
            GT1_TXCHARDISPMODE_IN => GT1_TXCHARDISPMODE_IN(1 downto 0),
            GT1_TXCHARDISPVAL_IN => GT1_TXCHARDISPVAL_IN(1 downto 0),
            GT1_TXCHARISK_IN => Zero2b(1 downto 0),
            GT1_TXBUFSTATUS_OUT => TxBufStatus1(1 downto 0),
            GT1_GTPTXN_OUT => Tx1_N,
            GT1_GTPTXP_OUT => Tx1_P,
            GT1_TXOUTCLK_OUT => open,
            GT1_TXOUTCLKFABRIC_OUT => open,
            GT1_TXOUTCLKPCS_OUT => open,
            GT1_TXPCSRESET_IN => TxPCSRst1,
            GT1_TXPMARESET_IN => TxPMARst1,
            GT1_TXRESETDONE_OUT => TxRstDone1,
            GT1_TXPRBSSEL_IN => PRBSTxEna1(2 downto 0),
            GT2_DRP_BUSY_OUT => open,
            GT2_DRPADDR_IN => Zero9b(8 downto 0),
            GT2_DRPCLK_IN => UserClk,
            GT2_DRPDI_IN => Zero16b(15 downto 0),
            GT2_DRPDO_OUT => open,
            GT2_DRPEN_IN => Zero,
            GT2_DRPRDY_OUT => open,
            GT2_DRPWE_IN => Zero,
            GT2_RXUSERRDY_IN => RxUserReady2,
            GT2_EYESCANDATAERROR_OUT => open,
            GT2_RXCDRHOLD_IN => Zero,
            GT2_RXCDRLOCK_OUT => open,
            GT2_RXCLKCORCNT_OUT => RxClkCorCnt2(1 downto 0),
            GT2_RXDATA_OUT => GT2_RXDATA_OUT(15 downto 0),
            GT2_RXUSRCLK_IN => UserClk,
            GT2_RXUSRCLK2_IN => UserClk,
            GT2_RXPRBSERR_OUT => PRBSErr2,
            GT2_RXPRBSSEL_IN => PRBSRxEna2(2 downto 0),
            GT2_RXPRBSCNTRESET_IN => PRBSErr2,
            GT2_RXCHARISCOMMA_OUT => open,
            GT2_RXCHARISK_OUT => GT2_RXCHARISK_OUT(1 downto 0),
            GT2_RXDISPERR_OUT => GT2_RXDISPERR(1 downto 0),
            GT2_RXNOTINTABLE_OUT => GT2_RXNOTINTABLE(1 downto 0),
            GT2_GTPRXN_IN => Rx2_N,
            GT2_GTPRXP_IN => Rx2_P,
            GT2_RXBUFRESET_IN => RxBufRst2,
            GT2_RXBUFSTATUS_OUT => RxBufStatus2(2 downto 0),
            GT2_RXBYTEISALIGNED_OUT => GT2_RXBYTEISALIGNED,
            GT2_RXMCOMMAALIGNEN_IN => GT2_RXENCOMMAALIGN,
            GT2_RXPCOMMAALIGNEN_IN => GT2_RXENCOMMAALIGN,
            GT2_RXLPMHFHOLD_IN => Zero,
            GT2_RXLPMLFHOLD_IN => Zero,
            GT2_RXOUTCLK_OUT => open,
            GT2_GTRXRESET_IN => GT2_RxRst,
            GT2_RXPCSRESET_IN => GT2_RxPCSRst,
            GT2_RXPMARESET_IN => RxPMARst2,
            GT2_RXRESETDONE_OUT => RxRstDone2_loc,
            GT2_GTTXRESET_IN => TxRst2,
            GT2_TXUSERRDY_IN => TxUserReady2,
            GT2_TXDATA_IN => GT2_TXDATA_IN(15 downto 0),
            GT2_TXUSRCLK_IN => UserClk,
            GT2_TXUSRCLK2_IN => UserClk,
            GT2_TXPRBSFORCEERR_IN => PRBSTxForce2,
            GT2_TX8B10BBYPASS_IN => One2b(1 downto 0),
            GT2_TXCHARDISPMODE_IN => GT2_TXCHARDISPMODE_IN(1 downto 0),
            GT2_TXCHARDISPVAL_IN => GT2_TXCHARDISPVAL_IN(1 downto 0),
            GT2_TXCHARISK_IN => Zero2b(1 downto 0),
            GT2_TXBUFSTATUS_OUT => TxBufStatus2(1 downto 0),
            GT2_GTPTXN_OUT => Tx2_N,
            GT2_GTPTXP_OUT => Tx2_P,
            GT2_TXOUTCLK_OUT => open,
            GT2_TXOUTCLKFABRIC_OUT => open,
            GT2_TXOUTCLKPCS_OUT => open,
            GT2_TXPCSRESET_IN => TxPCSRst2,
            GT2_TXPMARESET_IN => TxPMARst2,
            GT2_TXRESETDONE_OUT => TxRstDone2,
            GT2_TXPRBSSEL_IN => PRBSTxEna2(2 downto 0),
            GT3_DRP_BUSY_OUT => open,
            GT3_DRPADDR_IN => Zero9b(8 downto 0),
            GT3_DRPCLK_IN => UserClk,
            GT3_DRPDI_IN => Zero16b(15 downto 0),
            GT3_DRPDO_OUT => open,
            GT3_DRPEN_IN => Zero,
            GT3_DRPRDY_OUT => open,
            GT3_DRPWE_IN => Zero,
            GT3_RXUSERRDY_IN => RxUserReady3,
            GT3_EYESCANDATAERROR_OUT => open,
            GT3_RXCDRHOLD_IN => Zero,
            GT3_RXCDRLOCK_OUT => open,
            GT3_RXCLKCORCNT_OUT => RxClkCorCnt3(1 downto 0),
            GT3_RXDATA_OUT => GT3_RXDATA_OUT(15 downto 0),
            GT3_RXUSRCLK_IN => UserClk,
            GT3_RXUSRCLK2_IN => UserClk,
            GT3_RXPRBSERR_OUT => PRBSErr3,
            GT3_RXPRBSSEL_IN => PRBSRxEna3(2 downto 0),
            GT3_RXPRBSCNTRESET_IN => PRBSErr3,
            GT3_RXCHARISCOMMA_OUT => open,
            GT3_RXCHARISK_OUT => GT3_RXCHARISK_OUT(1 downto 0),
            GT3_RXDISPERR_OUT => GT3_RXDISPERR(1 downto 0),
            GT3_RXNOTINTABLE_OUT => GT3_RXNOTINTABLE(1 downto 0),
            GT3_GTPRXN_IN => Rx3_N,
            GT3_GTPRXP_IN => Rx3_P,
            GT3_RXBUFRESET_IN => RxBufRst3,
            GT3_RXBUFSTATUS_OUT => RxBufStatus3(2 downto 0),
            GT3_RXBYTEISALIGNED_OUT => GT3_RXBYTEISALIGNED,
            GT3_RXMCOMMAALIGNEN_IN => GT3_RXENCOMMAALIGN,
            GT3_RXPCOMMAALIGNEN_IN => GT3_RXENCOMMAALIGN,
            GT3_RXLPMHFHOLD_IN => Zero,
            GT3_RXLPMLFHOLD_IN => Zero,
            GT3_RXOUTCLK_OUT => open,
            GT3_GTRXRESET_IN => GT3_RxRst,
            GT3_RXPCSRESET_IN => GT3_RxPCSRst,
            GT3_RXPMARESET_IN => RxPMARst3,
            GT3_RXRESETDONE_OUT => RxRstDone3_loc,
            GT3_GTTXRESET_IN => TxRst3,
            GT3_TXUSERRDY_IN => TxUserReady3,
            GT3_TXDATA_IN => GT3_TXDATA_IN(15 downto 0),
            GT3_TXUSRCLK_IN => UserClk,
            GT3_TXUSRCLK2_IN => UserClk,
            GT3_TXPRBSFORCEERR_IN => PRBSTxForce3,
            GT3_TX8B10BBYPASS_IN => One2b(1 downto 0),
            GT3_TXCHARDISPMODE_IN => GT3_TXCHARDISPMODE_IN(1 downto 0),
            GT3_TXCHARDISPVAL_IN => GT3_TXCHARDISPVAL_IN(1 downto 0),
            GT3_TXCHARISK_IN => Zero2b(1 downto 0),
            GT3_TXBUFSTATUS_OUT => TxBufStatus3(1 downto 0),
            GT3_GTPTXN_OUT => Tx3_N,
            GT3_GTPTXP_OUT => Tx3_P,
            GT3_TXOUTCLK_OUT => open,
            GT3_TXOUTCLKFABRIC_OUT => open,
            GT3_TXOUTCLKPCS_OUT => open,
            GT3_TXPCSRESET_IN => TxPCSRst3,
            GT3_TXPMARESET_IN => TxPMARst3,
            GT3_TXRESETDONE_OUT => TxRstDone3,
            GT3_TXPRBSSEL_IN => PRBSTxEna3(2 downto 0),
            GT0_GTREFCLK0_IN => GTPClk,
            GT0_PLL0LOCK_OUT => PLLLock_loc,
            GT0_PLL0LOCKDETCLK_IN => UserClk,
            GT0_PLL0REFCLKLOST_OUT => PLLRefClkLost,
            GT0_PLL0RESET_IN => PLLRst
            );

B_GTP_16b20b_0: GTP_16b20bEnc
  port map (
            In_16b => TxData0(15 downto 0),
            Clk => UserClk,
            Out_20b => TXDATA0_IN(19 downto 0),
            Rst => TxRst0,
            KMode => KMode0(1 downto 0),
            Str => TxStr0
            );

GTP_ila : ila_1
port map (
  clk    => UserClk,
  probe0 => TxData0(15 downto 0),
  probe1 => TxStr0,
  probe2 => KMode0(1),
  probe3 => '0',
  probe4 => "00000",
  probe5 => "00000",
  probe6 => '0',
  probe7 => '0',
  probe8 => '0'
);


B_GTP_16b20b_1: GTP_16b20bEnc
  port map (
            In_16b => TxData1(15 downto 0),
            Clk => UserClk,
            Out_20b => TXDATA1_IN(19 downto 0),
            Rst => TxRst1,
            KMode => KMode1(1 downto 0),
            Str => TxStr1
            );

B_GTP_16b20b_2: GTP_16b20bEnc
  port map (
            In_16b => TxData2(15 downto 0),
            Clk => UserClk,
            Out_20b => TXDATA2_IN(19 downto 0),
            Rst => TxRst2,
            KMode => KMode2(1 downto 0),
            Str => TxStr2
            );

B_GTP_16b20b_3: GTP_16b20bEnc
  port map (
            In_16b => TxData3(15 downto 0),
            Clk => UserClk,
            Out_20b => TXDATA3_IN(19 downto 0),
            Rst => TxRst3,
            KMode => KMode3(1 downto 0),
            Str => TxStr3
            );

B_GTP_RxLogic_0: GTP_RxLogic
  port map (
            GTPData => GT0_RXDATA_OUT(15 downto 0),
            RxData => RxData0(15 downto 0),
            GTPIsK => GT0_RXCHARISK_OUT(1 downto 0),
            UsrClk => UserClk,
            RxStr => RxStr0,
            RxCommaMisalign => RxCommaMisalign0,
            RxDoubleComma => RxDoubleComma0,
            RxComma => RxComma0,
            Valid => Rx0Valid
            );

B_GTP_RxLogic_1: GTP_RxLogic
  port map (
            GTPData => GT1_RXDATA_OUT(15 downto 0),
            RxData => RxData1(15 downto 0),
            GTPIsK => GT1_RXCHARISK_OUT(1 downto 0),
            UsrClk => UserClk,
            RxStr => RxStr1,
            RxCommaMisalign => RxCommaMisalign1,
            RxDoubleComma => RxDoubleComma1,
            RxComma => RxComma1,
            Valid => Rx1Valid
            );

B_GTP_RxLogic_2: GTP_RxLogic
  port map (
            GTPData => GT2_RXDATA_OUT(15 downto 0),
            RxData => RxData2(15 downto 0),
            GTPIsK => GT2_RXCHARISK_OUT(1 downto 0),
            UsrClk => UserClk,
            RxStr => RxStr2,
            RxCommaMisalign => RxCommaMisalign2,
            RxDoubleComma => RxDoubleComma2,
            RxComma => RxComma2,
            Valid => Rx2Valid
            );

B_GTP_RxLogic_3: GTP_RxLogic
  port map (
            GTPData => GT3_RXDATA_OUT(15 downto 0),
            RxData => RxData3(15 downto 0),
            GTPIsK => GT3_RXCHARISK_OUT(1 downto 0),
            UsrClk => UserClk,
            RxStr => RxStr3,
            RxCommaMisalign => RxCommaMisalign3,
            RxDoubleComma => RxDoubleComma3,
            RxComma => RxComma3,
            Valid => Rx3Valid
            );

B_AlignRSFF_0: RSFF
  port map (
            Clk => UserClk,
            Set => AlignRx0,
            Clr => AlignedEdge0,
            Rst => open,
            Q => AlignRx0Q
            );

B_AlignRSFF_1: RSFF
  port map (
            Clk => UserClk,
            Set => AlignRx1,
            Clr => AlignedEdge1,
            Rst => open,
            Q => AlignRx1Q
            );

B_AlignRSFF_2: RSFF
  port map (
            Clk => UserClk,
            Set => AlignRx2,
            Clr => AlignedEdge2,
            Rst => open,
            Q => AlignRx2Q
            );

B_AlignRSFF_3: RSFF
  port map (
            Clk => UserClk,
            Set => AlignRx3,
            Clr => AlignedEdge3,
            Rst => open,
            Q => AlignRx3Q
            );

B_AlignedEdge_0: PosEdge
  port map (
            Clk => UserClk,
            SigIn => GT0_RXBYTEISALIGNED,
            PEdg => AlignedEdge0
            );

B_AlignedEdge_1: PosEdge
  port map (
            Clk => UserClk,
            SigIn => GT1_RXBYTEISALIGNED,
            PEdg => AlignedEdge1
            );

B_AlignedEdge_2: PosEdge
  port map (
            Clk => UserClk,
            SigIn => GT2_RXBYTEISALIGNED,
            PEdg => AlignedEdge2
            );

B_AlignedEdge_3: PosEdge
  port map (
            Clk => UserClk,
            SigIn => GT3_RXBYTEISALIGNED,
            PEdg => AlignedEdge3
            );

B_Rx0RstSeq: GTP_RstSeq
  port map (
            Clk => UserClk,
            Rst => PLLRst,
            Invalid => Rx0Invalid,
            RstRxCDR_in => RxRst0,
            RstRx_in => RxPCSRst0,
            RstRxCDR_out => GT0_RxRst,
            RstRx_out => GT0_RxPCSRst,
            RstDone => RxRstDone0_loc,
            Sta => open,
            Disable => RxRst0Disable
            );

B_Rx1RstSeq: GTP_RstSeq
  port map (
            Clk => UserClk,
            Rst => PLLRst,
            Invalid => Rx1Invalid,
            RstRxCDR_in => RxRst1,
            RstRx_in => RxPCSRst1,
            RstRxCDR_out => GT1_RxRst,
            RstRx_out => GT1_RxPCSRst,
            RstDone => RxRstDone1_loc,
            Sta => open,
            Disable => RxRst1Disable
            );

B_Rx2RstSeq: GTP_RstSeq
  port map (
            Clk => UserClk,
            Rst => PLLRst,
            Invalid => Rx2Invalid,
            RstRxCDR_in => RxRst2,
            RstRx_in => RxPCSRst2,
            RstRxCDR_out => GT2_RxRst,
            RstRx_out => GT2_RxPCSRst,
            RstDone => RxRstDone2_loc,
            Sta => open,
            Disable => RxRst2Disable
            );

B_Rx3RstSeq: GTP_RstSeq
  port map (
            Clk => UserClk,
            Rst => PLLRst,
            Invalid => Rx3Invalid,
            RstRxCDR_in => RxRst3,
            RstRx_in => RxPCSRst3,
            RstRxCDR_out => GT3_RxRst,
            RstRx_out => GT3_RxPCSRst,
            RstDone => RxRstDone3_loc,
            Sta => open,
            Disable => RxRst3Disable
            );

Loc_PRBSErrCnt0(15 downto 0) <= (visual_C67_cur_state);


visual_C67_en_state <= visual_C67_next_count
                     when CEN0 = '1'
                     else visual_C67_cur_state;

visual_C67_next_state <= visual_C67_en_state;

process (UserClk)
begin
if (UserClk'event and UserClk = '1') then
  if (PRBSCntReset0 = '1') then
    visual_C67_cur_state <= (others => '0');
 else
     visual_C67_cur_state <= visual_C67_next_state;
 end if;
end if;
end process;

process (visual_C67_cur_state )
variable plus_minus_one : unsigned(16 - 1 downto 0);

begin
  plus_minus_one :=  "0000000000000001" ;
  visual_C67_next_count <= std_logic_vector(unsigned(visual_C67_cur_state) +
 plus_minus_one);

end process;

 CEN0 <= (not TERCNT0) and ( PRBSErr0_1);

Loc_PRBSErrCnt1(15 downto 0) <= (visual_C68_cur_state);


visual_C68_en_state <= visual_C68_next_count
                     when CEN1 = '1'
                     else visual_C68_cur_state;

visual_C68_next_state <= visual_C68_en_state;

process (UserClk)
begin
if (UserClk'event and UserClk = '1') then
  if (PRBSCntReset1 = '1') then
    visual_C68_cur_state <= (others => '0');
 else
     visual_C68_cur_state <= visual_C68_next_state;
 end if;
end if;
end process;

process (visual_C68_cur_state )
variable plus_minus_one : unsigned(16 - 1 downto 0);

begin
  plus_minus_one :=  "0000000000000001" ;
  visual_C68_next_count <= std_logic_vector(unsigned(visual_C68_cur_state) +
 plus_minus_one);

end process;

 CEN1 <= (not TERCNT1) and ( PRBSErr1_1);

 Faults_DispError0_loc <= ( GT0_RXDISPERR(1)) or ( GT0_RXDISPERR(0));

 Faults_NotInTable0_loc <= ( GT0_RXNOTINTABLE(1)) or ( GT0_RXNOTINTABLE(0));

IsAligned0 <= GT0_RXBYTEISALIGNED;

One2b(1 downto 0) <= "11";

GT0_TXCHARDISPMODE_IN(1) <= TXDATA0_IN(0);
GT0_TXCHARDISPVAL_IN(1)  <= TXDATA0_IN(1);
GT0_TXDATA_IN(15)        <= TXDATA0_IN(2);
GT0_TXDATA_IN(14)        <= TXDATA0_IN(3);
GT0_TXDATA_IN(13)        <= TXDATA0_IN(4);
GT0_TXDATA_IN(12)        <= TXDATA0_IN(5);
GT0_TXDATA_IN(11)        <= TXDATA0_IN(6);
GT0_TXDATA_IN(10)        <= TXDATA0_IN(7);
GT0_TXDATA_IN(9)         <= TXDATA0_IN(8);
GT0_TXDATA_IN(8)         <= TXDATA0_IN(9);
GT0_TXCHARDISPMODE_IN(0) <= TXDATA0_IN(10);
GT0_TXCHARDISPVAL_IN(0)  <= TXDATA0_IN(11);
GT0_TXDATA_IN(7)         <= TXDATA0_IN(12);
GT0_TXDATA_IN(6)         <= TXDATA0_IN(13);
GT0_TXDATA_IN(5)         <= TXDATA0_IN(14);
GT0_TXDATA_IN(4)         <= TXDATA0_IN(15);
GT0_TXDATA_IN(3)         <= TXDATA0_IN(16);
GT0_TXDATA_IN(2)         <= TXDATA0_IN(17);
GT0_TXDATA_IN(1)         <= TXDATA0_IN(18);
GT0_TXDATA_IN(0)         <= TXDATA0_IN(19);

Zero2b(1 downto 0) <= "00";

GT1_TXCHARDISPMODE_IN(1) <= TXDATA1_IN(0);
GT1_TXCHARDISPVAL_IN(1)  <= TXDATA1_IN(1);
GT1_TXDATA_IN(15)        <= TXDATA1_IN(2);
GT1_TXDATA_IN(14)        <= TXDATA1_IN(3);
GT1_TXDATA_IN(13)        <= TXDATA1_IN(4);
GT1_TXDATA_IN(12)        <= TXDATA1_IN(5);
GT1_TXDATA_IN(11)        <= TXDATA1_IN(6);
GT1_TXDATA_IN(10)        <= TXDATA1_IN(7);
GT1_TXDATA_IN(9)         <= TXDATA1_IN(8);
GT1_TXDATA_IN(8)         <= TXDATA1_IN(9);
GT1_TXCHARDISPMODE_IN(0) <= TXDATA1_IN(10);
GT1_TXCHARDISPVAL_IN(0)  <= TXDATA1_IN(11);
GT1_TXDATA_IN(7)         <= TXDATA1_IN(12);
GT1_TXDATA_IN(6)         <= TXDATA1_IN(13);
GT1_TXDATA_IN(5)         <= TXDATA1_IN(14);
GT1_TXDATA_IN(4)         <= TXDATA1_IN(15);
GT1_TXDATA_IN(3)         <= TXDATA1_IN(16);
GT1_TXDATA_IN(2)         <= TXDATA1_IN(17);
GT1_TXDATA_IN(1)         <= TXDATA1_IN(18);
GT1_TXDATA_IN(0)         <= TXDATA1_IN(19);

GT2_TXCHARDISPMODE_IN(1) <= TXDATA2_IN(0);
GT2_TXCHARDISPVAL_IN(1)  <= TXDATA2_IN(1);
GT2_TXDATA_IN(15)        <= TXDATA2_IN(2);
GT2_TXDATA_IN(14)        <= TXDATA2_IN(3);
GT2_TXDATA_IN(13)        <= TXDATA2_IN(4);
GT2_TXDATA_IN(12)        <= TXDATA2_IN(5);
GT2_TXDATA_IN(11)        <= TXDATA2_IN(6);
GT2_TXDATA_IN(10)        <= TXDATA2_IN(7);
GT2_TXDATA_IN(9)         <= TXDATA2_IN(8);
GT2_TXDATA_IN(8)         <= TXDATA2_IN(9);
GT2_TXCHARDISPMODE_IN(0) <= TXDATA2_IN(10);
GT2_TXCHARDISPVAL_IN(0)  <= TXDATA2_IN(11);
GT2_TXDATA_IN(7)         <= TXDATA2_IN(12);
GT2_TXDATA_IN(6)         <= TXDATA2_IN(13);
GT2_TXDATA_IN(5)         <= TXDATA2_IN(14);
GT2_TXDATA_IN(4)         <= TXDATA2_IN(15);
GT2_TXDATA_IN(3)         <= TXDATA2_IN(16);
GT2_TXDATA_IN(2)         <= TXDATA2_IN(17);
GT2_TXDATA_IN(1)         <= TXDATA2_IN(18);
GT2_TXDATA_IN(0)         <= TXDATA2_IN(19);

GT3_TXCHARDISPMODE_IN(1) <= TXDATA3_IN(0);
GT3_TXCHARDISPVAL_IN(1)  <= TXDATA3_IN(1);
GT3_TXDATA_IN(15)        <= TXDATA3_IN(2);
GT3_TXDATA_IN(14)        <= TXDATA3_IN(3);
GT3_TXDATA_IN(13)        <= TXDATA3_IN(4);
GT3_TXDATA_IN(12)        <= TXDATA3_IN(5);
GT3_TXDATA_IN(11)        <= TXDATA3_IN(6);
GT3_TXDATA_IN(10)        <= TXDATA3_IN(7);
GT3_TXDATA_IN(9)         <= TXDATA3_IN(8);
GT3_TXDATA_IN(8)         <= TXDATA3_IN(9);
GT3_TXCHARDISPMODE_IN(0) <= TXDATA3_IN(10);
GT3_TXCHARDISPVAL_IN(0)  <= TXDATA3_IN(11);
GT3_TXDATA_IN(7)         <= TXDATA3_IN(12);
GT3_TXDATA_IN(6)         <= TXDATA3_IN(13);
GT3_TXDATA_IN(5)         <= TXDATA3_IN(14);
GT3_TXDATA_IN(4)         <= TXDATA3_IN(15);
GT3_TXDATA_IN(3)         <= TXDATA3_IN(16);
GT3_TXDATA_IN(2)         <= TXDATA3_IN(17);
GT3_TXDATA_IN(1)         <= TXDATA3_IN(18);
GT3_TXDATA_IN(0)         <= TXDATA3_IN(19);

process (UserClk)
begin
if (UserClk'event and UserClk = '1') then
      RxDataTest0(15 downto 0) <= (GT0_RXDATA_OUT(15 downto 0));


end if;
end process;

process (UserClk)
begin
if (UserClk'event and UserClk = '1') then
      RxIsKTest0(1 downto 0) <= (GT0_RXCHARISK_OUT(1 downto 0));


end if;
end process;

process (UserClk)
begin
if (UserClk'event and UserClk = '1') then
      RxDataTest1(15 downto 0) <= (GT1_RXDATA_OUT(15 downto 0));


end if;
end process;

process (UserClk)
begin
if (UserClk'event and UserClk = '1') then
      RxIsKTest1(1 downto 0) <= (GT1_RXCHARISK_OUT(1 downto 0));


end if;
end process;

process (UserClk)
begin
if (UserClk'event and UserClk = '1') then
      RxDataTest2(15 downto 0) <= (GT2_RXDATA_OUT(15 downto 0));


end if;
end process;

process (UserClk)
begin
if (UserClk'event and UserClk = '1') then
      RxIsKTest2(1 downto 0) <= (GT2_RXCHARISK_OUT(1 downto 0));


end if;
end process;

process (UserClk)
begin
if (UserClk'event and UserClk = '1') then
      RxDataTest3(15 downto 0) <= (GT3_RXDATA_OUT(15 downto 0));


end if;
end process;

process (UserClk)
begin
if (UserClk'event and UserClk = '1') then
      RxIsKTest3(1 downto 0) <= (GT3_RXCHARISK_OUT(1 downto 0));


end if;
end process;

Zero9b(8 downto 0) <= '0' & X"00";

Zero16b(15 downto 0) <= X"0000";

Zero <= '0';

Loc_PRBSErrCnt2(15 downto 0) <= (visual_C72_cur_state);


visual_C72_en_state <= visual_C72_next_count
                     when CEN2 = '1'
                     else visual_C72_cur_state;

visual_C72_next_state <= visual_C72_en_state;

process (UserClk)
begin
if (UserClk'event and UserClk = '1') then
  if (PRBSCntReset2 = '1') then
    visual_C72_cur_state <= (others => '0');
 else
     visual_C72_cur_state <= visual_C72_next_state;
 end if;
end if;
end process;

process (visual_C72_cur_state )
variable plus_minus_one : unsigned(16 - 1 downto 0);

begin
  plus_minus_one :=  "0000000000000001" ;
  visual_C72_next_count <= std_logic_vector(unsigned(visual_C72_cur_state) +
 plus_minus_one);

end process;

 CEN2 <= (not TERCNT2) and ( PRBSErr2_1);

Loc_PRBSErrCnt3(15 downto 0) <= (visual_C74_cur_state);


visual_C74_en_state <= visual_C74_next_count
                     when CEN3 = '1'
                     else visual_C74_cur_state;

visual_C74_next_state <= visual_C74_en_state;

process (UserClk)
begin
if (UserClk'event and UserClk = '1') then
  if (PRBSCntReset3 = '1') then
    visual_C74_cur_state <= (others => '0');
 else
     visual_C74_cur_state <= visual_C74_next_state;
 end if;
end if;
end process;

process (visual_C74_cur_state )
variable plus_minus_one : unsigned(16 - 1 downto 0);

begin
  plus_minus_one :=  "0000000000000001" ;
  visual_C74_next_count <= std_logic_vector(unsigned(visual_C74_cur_state) +
 plus_minus_one);

end process;

 CEN3 <= (not TERCNT3) and ( PRBSErr3_1);

 Faults_DispError1_loc <= ( GT1_RXDISPERR(1)) or ( GT1_RXDISPERR(0));

 Faults_NotInTable1_loc <= ( GT1_RXNOTINTABLE(1)) or ( GT1_RXNOTINTABLE(0));

IsAligned1 <= GT1_RXBYTEISALIGNED;

 Faults_DispError2_loc <= ( GT2_RXDISPERR(1)) or ( GT2_RXDISPERR(0));

 Faults_NotInTable2_loc <= ( GT2_RXNOTINTABLE(1)) or ( GT2_RXNOTINTABLE(0));

IsAligned2 <= GT2_RXBYTEISALIGNED;

 Faults_DispError3_loc <= ( GT3_RXDISPERR(1)) or ( GT3_RXDISPERR(0));

 Faults_NotInTable3_loc <= ( GT3_RXNOTINTABLE(1)) or ( GT3_RXNOTINTABLE(0));

IsAligned3 <= GT3_RXBYTEISALIGNED;

KMode0(1) <= TxMkr0;
KMode0(0) <= Zero;

KMode1(1) <= TxMkr1;
KMode1(0) <= Zero;

KMode2(1) <= TxMkr2;
KMode2(0) <= Zero;

KMode3(1) <= TxMkr3;
KMode3(0) <= Zero;

process (UserClk)
begin
if (UserClk'event and UserClk = '1') then
      PRBSErr0_1 <= (PRBSErr0);


end if;
end process;

process (UserClk)
begin
if (UserClk'event and UserClk = '1') then
      PRBSErr1_1 <= (PRBSErr1);


end if;
end process;

process (UserClk)
begin
if (UserClk'event and UserClk = '1') then
      PRBSErr2_1 <= (PRBSErr2);


end if;
end process;

process (UserClk)
begin
if (UserClk'event and UserClk = '1') then
      PRBSErr3_1 <= (PRBSErr3);


end if;
end process;

process (UserClk)
begin
if (UserClk'event and UserClk = '1') then
      PRBSErrCnt0(15 downto 0) <= (Loc_PRBSErrCnt0(15 downto 0));


end if;
end process;

process (UserClk)
begin
if (UserClk'event and UserClk = '1') then
      PRBSErrCnt1(15 downto 0) <= (Loc_PRBSErrCnt1(15 downto 0));


end if;
end process;

process (UserClk)
begin
if (UserClk'event and UserClk = '1') then
      PRBSErrCnt2(15 downto 0) <= (Loc_PRBSErrCnt2(15 downto 0));


end if;
end process;

process (UserClk)
begin
if (UserClk'event and UserClk = '1') then
      PRBSErrCnt3(15 downto 0) <= (Loc_PRBSErrCnt3(15 downto 0));


end if;
end process;

 TERCNT0 <= ( Loc_PRBSErrCnt0(15)) and ( Loc_PRBSErrCnt0(14))
           and ( Loc_PRBSErrCnt0(13)) and ( Loc_PRBSErrCnt0(12))
           and ( Loc_PRBSErrCnt0(11)) and ( Loc_PRBSErrCnt0(10))
           and ( Loc_PRBSErrCnt0(9)) and ( Loc_PRBSErrCnt0(8))
           and ( Loc_PRBSErrCnt0(7)) and ( Loc_PRBSErrCnt0(6))
           and ( Loc_PRBSErrCnt0(5)) and ( Loc_PRBSErrCnt0(4))
           and ( Loc_PRBSErrCnt0(3)) and ( Loc_PRBSErrCnt0(2))
           and ( Loc_PRBSErrCnt0(1)) and ( Loc_PRBSErrCnt0(0));

 TERCNT1 <= ( Loc_PRBSErrCnt1(15)) and ( Loc_PRBSErrCnt1(14))
           and ( Loc_PRBSErrCnt1(13)) and ( Loc_PRBSErrCnt1(12))
           and ( Loc_PRBSErrCnt1(11)) and ( Loc_PRBSErrCnt1(10))
           and ( Loc_PRBSErrCnt1(9)) and ( Loc_PRBSErrCnt1(8))
           and ( Loc_PRBSErrCnt1(7)) and ( Loc_PRBSErrCnt1(6))
           and ( Loc_PRBSErrCnt1(5)) and ( Loc_PRBSErrCnt1(4))
           and ( Loc_PRBSErrCnt1(3)) and ( Loc_PRBSErrCnt1(2))
           and ( Loc_PRBSErrCnt1(1)) and ( Loc_PRBSErrCnt1(0));

 TERCNT2 <= ( Loc_PRBSErrCnt2(15)) and ( Loc_PRBSErrCnt2(14))
           and ( Loc_PRBSErrCnt2(13)) and ( Loc_PRBSErrCnt2(12))
           and ( Loc_PRBSErrCnt2(11)) and ( Loc_PRBSErrCnt2(10))
           and ( Loc_PRBSErrCnt2(9)) and ( Loc_PRBSErrCnt2(8))
           and ( Loc_PRBSErrCnt2(7)) and ( Loc_PRBSErrCnt2(6))
           and ( Loc_PRBSErrCnt2(5)) and ( Loc_PRBSErrCnt2(4))
           and ( Loc_PRBSErrCnt2(3)) and ( Loc_PRBSErrCnt2(2))
           and ( Loc_PRBSErrCnt2(1)) and ( Loc_PRBSErrCnt2(0));

 TERCNT3 <= ( Loc_PRBSErrCnt3(15)) and ( Loc_PRBSErrCnt3(14))
           and ( Loc_PRBSErrCnt3(13)) and ( Loc_PRBSErrCnt3(12))
           and ( Loc_PRBSErrCnt3(11)) and ( Loc_PRBSErrCnt3(10))
           and ( Loc_PRBSErrCnt3(9)) and ( Loc_PRBSErrCnt3(8))
           and ( Loc_PRBSErrCnt3(7)) and ( Loc_PRBSErrCnt3(6))
           and ( Loc_PRBSErrCnt3(5)) and ( Loc_PRBSErrCnt3(4))
           and ( Loc_PRBSErrCnt3(3)) and ( Loc_PRBSErrCnt3(2))
           and ( Loc_PRBSErrCnt3(1)) and ( Loc_PRBSErrCnt3(0));

 GT0_RXENCOMMAALIGN <= ( AlignRx0Q) or (not GT0_RXBYTEISALIGNED);

Rx0ByteNotAligned <= not (GT0_RXBYTEISALIGNED);

Faults_DispError0 <= Faults_DispError0_loc;

Faults_NotInTable0 <= Faults_NotInTable0_loc;

Rx0InvalidSignals(3) <= Faults_DispError0_loc;
Rx0InvalidSignals(2) <= Faults_NotInTable0_loc;
Rx0InvalidSignals(1) <= Rx0ByteNotAligned;
Rx0InvalidSignals(0) <= Sfp0NotOk;

Sfp0NotOk <= not (Valid0);

 Rx0Invalid <= ( Rx0InvalidSignals(3)) or ( Rx0InvalidSignals(2))
           or ( Rx0InvalidSignals(1)) or ( Rx0InvalidSignals(0));

Rx0Valid <= not (Rx0Invalid);

 GT1_RXENCOMMAALIGN <= ( AlignRx1Q) or (not GT1_RXBYTEISALIGNED);

 GT2_RXENCOMMAALIGN <= ( AlignRx2Q) or (not GT2_RXBYTEISALIGNED);

 GT3_RXENCOMMAALIGN <= ( AlignRx3Q) or (not GT3_RXBYTEISALIGNED);

Faults_DispError1 <= Faults_DispError1_loc;

Faults_NotInTable1 <= Faults_NotInTable1_loc;

Rx1ByteNotAligned <= not (GT1_RXBYTEISALIGNED);

Rx1InvalidSignals(3) <= Faults_DispError1_loc;
Rx1InvalidSignals(2) <= Faults_NotInTable1_loc;
Rx1InvalidSignals(1) <= Rx1ByteNotAligned;
Rx1InvalidSignals(0) <= Sfp1NotOk;

Sfp1NotOk <= not (Valid1);

 Rx1Invalid <= ( Rx1InvalidSignals(3)) or ( Rx1InvalidSignals(2))
           or ( Rx1InvalidSignals(1)) or ( Rx1InvalidSignals(0));

Rx1Valid <= not (Rx1Invalid);

Faults_DispError2 <= Faults_DispError2_loc;

Faults_NotInTable2 <= Faults_NotInTable2_loc;

Rx2ByteNotAligned <= not (GT2_RXBYTEISALIGNED);

Rx2InvalidSignals(3) <= Faults_DispError2_loc;
Rx2InvalidSignals(2) <= Faults_NotInTable2_loc;
Rx2InvalidSignals(1) <= Rx2ByteNotAligned;
Rx2InvalidSignals(0) <= Sfp2NotOk;

Sfp2NotOk <= not (Valid2);

 Rx2Invalid <= ( Rx2InvalidSignals(3)) or ( Rx2InvalidSignals(2))
           or ( Rx2InvalidSignals(1)) or ( Rx2InvalidSignals(0));

Rx2Valid <= not (Rx2Invalid);

Faults_DispError3 <= Faults_DispError3_loc;

Faults_NotInTable3 <= Faults_NotInTable3_loc;

Rx3ByteNotAligned <= not (GT3_RXBYTEISALIGNED);

Rx3InvalidSignals(3) <= Faults_DispError3_loc;
Rx3InvalidSignals(2) <= Faults_NotInTable3_loc;
Rx3InvalidSignals(1) <= Rx3ByteNotAligned;
Rx3InvalidSignals(0) <= Sfp3NotOk;

Sfp3NotOk <= not (Valid3);

 Rx3Invalid <= ( Rx3InvalidSignals(3)) or ( Rx3InvalidSignals(2))
           or ( Rx3InvalidSignals(1)) or ( Rx3InvalidSignals(0));

Rx3Valid <= not (Rx3Invalid);

PLLLock <= PLLLock_loc;

RxRstDone0 <= RxRstDone0_loc;

 RxRst0Disable <= ( PRBSRx0) or (not PLLLock_loc);

 PRBSRx0 <= ( PRBSRxEna0(2)) or ( PRBSRxEna0(1)) or ( PRBSRxEna0(0));

 RxRst1Disable <= ( PRBSRx1) or (not PLLLock_loc);

 PRBSRx1 <= ( PRBSRxEna1(2)) or ( PRBSRxEna1(1)) or ( PRBSRxEna1(0));

RxRstDone1 <= RxRstDone1_loc;

RxRstDone2 <= RxRstDone2_loc;

 RxRst2Disable <= ( PRBSRx2) or (not PLLLock_loc);

 PRBSRx2 <= ( PRBSRxEna2(2)) or ( PRBSRxEna2(1)) or ( PRBSRxEna2(0));

 RxRst3Disable <= ( PRBSRx3) or (not PLLLock_loc);

 PRBSRx3 <= ( PRBSRxEna3(2)) or ( PRBSRxEna3(1)) or ( PRBSRxEna3(0));

RxRstDone3 <= RxRstDone3_loc;
end GTP_A7;
