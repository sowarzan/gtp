//////////////////////////////////////////////////////////////////////////////
// Title      : GTP_LinkEncoder
// Project    : GTP
// Date       : 2023-03-17
//////////////////////////////////////////////////////////////////////////////
// Docs: https://wikis.cern.ch/display/BERF/ObsBox+Gen1
//////////////////////////////////////////////////////////////////////////////
// File       : GTP_LinkEncoder.sv
// Author     : Sebastian Owarzany
// Company    : CERN
// Platform   : multiplatform
// Standard   : SystemVerilog IEEE 1800
//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2021-2022 CERN. All rights reserved.
//////////////////////////////////////////////////////////////////////////////

`default_nettype none

module GTP_LinkEncoder # (
  parameter [31:0] HEADER    = 32'h4F627331,
  parameter [31:0] BLOCKSIZE = 512
) (
  input  wire  [4:0]  headCnt_i,   // Fixed ASCII constant "Obs1" (0x4F627331) used to identify the header in the data stream,
  input  wire  [31:0] sourceID_i,  // A fixed constant, unique per system, used to identify each transmitter,
  input  wire  [31:0] turn_i,      // Monotonically increasing turn counter which is,
  input  wire  [31:0] tags_i,      // Tag bits to describe events associated to each turn,
  input  wire  [31:0] cycle_i,     // SPS: Current PPM cycle number / LHC: set to zero
  input  wire  [31:0] crc_i,       // CRC (Ethernet flavour) checksum calculated over the entire frame including the header
  output reg   [15:0] data_o       // Out incoming data in prpopr order and size
);

  always @( * ) begin
    case (headCnt_i)
      ({ 1'b0, 4'h0 }) : data_o <= HEADER[31:16];
      ({ 1'b0, 4'h1 }) : data_o <= HEADER[15:0];
      ({ 1'b0, 4'h2 }) : data_o <= sourceID_i[31:16];
      ({ 1'b0, 4'h3 }) : data_o <= sourceID_i[15:0];
      ({ 1'b0, 4'h4 }) : data_o <= BLOCKSIZE[31:16];
      ({ 1'b0, 4'h5 }) : data_o <= BLOCKSIZE[15:0];
      ({ 1'b0, 4'h6 }) : data_o <= turn_i[31:16];
      ({ 1'b0, 4'h7 }) : data_o <= turn_i[15:0];
      ({ 1'b0, 4'h8 }) : data_o <= tags_i[31:16];
      ({ 1'b0, 4'h9 }) : data_o <= tags_i[15:0];
      ({ 1'b0, 4'hA }) : data_o <= cycle_i[31:16];
      ({ 1'b0, 4'hB }) : data_o <= cycle_i[15:0];
      ({ 1'b0, 4'hC }) : data_o <= 16'h0000;
      ({ 1'b0, 4'hD }) : data_o <= 16'h0000;
      ({ 1'b0, 4'hE }) : data_o <= 16'h0000;
      ({ 1'b0, 4'hF }) : data_o <= 16'h0000;
      ({ 1'b1, 4'h0 }) : data_o <= crc_i[31:16];
      ({ 1'b1, 4'h1 }) : data_o <= crc_i[15:0];
      ({ 1'b1, 4'h2 }) : data_o <= 16'h0000; //
      ({ 1'b1, 4'h3 }) : data_o <= 16'h0000; // at least three comma characters (K28.5 + D5.6/D16.2) are inserted
      ({ 1'b1, 4'h4 }) : data_o <= 16'h0000; //
      default : data_o <= 16'h0000;
    endcase // case (header_i)
  end // always_comb

endmodule // GTP_LinkEncoder