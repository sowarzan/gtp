----------------------------------------------------
--
--  Library Name :  SPSDamperLoops
--  Unit    Name :  GTP_OutFF
--  Unit    Type :  Text Unit
--
------------------------------------------------------
------------------------------------------
------------------------------------------
-- Date        : Sun Dec 14 15:49:29 2014
--
-- Author      : Tom Levens <tom.levens@cern.ch>
--
-- Company     : CERN, BE-RF-FB
--
-- Description :
--
------------------------------------------
------------------------------------------

library ieee;
use ieee.STD_LOGIC_1164.all;

entity  GTP_OutFF  is
    port (
        Clk                 : in std_logic;
        UserClk             : in std_logic;

        PRBSErrCnt_I        : in std_logic_vector(15 downto 0);
        RxClkCorCnt_I       : in std_logic_vector(1 downto 0);
        RxBufStatus_I       : in std_logic_vector(2 downto 0);
        TxBufStatus_I       : in std_logic_vector(1 downto 0);
        IsAligned_I         : in std_logic;
        FaultsDispError_I   : in std_logic;
        FaultsNotInTable_I  : in std_logic;
        RxRstDone_I         : in std_logic;
        TxRstDone_I         : in std_logic;

        PRBSErrCnt_O        : out std_logic_vector(15 downto 0);
        RxClkCorCnt_O       : out std_logic_vector(1 downto 0);
        RxBufStatus_O       : out std_logic_vector(2 downto 0);
        TxBufStatus_O       : out std_logic_vector(1 downto 0);
        IsAligned_O         : out std_logic;
        FaultsDispError_O   : out std_logic;
        FaultsNotInTable_O  : out std_logic;
        RxRstDone_O         : out std_logic;
        TxRstDone_O         : out std_logic
    );
end;

------------------------------------------
------------------------------------------
-- Date        : Sun Dec 14 15:52:22 2014
--
-- Author      : Tom Levens <tom.levens@cern.ch>
--
-- Company     : CERN, BE-RF-FB
--
-- Description :
--
------------------------------------------
------------------------------------------

architecture  V1  of  GTP_OutFF  is

signal Loc_PRBSErrCnt1       : std_logic_vector(15 downto 0);
signal Loc_RxClkCorCnt1      : std_logic_vector(1 downto 0);
signal Loc_RxBufStatus1      : std_logic_vector(2 downto 0);
signal Loc_TxBufStatus1      : std_logic_vector(1 downto 0);
signal Loc_IsAligned1        : std_logic;
signal Loc_FaultsDispError1  : std_logic;
signal Loc_FaultsNotInTable1 : std_logic;
signal Loc_RxRstDone1        : std_logic;
signal Loc_TxRstDone1        : std_logic;

signal Loc_PRBSErrCnt2       : std_logic_vector(15 downto 0);
signal Loc_RxClkCorCnt2      : std_logic_vector(1 downto 0);
signal Loc_RxBufStatus2      : std_logic_vector(2 downto 0);
signal Loc_TxBufStatus2      : std_logic_vector(1 downto 0);
signal Loc_IsAligned2        : std_logic;
signal Loc_FaultsDispError2  : std_logic;
signal Loc_FaultsNotInTable2 : std_logic;
signal Loc_RxRstDone2        : std_logic;
signal Loc_TxRstDone2        : std_logic;

begin
    process (UserClk) begin
        if rising_edge(UserClk) then
            Loc_PRBSErrCnt1          <= PRBSErrCnt_I;
            Loc_RxClkCorCnt1         <= RxClkCorCnt_I;
            Loc_RxBufStatus1         <= RxBufStatus_I;
            Loc_TxBufStatus1         <= TxBufStatus_I;
            Loc_IsAligned1           <= IsAligned_I;
            Loc_FaultsDispError1     <= FaultsDispError_I;
            Loc_FaultsNotInTable1    <= FaultsNotInTable_I;
            Loc_RxRstDone1           <= RxRstDone_I;
            Loc_TxRstDone1           <= TxRstDone_I;
        end if;
    end process;

    process (Clk) begin
        if rising_edge(Clk) then
            Loc_PRBSErrCnt2          <= Loc_PRBSErrCnt1;
            Loc_RxClkCorCnt2         <= Loc_RxClkCorCnt1;
            Loc_RxBufStatus2         <= Loc_RxBufStatus1;
            Loc_TxBufStatus2         <= Loc_TxBufStatus1;
            Loc_IsAligned2           <= Loc_IsAligned1;
            Loc_FaultsDispError2     <= Loc_FaultsDispError1;
            Loc_FaultsNotInTable2    <= Loc_FaultsNotInTable1;
            Loc_RxRstDone2           <= Loc_RxRstDone1;
            Loc_TxRstDone2           <= Loc_TxRstDone1;
        end if;
    end process;

    process (Clk) begin
        if rising_edge(Clk) then
            PRBSErrCnt_O         <= Loc_PRBSErrCnt2;
            RxClkCorCnt_O        <= Loc_RxClkCorCnt2;
            RxBufStatus_O        <= Loc_RxBufStatus2;
            TxBufStatus_O        <= Loc_TxBufStatus2;
            IsAligned_O          <= Loc_IsAligned2;
            FaultsDispError_O    <= Loc_FaultsDispError2;
            FaultsNotInTable_O   <= Loc_FaultsNotInTable2;
            RxRstDone_O          <= Loc_RxRstDone2;
            TxRstDone_O          <= Loc_TxRstDone2;
        end if;
    end process;
end;