//////////////////////////////////////////////////////////////////////////////
// Title      : crc32_d16_pkg
// Project    : GTP
// Date       : 2023-03-17
//////////////////////////////////////////////////////////////////////////////
// Docs: https://wikis.cern.ch/display/BERF/ObsBox+Gen1
//////////////////////////////////////////////////////////////////////////////
// File       : crc32_d16_pkg.sv
// Author     : Sebastian Owarzany
// Company    : CERN
// Platform   : multiplatform
// Standard   : SystemVerilog IEEE 1800
//////////////////////////////////////////////////////////////////////////////
// Copyright [c] 2021-2022 CERN. All rights reserved.
//////////////////////////////////////////////////////////////////////////////


`ifndef crc32_d16_pkg_DONE
  `define crc32_d16_pkg_DONE
  package crc32_d16_pkg;
    function logic [31:0] nextCRC32_D16 (logic [15:0] data, logic [31:0] crc);

      nextCRC32_D16[0]  = data[12] ^ data[10] ^ data[9]  ^ data[6] ^ data[0] ^ crc[16]  ^ crc[22]  ^ crc[25]  ^ crc[26]  ^ crc[28] ;
      nextCRC32_D16[1]  = data[13] ^ data[12] ^ data[11] ^ data[9] ^ data[7] ^ data[6]  ^ data[1]  ^ data[0]  ^ crc[16]  ^ crc[17]  ^ crc[22]  ^ crc[23]  ^ crc[25]  ^ crc[27]  ^ crc[28]  ^ crc[29] ;
      nextCRC32_D16[2]  = data[14] ^ data[13] ^ data[9]  ^ data[8] ^ data[7] ^ data[6]  ^ data[2]  ^ data[1]  ^ data[0]  ^ crc[16]  ^ crc[17]  ^ crc[18]  ^ crc[22]  ^ crc[23]  ^ crc[24]  ^ crc[25]  ^ crc[29]  ^ crc[30] ;
      nextCRC32_D16[3]  = data[15] ^ data[14] ^ data[10] ^ data[9] ^ data[8] ^ data[7]  ^ data[3]  ^ data[2]  ^ data[1]  ^ crc[17]  ^ crc[18]  ^ crc[19]  ^ crc[23]  ^ crc[24]  ^ crc[25]  ^ crc[26]  ^ crc[30]  ^ crc[31] ;
      nextCRC32_D16[4]  = data[15] ^ data[12] ^ data[11] ^ data[8] ^ data[6] ^ data[4]  ^ data[3]  ^ data[2]  ^ data[0]  ^ crc[16]  ^ crc[18]  ^ crc[19]  ^ crc[20]  ^ crc[22]  ^ crc[24]  ^ crc[27]  ^ crc[28]  ^ crc[31] ;
      nextCRC32_D16[5]  = data[13] ^ data[10] ^ data[7]  ^ data[6] ^ data[5] ^ data[4]  ^ data[3]  ^ data[1]  ^ data[0]  ^ crc[16]  ^ crc[17]  ^ crc[19]  ^ crc[20]  ^ crc[21]  ^ crc[22]  ^ crc[23]  ^ crc[26]  ^ crc[29] ;
      nextCRC32_D16[6]  = data[14] ^ data[11] ^ data[8]  ^ data[7] ^ data[6] ^ data[5]  ^ data[4]  ^ data[2]  ^ data[1]  ^ crc[17]  ^ crc[18]  ^ crc[20]  ^ crc[21]  ^ crc[22]  ^ crc[23]  ^ crc[24]  ^ crc[27]  ^ crc[30] ;
      nextCRC32_D16[7]  = data[15] ^ data[10] ^ data[8]  ^ data[7] ^ data[5] ^ data[3]  ^ data[2]  ^ data[0]  ^ crc[16]  ^ crc[18]  ^ crc[19]  ^ crc[21]  ^ crc[23]  ^ crc[24]  ^ crc[26]  ^ crc[31] ;
      nextCRC32_D16[8]  = data[12] ^ data[11] ^ data[10] ^ data[8] ^ data[4] ^ data[3]  ^ data[1]  ^ data[0]  ^ crc[16]  ^ crc[17]  ^ crc[19]  ^ crc[20]  ^ crc[24]  ^ crc[26]  ^ crc[27]  ^ crc[28] ;
      nextCRC32_D16[9]  = data[13] ^ data[12] ^ data[11] ^ data[9] ^ data[5] ^ data[4]  ^ data[2]  ^ data[1]  ^ crc[17]  ^ crc[18]  ^ crc[20]  ^ crc[21]  ^ crc[25]  ^ crc[27]  ^ crc[28]  ^ crc[29] ;
      nextCRC32_D16[10] = data[14] ^ data[13] ^ data[9]  ^ data[5] ^ data[3] ^ data[2]  ^ data[0]  ^ crc[16]  ^ crc[18]  ^ crc[19]  ^ crc[21]  ^ crc[25]  ^ crc[29]  ^ crc[30] ;
      nextCRC32_D16[11] = data[15] ^ data[14] ^ data[12] ^ data[9] ^ data[4] ^ data[3]  ^ data[1]  ^ data[0]  ^ crc[16]  ^ crc[17]  ^ crc[19]  ^ crc[20]  ^ crc[25]  ^ crc[28]  ^ crc[30]  ^ crc[31] ;
      nextCRC32_D16[12] = data[15] ^ data[13] ^ data[12] ^ data[9] ^ data[6] ^ data[5]  ^ data[4]  ^ data[2]  ^ data[1]  ^ data[0]  ^ crc[16]  ^ crc[17]  ^ crc[18]  ^ crc[20]  ^ crc[21]  ^ crc[22]  ^ crc[25]  ^ crc[28]  ^ crc[29]  ^ crc[31] ;
      nextCRC32_D16[13] = data[14] ^ data[13] ^ data[10] ^ data[7] ^ data[6] ^ data[5]  ^ data[3]  ^ data[2]  ^ data[1]  ^ crc[17]  ^ crc[18]  ^ crc[19]  ^ crc[21]  ^ crc[22]  ^ crc[23]  ^ crc[26]  ^ crc[29]  ^ crc[30] ;
      nextCRC32_D16[14] = data[15] ^ data[14] ^ data[11] ^ data[8] ^ data[7] ^ data[6]  ^ data[4]  ^ data[3]  ^ data[2]  ^ crc[18]  ^ crc[19]  ^ crc[20]  ^ crc[22]  ^ crc[23]  ^ crc[24]  ^ crc[27]  ^ crc[30]  ^ crc[31] ;
      nextCRC32_D16[15] = data[15] ^ data[12] ^ data[9]  ^ data[8] ^ data[7] ^ data[5]  ^ data[4]  ^ data[3]  ^ crc[19]  ^ crc[20]  ^ crc[21]  ^ crc[23]  ^ crc[24]  ^ crc[25]  ^ crc[28]  ^ crc[31] ;
      nextCRC32_D16[16] = data[13] ^ data[12] ^ data[8]  ^ data[5] ^ data[4] ^ data[0]  ^ crc[0]   ^ crc[16]  ^ crc[20]  ^ crc[21]  ^ crc[24]  ^ crc[28]  ^ crc[29] ;
      nextCRC32_D16[17] = data[14] ^ data[13] ^ data[9]  ^ data[6] ^ data[5] ^ data[1]  ^ crc[1]   ^ crc[17]  ^ crc[21]  ^ crc[22]  ^ crc[25]  ^ crc[29]  ^ crc[30] ;
      nextCRC32_D16[18] = data[15] ^ data[14] ^ data[10] ^ data[7] ^ data[6] ^ data[2]  ^ crc[2]   ^ crc[18]  ^ crc[22]  ^ crc[23]  ^ crc[26]  ^ crc[30]  ^ crc[31] ;
      nextCRC32_D16[19] = data[15] ^ data[11] ^ data[8]  ^ data[7] ^ data[3] ^ crc[3]   ^ crc[19]  ^ crc[23]  ^ crc[24]  ^ crc[27]  ^ crc[31] ;
      nextCRC32_D16[20] = data[12] ^ data[9]  ^ data[8]  ^ data[4] ^ crc[4]  ^ crc[20]  ^ crc[24]  ^ crc[25]  ^ crc[28] ;
      nextCRC32_D16[21] = data[13] ^ data[10] ^ data[9]  ^ data[5] ^ crc[5]  ^ crc[21]  ^ crc[25]  ^ crc[26]  ^ crc[29] ;
      nextCRC32_D16[22] = data[14] ^ data[12] ^ data[11] ^ data[9] ^ data[0] ^ crc[6]   ^ crc[16]  ^ crc[25]  ^ crc[27]  ^ crc[28]  ^ crc[30] ;
      nextCRC32_D16[23] = data[15] ^ data[13] ^ data[9]  ^ data[6] ^ data[1] ^ data[0]  ^ crc[7]   ^ crc[16]  ^ crc[17]  ^ crc[22]  ^ crc[25]  ^ crc[29]  ^ crc[31] ;
      nextCRC32_D16[24] = data[14] ^ data[10] ^ data[7]  ^ data[2] ^ data[1] ^ crc[8]   ^ crc[17]  ^ crc[18]  ^ crc[23]  ^ crc[26]  ^ crc[30] ;
      nextCRC32_D16[25] = data[15] ^ data[11] ^ data[8]  ^ data[3] ^ data[2] ^ crc[9]   ^ crc[18]  ^ crc[19]  ^ crc[24]  ^ crc[27]  ^ crc[31] ;
      nextCRC32_D16[26] = data[10] ^ data[6]  ^ data[4]  ^ data[3] ^ data[0] ^ crc[10]  ^ crc[16]  ^ crc[19]  ^ crc[20]  ^ crc[22]  ^ crc[26] ;
      nextCRC32_D16[27] = data[11] ^ data[7]  ^ data[5]  ^ data[4] ^ data[1] ^ crc[11]  ^ crc[17]  ^ crc[20]  ^ crc[21]  ^ crc[23]  ^ crc[27] ;
      nextCRC32_D16[28] = data[12] ^ data[8]  ^ data[6]  ^ data[5] ^ data[2] ^ crc[12]  ^ crc[18]  ^ crc[21]  ^ crc[22]  ^ crc[24]  ^ crc[28] ;
      nextCRC32_D16[29] = data[13] ^ data[9]  ^ data[7]  ^ data[6] ^ data[3] ^ crc[13]  ^ crc[19]  ^ crc[22]  ^ crc[23]  ^ crc[25]  ^ crc[29] ;
      nextCRC32_D16[30] = data[14] ^ data[10] ^ data[8]  ^ data[7] ^ data[4] ^ crc[14]  ^ crc[20]  ^ crc[23]  ^ crc[24]  ^ crc[26]  ^ crc[30] ;
      nextCRC32_D16[31] = data[15] ^ data[11] ^ data[9]  ^ data[8] ^ data[5] ^ crc[15]  ^ crc[21]  ^ crc[24]  ^ crc[25]  ^ crc[27]  ^ crc[31] ;

    endfunction // nextCRC32_D16
  endpackage // crc32_d16_pkg

  // Import package in the design
  import crc32_d16_pkg::*;

`endif // crc32_d16_pkg_DONE