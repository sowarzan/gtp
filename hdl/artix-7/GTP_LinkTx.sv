//////////////////////////////////////////////////////////////////////////////
// Title      : GTP_LinkTx
// Project    : GTP
// Date       : 2023-03-17
//////////////////////////////////////////////////////////////////////////////
// Docs: https://wikis.cern.ch/display/BERF/ObsBox+Gen1
//////////////////////////////////////////////////////////////////////////////
// File       : GTP_LinkTx.sv
// Author     : Sebastian Owarzany
// Company    : CERN
// Platform   : multiplatform
// Standard   : SystemVerilog IEEE 1800
//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2021-2022 CERN. All rights reserved.
//////////////////////////////////////////////////////////////////////////////

`default_nettype none

module GTP_LinkTx #(
  parameter [31:0] HEADER    = 32'h4F627331,
  parameter [31:0] BLOCKSIZE = 512
) (
  input  wire        clk_125mhz,
  input  wire        rst_125mhz,
  input  wire        clk_150mhz,
  input  wire        rst_150mhz,
  input  wire        data_ready_i,
  input  wire [31:0] sourceID_i,
  input  wire [31:0] cycle_i,
  input  wire [31:0] turn_i,
  input  wire [31:0] tags_i,
  input  wire [15:0] data_i,
  output wire        gtp_ready_o,
  output reg         txStr_o,
  output reg         txMrk_o,
  output reg  [15:0] txData_o
);

 ila_2 GTP_ila_0 (
   .clk(clk_150mhz), // input wire clk
   .probe0(txData_l), // input wire [15:0]  probe0
   .probe1(incHeadCnt_l), // input wire [0:0]  probe1
   .probe2(incDataCnt_l), // input wire [0:0]  probe2
   .probe3(mkr_l), // input wire [0:0]  probe3
   .probe4(currentState), // input wire [4:0]  probe4
   .probe5(headCnt_l), // input wire [4:0]  probe5
   .probe6(data_ready_i), // input wire [0:0]  probe6
   .probe7(str_l), // input wire [0:0]  probe7
   .probe8(fifoDataCnt), // input wire [8:0]  probe8
   .probe9(fifoRead_l) // input wire [8:0]  probe8
 );

  reg [2:0] currentState, nextState; // Current FSM state

  // Local parameters for FSM states
  localparam IDLE         = 3'b000, // Send comma (K28.5 + D5.6/D16.2) until we don't have enoguht data in FIFO
             SEND_K23     = 3'b001, // Send one comma (K23.7 + K23.7) befor sending data frame
             SEND_HEADER  = 3'b010, // Send data frame from "Header" to "Revolution-frequency"
             SEND_DATA    = 3'b011, // Send user data, size (BLOCKSIZE * 8) / 16
             SEND_CRC     = 3'b100, // Send calculated CRC and
             SEND_K28     = 3'b101; // Send 3 commas (K28.5 + D5.6/D16.2)

  localparam num16bData = BLOCKSIZE/2; // Number of 16b data which we want to send in one data frame; (size_i*8)/16
  localparam [$clog2(num16bData)+2:0] fifoSize = '{default:'1}; // Size of FIFO memory based on BLOCKSIZE

  logic incHeadCnt_l, // If high start increment HeadCnt_l
        incDataCnt_l; // If high start increment DataCnt_l

  wire [$clog2(num16bData)+2:0] fifoDataCnt;            // Number of data collected in FIFO
  reg  [15:0] dataCnt; // Counter for num16bData


  wire [15:0] data_l,    // Data collected from input after async CDC FIFO
              frameData; // Frame data generated by GTP_LinkEncoder for GTP
  logic [4:0] headCnt_l; // Counter for frame generation for GTP_LinkEncoder

  wire [31:0] crc_l; // Vector for calculated CRC value from CRC32_D16

  wire FIFO_full; // FIFO flag

  reg [15:0] txData_l; // Vector for data sended to GTP
  reg fifoRead_l, // Read enable flag for FIFO
      mkr_l,      // Send comma flag for GTP
      str_l,      // Send data flag for GTP
      addCRC_l,   // CRC add flag for FSM
      crcEna_l;   // Count CRC flag for CRC32_D16 module

  always @( * ) begin : fsmTransactionCombProcess
      case( currentState )
        IDLE : begin
          if ( fifoDataCnt >= num16bData ) nextState <= SEND_K23;
          else                             nextState <= IDLE;
        end
        SEND_K23 : begin
          nextState <= SEND_HEADER;
        end
        SEND_HEADER : begin
          if ( headCnt_l >= {1'b0, 4'hF} ) nextState  <= SEND_DATA;
          else                             nextState  <= SEND_HEADER;
        end
        SEND_DATA : begin
          if ( dataCnt >= (num16bData) )   nextState  <= SEND_CRC;
          else                             nextState  <= SEND_DATA;
        end
        SEND_CRC : begin
          if ( headCnt_l >= { 1'b1, 4'h1 }  ) nextState <= SEND_K28;
          else                                nextState <= SEND_CRC;
        end
        SEND_K28 : begin
          if ( headCnt_l >= { 1'b1, 4'h4 }  ) nextState <= IDLE;
          else                                nextState <= SEND_K28;
        end
        default : begin
          nextState  <= IDLE;
        end
      endcase
  end
  always @( * ) begin  : fsmExecutionCombProcess
    case( currentState )
      IDLE : begin  // Default state after reset
        fifoRead_l   <= 1'b0;
        incHeadCnt_l <= 1'b0;
        incDataCnt_l <= 1'b0;
        mkr_l        <= 1'b0;
        str_l        <= 1'b0;
        addCRC_l     <= 1'b0;
      end
      SEND_K23 : begin // If we don't have enough amount of data in FIFO continuously send commas
        fifoRead_l   <= 1'b0;
        incHeadCnt_l <= 1'b0;
        incDataCnt_l <= 1'b0;
        str_l        <= 1'b0;
        mkr_l        <= 1'b1;
        addCRC_l     <= 1'b0;
      end
      SEND_HEADER : begin // If we have enough amount of data in FIFO start sending data frame; data from "Header" to "Revolution-frequency"
        if ( headCnt_l >= {1'b0, 4'hF} ) fifoRead_l <= 1'b1;
        else                             fifoRead_l <= 1'b0;
        incHeadCnt_l <= 1'b1;
        incDataCnt_l <= 1'b0;
        str_l        <= 1'b1;
        mkr_l        <= 1'b0;
        addCRC_l     <= 1'b0;
      end
      SEND_DATA : begin // After sending first part of dataframe send incoming user data collected in FIFO
        if ( dataCnt >= (num16bData-1) ) fifoRead_l <= 1'b0;
        else                             fifoRead_l <= 1'b1;
        incHeadCnt_l <= 1'b0;
        incDataCnt_l <= 1'b1;
        str_l        <= 1'b1;
        mkr_l        <= 1'b0;
        addCRC_l     <= 1'b0;
      end
      SEND_CRC : begin // If we send amount of user data definite by num16bData send calculated CRC and 3*(K28.5 + D5.6/D16.2)
        fifoRead_l   <= 1'b0;
        incHeadCnt_l <= 1'b1;
        incDataCnt_l <= 1'b0;
        str_l        <= 1'b1;
        mkr_l        <= 1'b0;
        addCRC_l     <= 1'b1;
      end
      SEND_K28 : begin // Send 3 commas (K28.5 + D5.6/D16.2)
        fifoRead_l   <= 1'b0;
        incHeadCnt_l <= 1'b1;
        incDataCnt_l <= 1'b0;
        str_l        <= 1'b0;
        mkr_l        <= 1'b0;
        addCRC_l     <= 1'b0;
      end
      default : begin
        fifoRead_l   <= 1'b0;
        incHeadCnt_l <= 1'b0;
        incDataCnt_l <= 1'b0;
        str_l        <= 1'b0;
        mkr_l        <= 1'b0;
        addCRC_l     <= 1'b0;
      end
    endcase
  end

  always_ff @( posedge clk_150mhz ) begin : fsmTransactionSyncProcess
    if ( rst_150mhz ) currentState <= IDLE;
    else              currentState <= nextState;
  end

  always_ff @( posedge clk_150mhz ) begin :  countersSyncProcess
    if ( rst_150mhz ) begin
      headCnt_l <= '{default:'0};
      dataCnt   <= '{default:'0};
    end
    else begin
      if  ( incDataCnt_l > num16bData || incHeadCnt_l > { 1'b1, 4'h4 } ) begin
        headCnt_l <= '{default:'0};
        dataCnt   <= '{default:'0};
      end else begin
        headCnt_l <= headCnt_l;
        dataCnt   <= dataCnt;
      end
      if      ( incHeadCnt_l ) headCnt_l <= headCnt_l + 1'b1;
      else if ( incDataCnt_l ) dataCnt   <= dataCnt   + 1'b1;
      else begin
        headCnt_l <= '{default:'0};
        dataCnt   <= '{default:'0};
      end
    end
  end

  always_ff @( posedge clk_150mhz ) begin : outputsSyncProcess
    if ( rst_150mhz ) begin
      txMrk_o  <= 1'b0;
      txStr_o  <= 1'b0;
      txData_o <= 16'h0000;
    end else begin
      txMrk_o  <= mkr_l;
      txStr_o  <= str_l;
      txData_o <= txData_l;
    end
  end

  GTP_LinkEncoder #(
    .HEADER(HEADER),
    .BLOCKSIZE(BLOCKSIZE)
  ) GTP_LinkEncoder_inst(
    .headCnt_i(headCnt_l),
    .sourceID_i(sourceID_i),
    .turn_i(turn_i),
    .tags_i(tags_i),
    .cycle_i(cycle_i),
    .crc_i(crc_l),
    .data_o(frameData)
  ); // GTP_LinkEncoder_inst

  CRC32_D16 #(
    .InitVal('{default:1})
  ) CRC32_D16_inst (
    .clk(clk_150mhz),
    .rst(rst_150mhz),
    .data_i(txData_l),
    .crcEna_i(crcEna_l),
    .clear_i(mkr_l),
    .crc_o(crc_l)
  ); // CRC32_D16_inst

  generic_async_fifo_wrap #(
    .DATA_WIDTH(16),
    .SIZE(fifoSize + 1)
  ) FIFO_GTP_LinkTx (
    // Control/Data Signals,
    .rst_i(rst_125mhz), // FPGA Reset

    // write port
    .clk_wr_i(clk_125mhz), // Write Clock
    .d_i(data_i[15:0]),    // Write data
    .we_i(data_ready_i),   // Wrte enable
    .wr_full_o(FIFO_full), // Write full
    .wr_count_o(fifoDataCnt),

    // read port
    .clk_rd_i(clk_150mhz),   // Read Clock
    .q_o(data_l),            // Read data
    .rd_i(fifoRead_l),       // Read enable
    .rd_empty_o(), // Read empty
    .rd_count_o()
  ); // FIFO_GTP_LinkTx

  assign gtp_ready_o    = (!FIFO_full); // If FIFO is not full gtp is ready
  assign crcEna_l       = ( str_l ) && ( !addCRC_l );                  // Flag for CRC calculate
  assign txData_l[15:0] = incDataCnt_l ? data_l[15:0] : ( str_l ? frameData[15:0] : {16{1'b0}}); // If we read data from FIFO send this data to GTP
  `include "GTP_LinkTx_assertions.svh"

endmodule // GTP_LinkTx