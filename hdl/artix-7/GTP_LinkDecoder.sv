//////////////////////////////////////////////////////////////////////////////
// Title      : GTP_LinkDecoder
// Project    : GTP
// Date       : 2023-03-17
//////////////////////////////////////////////////////////////////////////////
// Docs: https://wikis.cern.ch/display/BERF/ObsBox+Gen1
//////////////////////////////////////////////////////////////////////////////
// File       : GTP_LinkDecoder.sv
// Author     : Sebastian Owarzany
// Company    : CERN
// Platform   : multiplatform
// Standard   : SystemVerilog IEEE 1800
//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2021-2022 CERN. All rights reserved.
//////////////////////////////////////////////////////////////////////////////

`default_nettype none

module GTP_LinkDecoder (
  input  wire [4:0]  headCnt_i,   // Fixed ASCII constant "Obs1" (0x4F627331) used to identify the header in the data stream,
  input  wire [15:0] data_i,      // Out incoming data in prpopr order and size
  output reg  [31:0] header_o,    // Header for trensomiter
  output reg  [31:0] sourceID_o,  // A fixed constant, unique per system, used to identify each transmitter,
  output reg  [31:0] blockSize_o, // Size of user data
  output reg  [31:0] turn_o,      // Monotonically increasing turn counter which is,
  output reg  [31:0] tags_o,      // Tag bits to describe events associated to each turn,
  output reg  [31:0] cycle_o,     // SPS: Current PPM cycle number / LHC: set to zero
  output reg  [31:0] crc_o        // CRC (Ethernet flavour) checksum calculated over the entire frame including the header
);

  always_comb begin
    case (headCnt_i)
      ({ 1'b0, 4'h0}) : header_o[31:16]    <= data_i;
      ({ 1'b0, 4'h1}) : header_o[15:0]     <= data_i;
      ({ 1'b0, 4'h2}) : sourceID_o[31:16]  <= data_i;
      ({ 1'b0, 4'h3}) : sourceID_o[15:0]   <= data_i;
      ({ 1'b0, 4'h4}) : blockSize_o[31:16] <= data_i;
      ({ 1'b0, 4'h5}) : blockSize_o[15:0]  <= data_i;
      ({ 1'b0, 4'h6}) : turn_o[31:16]      <= data_i;
      ({ 1'b0, 4'h7}) : turn_o[15:0]       <= data_i;
      ({ 1'b0, 4'h8}) : tags_o[31:16]      <= data_i;
      ({ 1'b0, 4'h9}) : tags_o[15:0]       <= data_i;
      ({ 1'b0, 4'hA}) : cycle_o[31:16]     <= data_i;
      ({ 1'b0, 4'hB}) : cycle_o[15:0]      <= data_i;
      // ({ 1'b0, 4'hC})
      // ({ 1'b0, 4'hD})
      // ({ 1'b0, 4'hE})
      // ({ 1'b0, 4'hF})
      ({ 1'b1, 4'h0}) : crc_o[31:16]       <= data_i;
      ({ 1'b1, 4'h1}) : crc_o[15:0]        <= data_i;
      default : {header_o, sourceID_o, blockSize_o, turn_o, tags_o, cycle_o, crc_o} <= '{default:'0};
    endcase // case (headCnt_i)
  end // always_comb

endmodule // GTP_LinkDecoder