----------------------------------------------------
--
--  Library Name :  CommonVisual
--  Unit    Name :  DffxN
--  Unit    Type :  Text Unit
--
------------------------------------------------------
------------------------------------------
------------------------------------------
-- Date        : Wed Feb 01 16:59:17 2006
--
-- Author      : Gregoire Hagmann
--
-- Company     : CERN BE-RF-FB
--
-- Description : Shift register with n flip-flop
--
-- E1 : enable of the first flip-flop only
-- E :  general enable (all flip flop if several)
------------------------------------------
------------------------------------------
library ieee;
use ieee.STD_LOGIC_1164.all;



entity  DffxN  is
generic(N : integer := 2); --number of D flip-flop
port (Clk : in std_logic ;
      E,E1 : in std_logic  := '1'; --Enabled by default!
      Rst : in std_logic ;
      D : in std_logic;
      Q: out std_logic);
end;

------------------------------------------
------------------------------------------
-- Date        : Wed Feb 01 16:59:10 2006
--
-- Author      : Gregoire Hagmann
--
-- Company     : CERN BE-RF-FB
--
-- Description : Shift register with N flip-flop
--
-- E1 : enable of the first flip-flop only
-- E :  general enable (all flip flop if several)
------------------------------------------
------------------------------------------
-- History
-- Date : 20120925
-- Author : G. hagmann
-- Changes:
--    1)Change the condition on the generic attribut, put '1' instead of 0
--    2)If attributs is zero => no D flip flop => D=Q
------------------------------------------

architecture  DffxN  of  DffxN  is

signal Loc_RegN : std_logic_vector(N-1 downto 0);

begin

MultiDff: if N>1 generate
begin
  process(Clk, Rst)
  begin
    if rising_edge(Clk) then
      if Rst = '1'  then
        Loc_RegN <= (others => '0');
      elsif E='1' then
        if E1 = '1' then
          loc_RegN(0) <= D;
        end if;
        for I in 1 to N-1 loop
          loc_RegN(I) <= loc_RegN(I-1);
        end loop;
      end if;
    end if;
  end process;
end generate MultiDff;

SingleDff: if N=1 generate
begin
  process(Clk, Rst)
  begin
    if rising_edge(Clk) then
      if Rst = '1'  then
        Loc_RegN <= (others => '0');
      elsif E='1' and E1 = '1' then
        loc_RegN(0) <= D;
      end if;
    end if;
  end process;
end generate SingleDff;

Output0: if N=0 generate
    Q <= D;
end generate Output0;

OutputN: if N>0 generate
    Q <= loc_RegN(N-1);
end generate OutputN;


end;