//////////////////////////////////////////////////////////////////////////////
// Title      : CRC32_D16
// Project    : GTP
// Date       : 2023-03-17
//////////////////////////////////////////////////////////////////////////////
// Docs: https://wikis.cern.ch/display/BERF/ObsBox+Gen1
//////////////////////////////////////////////////////////////////////////////
// File       : CRC32_D16.sv
// Author     : Sebastian Owarzany
// Company    : CERN
// Platform   : multiplatform
// Standard   : SystemVerilog IEEE 1800
//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2021-2022 CERN. All rights reserved.
//////////////////////////////////////////////////////////////////////////////

`default_nettype none
`include "crc32_d16_pkg.sv"

module CRC32_D16 #(
  parameter [31:0] InitVal = '{default:0}
) (
  input  wire        clk,
  input  wire        rst,
  input  wire [15:0] data_i,
  input  wire        crcEna_i,
  input  wire        clear_i,
  output wire [31:0] crc_o
);

  reg [31:0] crc_l = '{default:0};

  always_ff @( posedge clk ) begin
    if      ( rst || clear_i ) crc_l <= InitVal;
    else if ( crcEna_i )       crc_l <= nextCRC32_D16(data_i, crc_l);
  end

  assign crc_o    = crc_l;

endmodule // CRC32_D16