----------------------------------------------------
--
--  Library Name :  SPSDamperLoops
--  Unit    Name :  GTP_InFF
--  Unit    Type :  Text Unit
--
------------------------------------------------------
------------------------------------------
------------------------------------------
-- Date        : Sun Dec 14 15:26:23 2014
--
-- Author      : Tom Levens <tom.levens@cern.ch>
--
-- Company     : CERN, BE-RF-FB
--
-- Description :
--
------------------------------------------
------------------------------------------

library ieee;
use ieee.STD_LOGIC_1164.all;

entity  GTP_InFF  is
    port (
        Clk             : in std_logic;
        UserClk         : in std_logic;

        PRBSEna_I       : in std_logic;
        PRBSCntReset_I  : in std_logic;
        PRBSForce_I     : in std_logic;
        RxRst_I         : in std_logic;
        TxRst_I         : in std_logic;
        RxBufRst_I      : in std_logic;
        Align_I         : in std_logic;
        RxPMARst_I      : in std_logic;
        RxPCSRst_I      : in std_logic;
        TxPMARst_I      : in std_logic;
        TxPCSRst_I      : in std_logic;

        PRBSRxEna_O     : out std_logic_vector(2 downto 0);
        PRBSTxEna_O     : out std_logic_vector(2 downto 0);
        PRBSCntReset_O  : out std_logic;
        PRBSForce_O     : out std_logic;
        RxRst_O         : out std_logic;
        TxRst_O         : out std_logic;
        RxBufRst_O      : out std_logic;
        Align_O         : out std_logic;
        RxPMARst_O      : out std_logic;
        RxPCSRst_O      : out std_logic;
        TxPMARst_O      : out std_logic;
        TxPCSRst_O      : out std_logic
    );
end;

------------------------------------------
------------------------------------------
-- Date        : Sun Dec 14 15:38:27 2014
--
-- Author      : Tom Levens <tom.levens@cern.ch>
--
-- Company     : CERN, BE-RF-FB
--
-- Description :
--
------------------------------------------
------------------------------------------

architecture  V1  of  GTP_InFF  is

signal Loc_PRBSEna1         : std_logic;
signal Loc_PRBSCntReset1    : std_logic;
signal Loc_PRBSForce1       : std_logic;
signal Loc_RxRst1           : std_logic;
signal Loc_TxRst1           : std_logic;
signal Loc_RxBufRst1        : std_logic;
signal Loc_Align1           : std_logic;
signal Loc_RxPMARst1        : std_logic;
signal Loc_RxPCSRst1        : std_logic;
signal Loc_TxPMARst1        : std_logic;
signal Loc_TxPCSRst1        : std_logic;

signal Loc_PRBSEna2         : std_logic;
signal Loc_PRBSCntReset2    : std_logic;
signal Loc_PRBSForce2       : std_logic;
signal Loc_RxRst2           : std_logic;
signal Loc_TxRst2           : std_logic;
signal Loc_RxBufRst2        : std_logic;
signal Loc_Align2           : std_logic;
signal Loc_RxPMARst2        : std_logic;
signal Loc_RxPCSRst2        : std_logic;
signal Loc_TxPMARst2        : std_logic;
signal Loc_TxPCSRst2        : std_logic;

begin
    process (Clk) begin
        if rising_edge(Clk) then
            Loc_PRBSEna1        <= PRBSEna_I;
            Loc_PRBSCntReset1   <= PRBSCntReset_I;
            Loc_PRBSForce1      <= PRBSForce_I;
            Loc_RxRst1          <= RxRst_I;
            Loc_TxRst1          <= TxRst_I;
            Loc_RxBufRst1       <= RxBufRst_I;
            Loc_Align1          <= Align_I;
            Loc_RxPMARst1       <= RxPMARst_I;
            Loc_RxPCSRst1       <= RxPCSRst_I;
            Loc_TxPMARst1       <= TxPMARst_I;
            Loc_TxPCSRst1       <= TxPCSRst_I;
        end if;
    end process;

    process (UserClk) begin
        if rising_edge(UserClk) then
            Loc_PRBSEna2        <= Loc_PRBSEna1;
            Loc_PRBSCntReset2   <= Loc_PRBSCntReset1;
            Loc_PRBSForce2      <= Loc_PRBSForce1;
            Loc_RxRst2          <= Loc_RxRst1;
            Loc_TxRst2          <= Loc_TxRst1;
            Loc_RxBufRst2       <= Loc_RxBufRst1;
            Loc_Align2          <= Loc_Align1;
            Loc_RxPMARst2       <= Loc_RxPMARst1;
            Loc_RxPCSRst2       <= Loc_RxPCSRst1;
            Loc_TxPMARst2       <= Loc_TxPMARst1;
            Loc_TxPCSRst2       <= Loc_TxPCSRst1;
        end if;
    end process;

    process (UserClk) begin
        if rising_edge(UserClk) then
            PRBSRxEna_O      <= "00" & Loc_PRBSEna2;
            PRBSTxEna_O      <= "00" & Loc_PRBSEna2;
            PRBSCntReset_O   <= Loc_PRBSCntReset2;
            PRBSForce_O      <= Loc_PRBSForce2;
            RxRst_O          <= Loc_RxRst2;
            TxRst_O          <= Loc_TxRst2;
            RxBufRst_O       <= Loc_RxBufRst2;
            Align_O          <= Loc_Align2;
            RxPMARst_O       <= Loc_RxPMARst2;
            RxPCSRst_O       <= Loc_RxPCSRst2;
            TxPMARst_O       <= Loc_TxPMARst2;
            TxPCSRst_O       <= Loc_TxPCSRst2;
        end if;
    end process;
end;