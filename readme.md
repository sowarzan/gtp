Welcome to the *gtp* project.

## Supported hardware ##
- Any board, with series 7 FPGA. Testet on EDA2917-V2 with xc7a200tffg1156.

## How to clone and build the project ##

1. Use this project like as submodule in your project:

```
git submodule add ssh://git@gitlab.cern.ch:7999/sowarzan/eda-02917-v2-0-adcdacv.git
```

## Other useful info ##

- Toolchain: Vivado 2018.2
